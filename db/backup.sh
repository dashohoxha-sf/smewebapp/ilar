#!/bin/bash
### dump the content of the database and its structure (tables)

### include the connection variables
. connection.sh

### dump only the structure of the tables of 'inima_ilar'
mysqldump --all --add-drop-table --no-data \
          --host=$HOST --user=$USER --password=$PASSWD \
          --databases $DBNAME \
          --result-file=ilar_tables.sql

### dump both the structure and the data
mysqldump --all --add-drop-table --complete-insert \
          --host=$HOST --user=$USER --password=$PASSWD \
          --databases $DBNAME \
          --result-file=ilar.sql
gzip ilar.sql
