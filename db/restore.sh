#!/bin/bash
### restore the database from a backup (dump) file

### go to this dir
cd $(dirname $0)

### include the connection parameters
. connection.sh

### restore the database from the backup file
gunzip ilar.sql.gz 
mysql --host=$HOST --user=$USER --password=$PASSWD --database=$DBNAME < ilar.sql
gzip ilar.sql
