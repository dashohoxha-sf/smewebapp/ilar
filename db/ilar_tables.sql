--
-- Table structure for table `Levizje_Type`
--

DROP TABLE IF EXISTS Levizje_Type;
CREATE TABLE Levizje_Type (
  Levizja varchar(10) NOT NULL default '',
  PRIMARY KEY  (Levizja)
) TYPE=MyISAM;

--
-- Table structure for table `botimet`
--

DROP TABLE IF EXISTS botimet;
CREATE TABLE botimet (
  botim_id int(10) unsigned NOT NULL auto_increment,
  nr_kontrate varchar(100) NOT NULL default '',
  date_kontrate date default '0000-00-00',
  date_krijimi date NOT NULL default '0000-00-00',
  date_mbyllje_botimi date default '0000-00-00',
  pala_ilar varchar(100) default '',
  pala_porositese varchar(100) default '',
  gjinia varchar(20) default '',
  formati varchar(100) default '',
  faqe_te_planifikuara varchar(100) default '0',
  faqe_shtese varchar(100) default '0',
  letra varchar(100) default '',
  kopertina varchar(100) default '',
  rradhimi tinyint(1) default '0',
  faqosja tinyint(1) default '0',
  filmi tinyint(1) default '0',
  foto_kalk int(11) default '0',
  foto_film int(11) default '0',
  nr_ngjyra_teksti int(11) default '0',
  nr_ngjyra_kopertine int(11) default '0',
  date_dorezimi date default '0000-00-00',
  afati_terheqjes int(11) default '0',
  tirazhi int(11) default '0',
  afati_kontrates date default '0000-00-00',
  qepja varchar(100) default '',
  formati_perfundimtar varchar(100) default '',
  kapaku varchar(100) default '',
  amballazhimi varchar(100) default '',
  cmimi_per_kopje int(11) default '0',
  vlera_e_kontrates int(11) default '0',
  tvsh int(11) default '0',
  parapagimi int(11) default '0',
  pagesa_perfundimtare int(11) default '0',
  urdherxhirimi varchar(100) default '',
  titull_botimi varchar(100) default '',
  PRIMARY KEY  (botim_id)
) TYPE=MyISAM;

--
-- Table structure for table `botimet_old`
--

DROP TABLE IF EXISTS botimet_old;
CREATE TABLE botimet_old (
  botim_id int(11) NOT NULL auto_increment,
  nr_kontrate varchar(11) NOT NULL default '0',
  porositesi varchar(50) default NULL,
  titull_botimi varchar(70) default NULL,
  gjinia varchar(50) default NULL,
  tipi varchar(50) default NULL,
  kopjet int(10) default NULL,
  makina varchar(50) default NULL,
  formati_botimit varchar(100) default NULL,
  faqe varchar(10) default NULL,
  formati_gatshem varchar(50) default NULL,
  nr_xhirime int(10) default NULL,
  nr_fashikuj int(10) default NULL,
  nr_faqesh int(10) default NULL,
  nr_lastrash int(10) default NULL,
  nr_ngjyrave int(10) default NULL,
  flete_tipografike int(10) default NULL,
  data date default NULL,
  creation_date date default NULL,
  shpenzimetp double(10,1) default NULL,
  ribotim smallint(1) default '0',
  close_date date default NULL,
  PRIMARY KEY  (botim_id)
) TYPE=MyISAM;

--
-- Table structure for table `buletinet`
--

DROP TABLE IF EXISTS buletinet;
CREATE TABLE buletinet (
  buletin_id bigint(11) NOT NULL auto_increment,
  reparti varchar(28) default NULL,
  makina varchar(28) default NULL,
  data date default NULL,
  turni varchar(10) default NULL,
  punetor1 varchar(20) default NULL,
  punetor2 varchar(20) NOT NULL default '',
  punetor3 varchar(20) default NULL,
  punetor4 varchar(20) default NULL,
  punetor5 varchar(20) default NULL,
  ore_pune double(4,2) NOT NULL default '0.00',
  ore_deklaruar double(4,2) NOT NULL default '0.00',
  realizimi double(6,2) NOT NULL default '0.00',
  makinisti varchar(20) default NULL,
  normisti varchar(20) default NULL,
  close_date date default NULL,
  PRIMARY KEY  (buletin_id)
) TYPE=MyISAM;

--
-- Table structure for table `buletinet_parapreg`
--

DROP TABLE IF EXISTS buletinet_parapreg;
CREATE TABLE buletinet_parapreg (
  buletin_id bigint(11) NOT NULL auto_increment,
  reparti varchar(28) default NULL,
  proces varchar(28) default NULL,
  data date default NULL,
  turni varchar(10) default NULL,
  punetor1 varchar(20) default NULL,
  ore_pune double(4,2) NOT NULL default '0.00',
  ore_deklaruar double(4,2) NOT NULL default '0.00',
  realizimi double(6,2) NOT NULL default '0.00',
  makinisti varchar(20) default NULL,
  normisti varchar(20) default NULL,
  close_date date default NULL,
  PRIMARY KEY  (buletin_id)
) TYPE=MyISAM;

--
-- Table structure for table `format`
--

DROP TABLE IF EXISTS format;
CREATE TABLE format (
  format varchar(50) NOT NULL default '',
  format_id tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (format_id)
) TYPE=MyISAM;

--
-- Table structure for table `formatet`
--

DROP TABLE IF EXISTS formatet;
CREATE TABLE formatet (
  format_id tinyint(4) NOT NULL default '0',
  format varchar(50) NOT NULL default '',
  PRIMARY KEY  (format_id)
) TYPE=MyISAM;

--
-- Table structure for table `gjinia`
--

DROP TABLE IF EXISTS gjinia;
CREATE TABLE gjinia (
  id int(11) NOT NULL auto_increment,
  gjinia varchar(50) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `kartat`
--

DROP TABLE IF EXISTS kartat;
CREATE TABLE kartat (
  kart_id int(10) unsigned NOT NULL auto_increment,
  nr_karte int(11) NOT NULL default '0',
  nr_kontrate int(11) NOT NULL default '0',
  nr_plani int(11) default '0',
  date_krijimi date NOT NULL default '0000-00-00',
  date_mbyllje date default '0000-00-00',
  titull_botimi varchar(100) default '',
  porositesi varchar(100) default '',
  sasia_cope int(11) default '0',
  formati_botimit varchar(100) default '',
  formati_i_prere varchar(100) default '',
  lloj_letre varchar(100) default '',
  kapaku varchar(100) default '',
  leter_per_fashikull int(11) default '0',
  leter_per_skarcitet_makine int(11) default '0',
  leter_per_skarcitet_libri int(11) default '0',
  leter_per_skarcitet_numeratori int(11) default '0',
  shuma_flete int(11) default '0',
  leter_per_gjithe_botimin int(11) default '0',
  maket varchar(100) default '',
  faqe_doreshkrim varchar(100) default '',
  foto_1c int(11) default '0',
  foto_4c int(11) default '0',
  foto_jashte_tekstit int(11) default '0',
  kapaku_foto_1c int(11) default '0',
  kapaku_foto_4c int(11) default '0',
  kapaku_anet int(11) default '0',
  kapaku_color varchar(10) default '',
  masa_e_faqosjes varchar(100) default '',
  maxhinatura_shpine varchar(100) default '',
  maxhinatura_koke varchar(100) default '',
  maxhinatura_anash varchar(100) default '',
  maxhinatura_poshte varchar(100) default '',
  lloji_lidhjes varchar(100) default '',
  faqe_per_folje int(11) default '0',
  letra_per_montim varchar(100) default '',
  faqe_fillim_teksti int(11) default '0',
  fashikuj_me_ngjyra int(11) default '0',
  fashikuj_1c int(11) default '0',
  lastra_per_fashikull int(11) default '0',
  lastra_per_botim int(11) default '0',
  lastra_kapaku_1c int(11) default '0',
  lastra_kapaku_4c int(11) default '0',
  lloji_letres varchar(100) default '',
  formati_per_shtyp varchar(100) default '',
  leter_per_botim int(11) default '0',
  makina varchar(100) default '',
  prerja_e_letres varchar(100) default '',
  shtyp_anet int(11) default '0',
  shtyp_color varchar(5) default '',
  shtyp_x_here int(11) default '0',
  shtyp_shenime varchar(255) default '',
  tirazhi int(11) default '0',
  faqe int(11) default '0',
  nr_fashikujve varchar(100) default '',
  formati_perfundimtar varchar(100) default '',
  cope_per_pako int(11) default '0',
  udhezime text,
  shenime_te_vecanta text,
  skemat text,
  PRIMARY KEY  (kart_id)
) TYPE=MyISAM;

--
-- Table structure for table `kartat_teknologjike`
--

DROP TABLE IF EXISTS kartat_teknologjike;
CREATE TABLE kartat_teknologjike (
  kart_id int(11) NOT NULL auto_increment,
  autorizim_nr varchar(11) NOT NULL default '0',
  porositesi varchar(50) default NULL,
  titull_botimi varchar(70) default NULL,
  gjinia varchar(50) default NULL,
  tipi varchar(50) default NULL,
  kopjet int(10) default NULL,
  makina varchar(50) default NULL,
  formati_botimit varchar(100) default NULL,
  faqe varchar(10) default NULL,
  formati_gatshem varchar(50) default NULL,
  nr_xhirime int(10) default NULL,
  nr_fashikuj int(10) default NULL,
  nr_faqesh int(10) default NULL,
  nr_lastrash int(10) default NULL,
  nr_ngjyrave int(10) default NULL,
  flete_tipografike int(10) default NULL,
  data date default NULL,
  creation_date date default NULL,
  shpenzimetp double(10,1) default NULL,
  ribotim smallint(1) default '0',
  close_date date default NULL,
  PRIMARY KEY  (kart_id)
) TYPE=MyISAM;

--
-- Table structure for table `koeficientet`
--

DROP TABLE IF EXISTS koeficientet;
CREATE TABLE koeficientet (
  id tinyint(4) NOT NULL auto_increment,
  emri varchar(20) NOT NULL default '',
  sasia double NOT NULL default '0',
  vlera double(8,2) NOT NULL default '0.00',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `koeficientet_parapreg`
--

DROP TABLE IF EXISTS koeficientet_parapreg;
CREATE TABLE koeficientet_parapreg (
  id tinyint(4) NOT NULL auto_increment,
  emri varchar(20) NOT NULL default '',
  lloj_makina varchar(50) default NULL,
  sasia double NOT NULL default '0',
  vlera double(8,2) NOT NULL default '0.00',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `llojet_e_materialit_parapreg`
--

DROP TABLE IF EXISTS llojet_e_materialit_parapreg;
CREATE TABLE llojet_e_materialit_parapreg (
  lloj_pune_id tinyint(4) NOT NULL auto_increment,
  lloj_pune varchar(50) NOT NULL default '',
  PRIMARY KEY  (lloj_pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `llojet_e_puneve`
--

DROP TABLE IF EXISTS llojet_e_puneve;
CREATE TABLE llojet_e_puneve (
  lloj_pune_id tinyint(4) NOT NULL default '0',
  lloj_pune varchar(50) NOT NULL default '',
  PRIMARY KEY  (lloj_pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `llojet_e_puneve_parapreg`
--

DROP TABLE IF EXISTS llojet_e_puneve_parapreg;
CREATE TABLE llojet_e_puneve_parapreg (
  lloj_pune_id tinyint(4) NOT NULL auto_increment,
  lloj_pune varchar(50) NOT NULL default '',
  PRIMARY KEY  (lloj_pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `lloji_leter`
--

DROP TABLE IF EXISTS lloji_leter;
CREATE TABLE lloji_leter (
  lloji_id tinyint(4) NOT NULL default '0',
  cmimi_kg double NOT NULL default '0',
  lloji varchar(30) NOT NULL default '',
  gjendja int(11) default NULL,
  PRIMARY KEY  (lloji_id)
) TYPE=MyISAM;

--
-- Table structure for table `magazina_boja`
--

DROP TABLE IF EXISTS magazina_boja;
CREATE TABLE magazina_boja (
  boja_id int(11) NOT NULL auto_increment,
  emertimi varchar(255) default NULL,
  cmimi int(11) default NULL,
  gjendja int(11) default NULL,
  PRIMARY KEY  (boja_id)
) TYPE=MyISAM COMMENT='Tabela e bojrave';

--
-- Table structure for table `magazina_faturat`
--

DROP TABLE IF EXISTS magazina_faturat;
CREATE TABLE magazina_faturat (
  fature_id int(11) NOT NULL auto_increment,
  magazina varchar(10) NOT NULL default '',
  lloj_fature varchar(10) NOT NULL default '',
  nr_fature int(11) default NULL,
  date_fature date default NULL,
  date_regjistrimi timestamp(14) NOT NULL,
  date_mbyllje timestamp(14) NOT NULL,
  reparti varchar(50) default NULL,
  personi_i_autorizuar varchar(100) default NULL,
  magazinieri varchar(100) default NULL,
  marresi_ne_dorezim varchar(100) default NULL,
  transportuesi varchar(100) default NULL,
  llogaritari varchar(100) default NULL,
  PRIMARY KEY  (fature_id)
) TYPE=MyISAM COMMENT='Tabela e regjistrimit te faturave te hyrje daljeve';

--
-- Table structure for table `magazina_gjendja`
--

DROP TABLE IF EXISTS magazina_gjendja;
CREATE TABLE magazina_gjendja (
  gjendja_id int(11) NOT NULL auto_increment,
  magazina varchar(10) NOT NULL default '',
  malli varchar(255) default NULL,
  njesia varchar(10) default NULL,
  sasia float default NULL,
  cmimi float default NULL,
  PRIMARY KEY  (gjendja_id)
) TYPE=MyISAM COMMENT='Tabela e gjendjeve te artikujve';

--
-- Table structure for table `magazina_levizjet`
--

DROP TABLE IF EXISTS magazina_levizjet;
CREATE TABLE magazina_levizjet (
  levizje_id int(11) NOT NULL auto_increment,
  fature_id int(11) NOT NULL default '0',
  malli varchar(255) default NULL,
  njesia varchar(10) default NULL,
  sasia int(11) default NULL,
  cmimi int(11) default NULL,
  vlera int(11) default NULL,
  karta_teknologjike int(11) default NULL,
  PRIMARY KEY  (levizje_id)
) TYPE=MyISAM COMMENT='Tabela e regjistrimit te levizjeve ne magazine';

--
-- Table structure for table `magazina_mallrat`
--

DROP TABLE IF EXISTS magazina_mallrat;
CREATE TABLE magazina_mallrat (
  mall_id int(11) NOT NULL auto_increment,
  magazina varchar(10) NOT NULL default '',
  emertimi varchar(255) default NULL,
  njesia varchar(10) default NULL,
  sasia int(11) default NULL,
  vlera int(11) default NULL,
  PRIMARY KEY  (mall_id)
) TYPE=MyISAM COMMENT='Tabela e gjendjeve te artikujve';

--
-- Table structure for table `makinat`
--

DROP TABLE IF EXISTS makinat;
CREATE TABLE makinat (
  makina_id tinyint(3) NOT NULL default '0',
  makina varchar(30) NOT NULL default '',
  UNIQUE KEY makina_id (makina_id)
) TYPE=MyISAM;

--
-- Table structure for table `njesite`
--

DROP TABLE IF EXISTS njesite;
CREATE TABLE njesite (
  njesia_id int(11) NOT NULL default '0',
  njesia_desc varchar(50) default NULL,
  PRIMARY KEY  (njesia_id)
) TYPE=MyISAM COMMENT='tabela e njesive';

--
-- Table structure for table `normativat`
--

DROP TABLE IF EXISTS normativat;
CREATE TABLE normativat (
  id tinyint(4) NOT NULL auto_increment,
  makina char(25) NOT NULL default '0',
  lloj_pune char(40) NOT NULL default '0',
  normativat double(11,3) NOT NULL default '0.000',
  UNIQUE KEY id (id)
) TYPE=MyISAM;

--
-- Table structure for table `normativat_parapreg`
--

DROP TABLE IF EXISTS normativat_parapreg;
CREATE TABLE normativat_parapreg (
  id tinyint(4) NOT NULL auto_increment,
  lloj_pune char(60) NOT NULL default '',
  lloj_materialit char(70) default NULL,
  makina_p char(25) default NULL,
  normativat double(11,3) NOT NULL default '0.000',
  UNIQUE KEY id (id)
) TYPE=MyISAM;

--
-- Table structure for table `permasat_leter`
--

DROP TABLE IF EXISTS permasat_leter;
CREATE TABLE permasat_leter (
  permasa_leter_id tinyint(4) NOT NULL default '0',
  permasa1 int(3) NOT NULL default '0',
  permasa2 int(3) NOT NULL default '0',
  PRIMARY KEY  (permasa_leter_id)
) TYPE=MyISAM;

--
-- Table structure for table `pesha_leter`
--

DROP TABLE IF EXISTS pesha_leter;
CREATE TABLE pesha_leter (
  pesha_id tinyint(4) NOT NULL default '0',
  pesha int(3) NOT NULL default '0',
  PRIMARY KEY  (pesha_id)
) TYPE=MyISAM;

--
-- Table structure for table `proces_parapreg`
--

DROP TABLE IF EXISTS proces_parapreg;
CREATE TABLE proces_parapreg (
  proces char(30) NOT NULL default '',
  proces_id tinyint(2) NOT NULL auto_increment,
  PRIMARY KEY  (proces_id)
) TYPE=MyISAM;

--
-- Table structure for table `puna_e_kryer`
--

DROP TABLE IF EXISTS puna_e_kryer;
CREATE TABLE puna_e_kryer (
  pune_id bigint(20) NOT NULL auto_increment,
  buletin_id bigint(11) NOT NULL default '0',
  auto varchar(12) NOT NULL default '',
  kart_id int(11) NOT NULL default '0',
  lloj_pune varchar(35) NOT NULL default '',
  sasia_planifikuar varchar(50) NOT NULL default '',
  ore_planifikuar varchar(50) NOT NULL default '',
  sasia_shtypur bigint(50) NOT NULL default '0',
  ore_shtypur decimal(10,2) NOT NULL default '0.00',
  nr_rulash int(2) NOT NULL default '0',
  perqind_realizuar int(10) NOT NULL default '0',
  formati varchar(100) NOT NULL default '',
  ndarjet_letres int(2) NOT NULL default '0',
  ngjyrat mediumint(2) NOT NULL default '0',
  ana smallint(2) NOT NULL default '0',
  verejtje varchar(50) NOT NULL default '',
  shpenzimet mediumtext,
  PRIMARY KEY  (pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `punet_lidhja`
--

DROP TABLE IF EXISTS punet_lidhja;
CREATE TABLE punet_lidhja (
  pune_id bigint(20) NOT NULL auto_increment,
  buletin_id bigint(11) NOT NULL default '0',
  auto varchar(12) NOT NULL default '',
  kart_id int(11) NOT NULL default '0',
  lloj_pune varchar(35) NOT NULL default '',
  sasia_planifikuar varchar(50) NOT NULL default '',
  ore_planifikuar varchar(50) NOT NULL default '',
  sasia_shtypur bigint(50) NOT NULL default '0',
  ore_shtypur decimal(10,2) NOT NULL default '0.00',
  nr_rulash int(2) NOT NULL default '0',
  perqind_realizuar int(10) NOT NULL default '0',
  formati varchar(100) NOT NULL default '',
  ndarjet_letres int(2) NOT NULL default '0',
  ngjyrat mediumint(2) NOT NULL default '0',
  ana smallint(2) NOT NULL default '0',
  verejtje varchar(50) NOT NULL default '',
  shpenzimet mediumtext,
  PRIMARY KEY  (pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `punet_parapreg`
--

DROP TABLE IF EXISTS punet_parapreg;
CREATE TABLE punet_parapreg (
  pune_id bigint(20) NOT NULL auto_increment,
  buletin_id bigint(11) NOT NULL default '0',
  auto varchar(12) NOT NULL default '',
  kart_id int(11) NOT NULL default '0',
  lloj_pune varchar(35) NOT NULL default '',
  lloj_materialit varchar(70) NOT NULL default '',
  makina_p varchar(50) default NULL,
  sasia_shtypur bigint(50) NOT NULL default '0',
  ore_shtypur decimal(10,2) NOT NULL default '0.00',
  perqind_realizuar int(10) NOT NULL default '0',
  formati varchar(100) NOT NULL default '',
  ngjyrat mediumint(2) NOT NULL default '0',
  verejtje varchar(50) NOT NULL default '',
  shpenzimet mediumtext,
  PRIMARY KEY  (pune_id)
) TYPE=MyISAM;

--
-- Table structure for table `repartet`
--

DROP TABLE IF EXISTS repartet;
CREATE TABLE repartet (
  reparti varchar(25) NOT NULL default '',
  repart_id tinyint(4) NOT NULL auto_increment,
  UNIQUE KEY repart_id (repart_id)
) TYPE=MyISAM;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS session;
CREATE TABLE session (
  id varchar(255) NOT NULL default '',
  vars text NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `su`
--

DROP TABLE IF EXISTS su;
CREATE TABLE su (
  password varchar(50) NOT NULL default ''
) TYPE=MyISAM;

--
-- Table structure for table `titujt`
--

DROP TABLE IF EXISTS titujt;
CREATE TABLE titujt (
  titull_id varchar(20) NOT NULL default '0',
  titull varchar(50) NOT NULL default '',
  PRIMARY KEY  (titull_id)
) TYPE=MyISAM;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id int(11) NOT NULL auto_increment,
  username varchar(20) default NULL,
  password varchar(20) NOT NULL default '',
  firstname varchar(20) NOT NULL default '',
  lastname varchar(20) default NULL,
  address varchar(35) default NULL,
  tel1 varchar(20) default NULL,
  paga double(10,2) default '0.00',
  e_mail varchar(25) default NULL,
  titulli varchar(25) NOT NULL default '',
  notes text,
  access_rights text,
  PRIMARY KEY  (user_id)
) TYPE=MyISAM;

