#!/bin/bash
### generate a list of modules from the directory tree

tree -d ../templates/ | gawk '$NF!="CVS"' > modules.txt
