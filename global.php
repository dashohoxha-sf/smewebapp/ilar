<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Functions and variables that are global
 * and are used through the whole application.
 */

/**
 * Returns the current date in the requested format.
 */
function get_curr_date($format)
{
  if (TEST) 
    {
      return simulated_date($format);
    }
  else
    {
      return date($format);
    }
}

/**
 * Converts a month number to format MMM, e.g. from 1 to Jan.
 */
function int2mon($m_id)
{
  $mon = date("M", mktime(0,0,0,$m_id,1,2002));
  return $mon;
}

/** 
 * Converts a MMM month format to integer, e.g. from Jan to 1. 
 */
function mon2int($mon)
{
  return date("n", strtotime("$mon 01, 2002"));
}


function simulated_date($format)
{
  $day   = WebApp::getSVar("simulated_current_day");
  $month = WebApp::getSVar("simulated_current_month");
  $year  = WebApp::getSVar("simulated_current_year");

  switch ($format)
    {
    case "Y":
      return $year;
      break;
    case "m":
      return $month;
      break;
    case "M":
      return int2mon($month);
      break;
    case "n":
      return (int)$month;
      break;
    case "d":
      return $day;
      break;
    case "d/m/Y":
      return "$day/$month/$year";
      break;
    case "Y-m-d":
      return "$year-$month-$day";
    case "M d, Y":
      return int2mon($month)." $day, $year";
      break;
    }
}

/** 
 * Converts the given latex content to pdf,
 * and returns the name of the pdf file.
 */
function latex2pdf($fname, &$latex_content)
{
  //write the latex content to a file
  $uid = WebApp::getSVar('u_id');
  $fp = fopen("tmp/${fname}_${uid}.tex", 'w');
  fputs($fp, $latex_content);
  fclose($fp);

  //convert it to pdf
  shell_exec("cd tmp/ ; pdflatex ${fname}_${uid}.tex");

  //return the name of the prd file
  return "tmp/${fname}_${uid}.pdf";
}
?>