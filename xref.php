<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (c) 2004 Dashamir Hoxha, dashohoxha@users.sourceforge.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file is used to jump directly (cross reference) to a certain section 
 * of a certain book.  It is called with the id of the book, section and
 * language as parameters in the query string, like this:
 *
 * xref.php?book_id/node_id/lng
 *
 * 'node_id' and 'lng' can be ommitted (are optional), like this:
 * xref.php?book_id/node_id  or  xref.php?book_id  or  xref.php?book_id//lng
 * 
 * It opens first the application as usually, by including index.php,
 * then redirects it automatically to the specified book and section,
 * by calling GoTo().  The second parameter of GoTo() tells it to open
 * 'index.php' next, instead of 'xref.php' (this file). 
 */

/** get the interface id from the query string */
$interface_id = $_SERVER["QUERY_STRING"];

//construct the page as usually
include_once "index.php";

if ($interface_id=='docbookwiki_guide/editing-html-mode')
{
  $book_id = 'docbookwiki_guide';
  $node_id = 'editing-html-mode';
  $lng = 'en';
}
else
{
  $book_id = 'ilar_users_guide';
  $node_id = get_help_id($interface_id);
  $lng = 'al';
}

//get node path from node id
$xsl = XSLT.'edit/get_node_path.xsl';
$index_xml = BOOKS.$book_id."/index.xml";
$param_id = "--stringparam id $node_id";
$node_path = shell_exec("xsltproc $param_id $xsl $index_xml");

//build the event string
$event_args = "book_id=$book_id;node_path=$node_path;lng=$lng";
$strEvent = "event=main.docbook($event_args)";

//redirect to the specified book and node
print "<script language='javascript'>
 GoTo('main/main.html?$strEvent', 'index');
</script>";

exit(0);

function get_help_id($interface_id)
{
  switch ($interface_id)
    {
    default:
      $help_id = '';
      break;

      //Botimet
    case 'botimet/kontratat/listo':
    case 'botimet/kontratat/edit':
    case 'botimet/kontratat/krijo':
      $help_id = 'kontratat';
      break;
    case 'botimet/kartat/listo':
    case 'botimet/kartat/edit':
    case 'botimet/kartat/krijo':
      $help_id = 'kartat';
      break;

      //Buletinet
    case 'buletinet/offseti/listo':
    case 'buletinet/offseti/edit':
    case 'buletinet/offseti/krijo':
    case 'buletinet/parapreg/listo':
    case 'buletinet/parapreg/edit':
    case 'buletinet/parapreg/krijo':
    case 'buletinet/lidhja/listo':
    case 'buletinet/lidhja/edit':
    case 'buletinet/lidhja/krijo':
      $help_id = 'buletinet';
      break;

      //Magazinat
    case 'magazina/leter/faturat/listo':
    case 'magazina/leter/faturat/hyrje':
    case 'magazina/leter/faturat/dalje':
    case 'magazina/leter/faturat/edit':
    case 'magazina/boje/faturat/listo':
    case 'magazina/boje/faturat/hyrje':
    case 'magazina/boje/faturat/dalje':
    case 'magazina/boje/faturat/edit':
    case 'magazina/produkt/faturat/listo':
    case 'magazina/produkt/faturat/hyrje':
    case 'magazina/produkt/faturat/dalje':
    case 'magazina/produkt/faturat/edit':
      $help_id = 'mgz-faturat';
      break;
    case 'magazina/leter/mallrat/listo':
    case 'magazina/boje/mallrat/listo':
    case 'magazina/produkt/mallrat/listo':
      $help_id = 'mgz-gjendja';
      break;
    case 'magazina/leter/levizjet':
    case 'magazina/boje/levizjet':
    case 'magazina/produkt/levizjet':
      $help_id = 'mgz-levizjet';
      break;

      //Raportet-->Shpenzimet
    case 'rpt_shpenzimet':
    case 'rpt_shpenzimet/sasi':
    case 'rpt_shpenzimet/sasi/offseti':
    case 'rpt_shpenzimet/sasi/parapreg':
    case 'rpt_shpenzimet/sasi/lidhja':
    case 'rpt_shpenzimet/vlere':
    case 'rpt_shpenzimet/vlere/offseti':
    case 'rpt_shpenzimet/vlere/parapreg':
    case 'rpt_shpenzimet/vlere/lidhja':
    case 'rpt_shpenzimet/sasi_vlere':
    case 'rpt_shpenzimet/sasi_vlere/offseti':
    case 'rpt_shpenzimet/sasi_vlere/parapreg':
    case 'rpt_shpenzimet/sasi_vlere/lidhja':
      $help_id = 'rpt-shpenzimet';
      break;

      //Raportet-->Punet
    case 'rpt_punet/parapreg':
    case 'rpt_punet/offseti':
    case 'rpt_punet/lidhja':
    case 'rpt_punet/permbledhese':
      $help_id = 'rpt-punet';
      break;

      //Koeficientet
    case 'koeficientet/offseti':
    case 'koeficientet/parapreg':
    case 'koeficientet/lidhja':
      $help_id = 'koef-teknologjike';
      break;
    case 'koeficientet/financiare':
      $help_id = 'koef-financiare';
      break;

      //Administrim
    case 'superuser':
      $help_id = 'adm-superuser';
      break;
    case 'user_profile':
      $help_id = 'tp-perdoruesi';
      break;
    case 'ndrysho_daten':
      $help_id = 'tp-data';
      break;
    case 'perdoruesit':
      $help_id = 'adm-perdoruesit';
      break;
    case 'punetoret':
    case 'punetoret/parapreg':
    case 'punetoret/offseti':
    case 'punetoret/lidhja':
      $help_id = 'adm-punetoret';
      break;
    case 'backup_restore':
      $help_id = 'adm-backup_restore';
      break;
    case 'tabelat':
      $help_id = 'adm-tabelat';
      break;
    }

  return $help_id;
}
?>