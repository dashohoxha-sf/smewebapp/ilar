<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al
*/

if (TEST)
{
  WebApp::addSVar("simulated_current_day", date("d"));
  WebApp::addSVar("simulated_current_month", date("m"));
  WebApp::addSVar("simulated_current_year", date("Y"));
}
?>