<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package main
 */
class main extends WebObject
{
  /**
   * Returns true when the current user has no rights to access 
   * the given interface.
   */ 
  function has_no_access($interface)
    {
      $u_id = WebApp::getSVar('u_id');
      if ($u_id==0)  return false; //superuser has no restrictions

      if ($interface=='user_profile')  return false;

      $access_rights = WebApp::getSVar('access_rights');
      $arr_access_rights = explode(',', $access_rights);
      $has_access = in_array($interface, $arr_access_rights);

      return !$has_access;
    }

  function on_select($event_args)
    {
      $interface = $event_args['interface'];
      if ($this->has_no_access($interface))
        {
          $msg = "Nuk ke te drejte ta perdoresh nderfaqesin '$interface'.";
          WebApp::message($msg);
          return;
        }

      //user can access this interface

      WebApp::setSVar('interface', $interface);

      switch ($interface)
        {
        default:
          WebApp::message("Unknown interface '$interface'.");
          return;
          break;

          //Botimet
        case 'botimet/ekonom_shp':
          $interface_title = 'Ekonomik i Shpejte';
          $module = 'botimet/ekonomiku/ekonom_shp.html';
          break;
        case 'botimet/ekonomiku':
          $interface_title = 'Ekonomiku';
          $module = 'botimet/ekonomiku/ekonomiku.html';
          break;
        case 'botimet/kartat/listo':
        case 'botimet/kartat/edit':
        case 'botimet/kartat/krijo':
        case 'botimet/kontratat/listo':
        case 'botimet/kontratat/edit':
        case 'botimet/kontratat/krijo':
          $interface_title = 'Botimet';
          $module = 'botimet/botimet.html';
          break;

          //Buletinet
        case 'buletinet/offseti/listo':
        case 'buletinet/offseti/edit':
        case 'buletinet/offseti/krijo':
        case 'buletinet/rrotative/listo':
        case 'buletinet/rrotative/edit':
        case 'buletinet/rrotative/krijo':
        case 'buletinet/parapreg/listo':
        case 'buletinet/parapreg/edit':
        case 'buletinet/parapreg/krijo':
        case 'buletinet/lidhja/listo':
        case 'buletinet/lidhja/edit':
        case 'buletinet/lidhja/krijo':
          $interface_title = 'Buletinet';
          $module = 'buletinet/buletinet.html';
          break;

          //Magazinat
        case 'magazina/leter/faturat/listo':
        case 'magazina/leter/faturat/hyrje':
        case 'magazina/leter/faturat/dalje':
        case 'magazina/leter/faturat/edit':
        case 'magazina/leter/mallrat/listo':
        case 'magazina/leter/levizjet':
        case 'magazina/boje/faturat/listo':
        case 'magazina/boje/faturat/hyrje':
        case 'magazina/boje/faturat/dalje':
        case 'magazina/boje/faturat/edit':
        case 'magazina/boje/mallrat/listo':
        case 'magazina/boje/levizjet':
          $interface_title = 'Magazina';
          $module = 'magazina/magazina.html';
          break;

          //Raportet-->Shpenzimet
        case 'rpt_shpenzimet':
          $interface_title = 'Raportet-->Shpenzimet Permbledhese';
          $module = 'raportet/shpenzimet/permbledhese/raport.html';
          break;
        case 'rpt_shpenzimet/sasi':
        case 'rpt_shpenzimet/sasi/offseti':
        case 'rpt_shpenzimet/sasi/rrotative':
        case 'rpt_shpenzimet/sasi/parapreg':
        case 'rpt_shpenzimet/sasi/lidhja':
        case 'rpt_shpenzimet/vlere':
        case 'rpt_shpenzimet/vlere/offseti':
        case 'rpt_shpenzimet/vlere/rrotative':
        case 'rpt_shpenzimet/vlere/parapreg':
        case 'rpt_shpenzimet/vlere/lidhja':
        case 'rpt_shpenzimet/sasi_vlere':
        case 'rpt_shpenzimet/sasi_vlere/offseti':
        case 'rpt_shpenzimet/sasi_vlere/rrotative':
        case 'rpt_shpenzimet/sasi_vlere/parapreg':
        case 'rpt_shpenzimet/sasi_vlere/lidhja':
          $interface_title = 'Raportet-->Shpenzimet';
          $module = 'raportet/shpenzimet/shpenzimet.html';
          break;

          //Raportet-->Punet
        case 'rpt_punet/parapreg':
        case 'rpt_punet/offseti':
        case 'rpt_punet/rrotative':
        case 'rpt_punet/lidhja':
        case 'rpt_punet/permbledhese':
          $interface_title = 'Raportet-->Punet';
          $module = 'raportet/punet/rpt_punet.html';
          break;

          //Koeficientet
        case 'koeficientet/offseti':
        case 'koeficientet/parapreg':
        case 'koeficientet/lidhja':
        case 'koeficientet/financiare':
          $interface_title = 'Koeficientet-->Offseti';
          $module  = 'koeficientet/koeficientet.html';
          break;

          //Administrim
        case 'superuser':
        case 'user_profile':
        case 'ndrysho_daten':
        case 'perdoruesit':
        case 'punetoret':
        case 'punetoret/parapreg':
        case 'punetoret/offseti':
        case 'punetoret/lidhja':
        case 'backup_restore':
        case 'tabelat':
          $interface_title = 'Administrim';
          $module  = 'administrim/administrim.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);    
      WebApp::setSVar('module', $module);    
    }
}
?>