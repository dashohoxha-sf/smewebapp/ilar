<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al
*/

class header extends WebObject
{
  function onRender()
    {
      WebApp::addVar("current_date", get_curr_date("M d, Y"));
    }
}
?>