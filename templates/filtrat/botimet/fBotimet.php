<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage botimet
*/

class fBotimet extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "autorizimi"    => "",
                             "titull_botimi" => "",
                             "porositesi"    => "",
                             "formati_botimit"=>""
                             ));
      $this->addSVar("filter", "");
    }

  function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }

  function get_filter()
    {
      $autorizimi = $this->getSVar("autorizimi");
      $titull_botimi = $this->getSVar("titull_botimi");
      $porositesi = $this->getSVar("porositesi");
      $formati_botimit = $this->getSVar("formati_botimit");

      $conditions = array();
      if ($autorizimi<>"")
        $conditions[] = "autorizim_nr='$autorizimi'";
      if ($titull_botimi<>"")
        $conditions[] = "titull_botimi Like '%".$titull_botimi."%'";

      if ($porositesi<>"")
        $conditions[] = "porositesi Like '%".$porositesi."%'";

      if ($formati_botimit<>"")
        $conditions[] = "formati_botimit Like '%".$formati_botimit."%'";
 
      $filter = implode(" AND ", $conditions);
      if ($filter<>"")  $filter = "(".$filter.")";
      return $filter;
    }
}
?>