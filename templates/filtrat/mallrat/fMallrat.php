<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage mallrat
*/

class fMallrat extends WebObject
{
  /*function init()
    {
      $this->addSVar("malli", "");
      $this->addSVar("njesia", "");
      $this->addSVar("filter", "");
    }*/
  function init()
    {
      $this->addSVars( array(
	  						 "malli"   => "",
							 "malli1"   => "",
							 "permasa1"   => "",
							 "permasa2"   => "",
							 "pesha"   => "",
                             "njesia"     => "",
							 "sasia"     => "",
                             ));
      $this->addSVar("filter", "");
	  //WebApp::message('init');
    }
	function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }

  function get_filter()
  //function onParse()
    {
      //$filter = "1=1";
      //ToDo: construct the filter

	  $magazina = WebApp::getSVar("magazina->id");
	  if ($magazina=='leter')
	  {
	  	//echo WebApp::getSVar("fMallrat->njesia");
		//echo  $this->getSVar("njesia");
	  	$malli = WebApp::getSVar("fMallrat->malli");
	  }
	  else
	  	$malli = WebApp::getSVar("fMallrat->malli");

      //$njesia = WebApp::getSVar("fMallrat->njesia");
	  $sasia = $this->getSVar("sasia");
      $njesia = $this->getSVar("njesia");

      $conditions = array();
	  //if ($magazina<>"")   $conditions[] = "magazina ='".$magazina."'";
	  if ($malli<>"")
        $conditions[] = "malli Like '%".$malli."%'";
      if ($sasia<>"")
        $conditions[] = "sasia='".$sasia."'";

      if ($njesia<>"")
        $conditions[] = "njesia Like '%".$njesia."%'";

      $filter = implode(" AND ", $conditions);
      if ($filter<>"")  $filter = "(".$filter.")";
	  //echo $filter;
      return $filter;
      //$this->setSVar("filter", $filter);
    }
}
?>
