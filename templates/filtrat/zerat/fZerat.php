<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage zerat
*/

class fZerat extends WebObject
{
  function init()
    {
      $this->addSVar("selected", "");
      $this->addSVar("labels", "");
    }


  function onRender()
    {
      $reparti = WebApp::getSVar("shpenzimet->reparti");
      $fname = dirname(__FILE__)."/zerat_".$reparti.".php";
      include_once $fname;

      $rs_zerat = new EditableRS("fZerat_rs");
      while ( list($name,$label) = each($arr_zerat) )
        {
          $rs_zerat->addRec(compact("name","label"));
        }

      global $webPage;
      $webPage->addRecordset($rs_zerat);
    }

  //called inside the <Repeat> template (by the <Var> element)
  function checked($chkbox)
    {
      $selected = WebApp::getSVar("fZerat->selected");
      $arr_checked = explode(',', $selected);
      if (in_array($chkbox, $arr_checked))
        return "checked";
      else
        return "";
    }
}
?>