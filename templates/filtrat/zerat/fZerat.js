// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb  is free  software; you  can redistribute  it and/or  modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWeb is distributed in the hope  that it will be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along  with SMEWeb;  if not,  write to  the Free  Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function set_checked_boxes()
{
  var zerat = document.fZerat
  var checked_boxes_names = new Array;
  var checked_boxes_values = new Array;

  for(i=0; zerat[i]; i++)
    {
      if (zerat[i].checked)
        {
          checked_boxes_names.push(zerat[i].name);
          checked_boxes_values.push(zerat[i].value);
        }
    }

  session.setVar("fZerat->selected", checked_boxes_names.join());
  session.setVar("fZerat->labels", checked_boxes_values.join());
}
