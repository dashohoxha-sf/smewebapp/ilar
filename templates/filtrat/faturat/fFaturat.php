<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage faturat
*/

class fFaturat extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "lloj_fature" => "",
                             "nr_fature"   => "",
                             "magazinieri"     => ""
                             ));
      $this->addSVar("filter", "");
    }

  function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }

  function get_filter()
    {
      //ToDo: rregullo filtrin
//echo $this->getSVar("lloj_fature");
      $lloj_fature = $this->getSVar("lloj_fature");
      $nr_fature = $this->getSVar("nr_fature");
      $magazinieri = $this->getSVar("magazinieri");

		$boje_leter_filter=WebApp::getSVar("magazina->id");

      $conditions = array();
      if ($lloj_fature<>"")
        $conditions[] = "lloj_fature='$lloj_fature'";
      if ($nr_fature<>"")
        $conditions[] = "nr_fature='$nr_fature'";

      if ($magazinieri<>"")
        $conditions[] = "magazinieri Like '%".$magazinieri."%'";

	  if ($boje_leter_filter<>"")
	  	$conditions[] = "magazina = '$boje_leter_filter'";

      $filter = implode(" AND ", $conditions);
      if ($filter<>"")  $filter = "(".$filter.")";
//	  echo $filter;
      return $filter;
    }
	 
}
?>