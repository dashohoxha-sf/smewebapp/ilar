// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function set_titull_botimi(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->titull_botimi", textbox.value);
}



function set_porositesi(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->porositesi", textbox.value);
}


function set_gjinia(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->gjinia", textbox.value);
}


function set_formati_botimit(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->formati_botimit", textbox.value);
}



function set_nr_faqesh(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->nr_faqesh", textbox.value);
}


function set_kopjet(textbox)
{
  //alert(textbox.value);
  session.setVar("fKartat->kopjet", textbox.value);
}
