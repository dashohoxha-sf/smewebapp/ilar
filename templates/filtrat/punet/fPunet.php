<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage punet
*/

class fPunet extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "kart_id" => "",
                             "titull_botimi" => "",
                             "lloj_pune" => "",
                             "formati" => ""
                             ) );
      $this->addSVar("filter", "");
    }

  function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }

  function get_filter()
    {
      $kart_id = $this->getSVar("kart_id");
      $titull_botimi = $this->getSVar("titull_botimi");
      $lloj_pune    = $this->getSVar("lloj_pune");
      $formati = $this->getSVar("formati");

      $conditions = array();

      if ($kart_id<>"")
        $conditions[] = "kartat_teknologjike.kart_id='".$kart_id."'";

      if ($titull_botimi<>"")
        $conditions[] = "titull_botimi LIKE '%".$titull_botimi."%'";
    
      if ($lloj_pune<>"")
        $conditions[] = "lloj_pune='".$lloj_pune."'";
    
      if ($formati<>"")
        $conditions[] = "puna_e_kryer.formati LIKE '%".$formati."%'";

      $filter = implode(" AND ", $conditions);
      if ($filter<>"")    $filter = "(".$filter.")";
      return $filter;

    }
}
?>