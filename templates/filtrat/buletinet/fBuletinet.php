<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package filtrat
* @subpackage buletinet
*/

class fBuletinet extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             "makina" => "",
                             "makinisti" => "",
                             "turni" => "",
                             "proces" => ""
                             ) );
      $this->addSVar("filter", "");
    }
    
    
  
  function onParse()
    {
      $this->setSVar("filter", $this->get_filter());
    }  
    
    
  function get_filter()
    {
      //gjej repartin
      $interface = WebApp::getSVar('interface');
      if (ereg('rpt_punet/', $interface))  //filtri perdoret te punet
        {
          $reparti = WebApp::getSVar('rpt_punet->reparti');  
        }
      else    //filtri perdoret te buletinet
        {
      $reparti = WebApp::getSVar('buletinet->reparti');  
        }
        
      $makina    = $this->getSVar("makina");
      $makinisti = $this->getSVar("makinisti");
      $turni    = $this->getSVar("turni");
      $proces    = $this->getSVar("proces");
      
      $conditions = array();

      if ($reparti=="offseti")
        {
          $conditions[] = "makina!='Rrotative'";
          if ($makina<>"") $conditions[] = "makina='$makina'";
        }

      if ($reparti=="rrotative")
        {
          $conditions[] = "makina='Rrotative'";
         }
         
      if ($reparti=="parapreg")
        {
          if ($proces<>"") $conditions[] = "proces LIKE '%".$proces."%'";
         }   

      if ($makinisti<>"")
        $conditions[] = "makinisti LIKE '%".$makinisti."%'";
      
      if ($turni<>"")
        $conditions[] = "turni='$turni'";

         
      $filter = implode(" AND ", $conditions);
      if ($filter<>"")    $filter = "(".$filter.")";
      return $filter;
          
    }
}
?>  

  
   


       
  
