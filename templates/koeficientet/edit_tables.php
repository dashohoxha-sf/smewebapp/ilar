<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package koeficientet
*/

class edit_tables extends WebObject
{
  function init()
    {
      $this->addSVar('one_table', 'true');  //true|false

      //for the case of one table
      $this->addSVar('table', UNDEFINED);
      $this->addSVar('fields', '*'); //display these fields of the table

      //for the case of two tables
      $this->addSVar('table1', UNDEFINED);
      $this->addSVar('fields1', '*');
      //filters the records of table2 according 
      //to the value that is selected in table1
      $this->addSVar('filter', '');  
      $this->addSVar('table2', UNDEFINED);
      $this->addSVar('fields2', '*');
    }

  function onParse()
    {
      $reparti = WebApp::getSVar('koeficientet->reparti');
      $table = WebApp::getSVar("tabs3::${reparti}->selected_item");

      //general case
      $this->setSVar('table', $table);
      $this->setSVar('one_table', 'true');
      $this->setSVar('table1', UNDEFINED);
      $this->setSVar('table2', UNDEFINED);         

      //special case
      if ($table=='normativat')
        {
          $this->setSVar('one_table', 'false');
          $this->setSVar('table1', 'makinat');
          $this->setSVar('table2', 'normativat');         
          $this->setSVar('filter', "makina='{{makina}}'");         
        }

      //special case
      //...
    }
}
?>