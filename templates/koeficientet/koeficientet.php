<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package koeficientet
*/

class koeficientet extends WebObject
{
  function init()
    {
      //reparti: offseti|rrotative|parapreg|lidhja|financiare
      $this->addSVar('reparti', 'offseti');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_koeficient_interface($event->args);
        }
    }

  function select_koeficient_interface($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
        case 'koeficientet/offseti':
          $interface_title = 'Koeficientet Teknologjike-->Offseti';
          $this->setSVar('reparti', 'offseti');
          break;
        case 'koeficientet/parapreg':
          $interface_title = 'Koeficientet Teknologjike-->Parapregatitja';
          $this->setSVar('reparti', 'parapreg');
          break;
        case 'koeficientet/lidhja':
          $interface_title = 'Koeficientet Teknologjike-->Liberlidhja';
          $this->setSVar('reparti', 'lidhja');
          break;
        case 'koeficientet/financiare':
          $interface_title = 'Koeficientet Financiare';
          $this->setSVar('reparti', 'financiare');
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
    }
}
?>