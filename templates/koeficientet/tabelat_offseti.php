<?php
/** 
 * The $menu_items array contains the items of the tabs3::koeficientet. 
 */
$menu_items = array(
            "normativat"      => "Normativat",
            "makinat"         => "Makinat",
            "llojet_e_puneve" => "Llojet E Puneve",
            "koeficientet"    => "Koeficientet",
            "lloji_leter"     => "Lloji i Letres",
            "pesha_leter"     => "Pesha e Letres",
            "permasat_leter"  => "Permasat e Letres",
            "gjinia"          => "Gjinia"
            );
?>
