<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Called at the login time.
 */

 /**
* @package login
*/

if (!valid_user())
{	
  //the user is not authenticated, go to the info page
  $event->targetPage = 'login/login.html';
  $event->target = 'login';
  $event->name = 'PasswdError';
  WebApp::addGlobalVar('login_error', 'true');
  WebApp::message('Perdorues ose fjalekalim i gabuar!');
}
else
{
  // add some state variables and create the user menus
  init_user();
  $first_interface = create_user_menu();

  //open the main page
  $event->targetPage = 'main/main.html';
  $event->target = 'main';
  $event->name = 'select';
  $event->args['interface'] = $first_interface;
}

/** 
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  global $event;
  $username = $event->args['username'];
  $password = $event->args['password'];

  if ($username=='superuser')
    {
      $query = 'SELECT password FROM su';
    }
  else
    {
      $query = "SELECT * FROM users WHERE username='$username'";
    }
  $rs = new StaticRS('user_data', $query);
  global $webPage;
  $webPage->addRecordset($rs);

  $rs = WebApp::openRS('user_data');

  if ($rs->EOF())  return false; //query returned no records

  $passwd = $rs->Field('password');

  return ($passwd==$password);
}

/**
 * Add some state variables and create the user menus.
 */
function init_user()
{
  global $event;
  $username = $event->args['username'];
  $rs = WebApp::openRS('user_data');

  WebApp::addSVar('username', $username, 'DB');
  WebApp::addSVar('password', $rs->Field('password'), 'DB');

  if ($username=='superuser')
    {
      WebApp::addSVar('u_id', '0', 'DB');
    }
  else
    {
      //save some data in the session 
      WebApp::addSVar('u_id', $rs->Field('user_id'), 'DB');
      WebApp::addSVar('firstname', $rs->Field('firstname'));
      WebApp::addSVar('lastname', $rs->Field('lastname'));
      add_access_rights($rs->Field('access_rights'));
    }
}

/**
 * According to the selected interfaces, add some more interfaces
 * according to a certain logic (e.g. if somebody has the right 
 * to create a new record, then he should be able to edit 
 * the records as well). then add the state variable access_rights.
 */
function add_access_rights($access_rights)
{
  $arr_access_rights = explode(',', $access_rights);

  //botimet
  if (in_array('botimet/kartat/krijo', $arr_access_rights))
    {
      $arr_access_rights[] = 'botimet/kartat/edit';
    }
  if (in_array('botimet/kontratat/krijo', $arr_access_rights))
    {
      $arr_access_rights[] = 'botimet/kontratat/edit';
    }

  //buletinet
  if (in_array('buletinet/offseti/krijo', $arr_access_rights))
    {
      $arr_access_rights[] = 'buletinet/offseti/edit';
    }
  if (in_array('buletinet/parapreg/krijo', $arr_access_rights))
    {
      $arr_access_rights[] = 'buletinet/parapreg/edit';
    }
  if (in_array('buletinet/lidhja/krijo', $arr_access_rights))
    {
      $arr_access_rights[] = 'buletinet/lidhja/edit';
    }

  //magazina
  if (in_array('magazina/leter/faturat/hyrje', $arr_access_rights))
    {
      $arr_access_rights[] = 'magazina/leter/faturat/edit';
    }
  if (in_array('magazina/boje/faturat/hyrje', $arr_access_rights))
    {
      $arr_access_rights[] = 'magazina/boje/faturat/edit';
    }

  $new_access_rights = implode(',', $arr_access_rights);
  WebApp::addSVar('access_rights', $new_access_rights, 'DB');
}

/**
 * Creates the JS menu according to the access rights
 * of the user and save it in a cache file. Returns
 * the first interface that will be displayed to the user.
 */
function create_user_menu()
{
  include_once MENU_PATH.'class.MenuRoot.php';
  include_once MENU_PATH.'class.XMLMenu.php';

  //parse menu from XML file and create the menu tree structure
  $xmlMenu = new XMLMenu(MENU_PATH.'menu.xml');
  $menu = $xmlMenu->getMenu();

  
  $u_id = WebApp::getSVar('u_id');
  if ($u_id==0) //superuser
    {
      //add one more item for the superuser
      $item = new MenuItem('superuser', 'Superuser');
      $nr = $menu->nr_children();
      $menu->children[$nr-1]->add_child($item);

      $first_interface = 'superuser';
    }
  else
    {
      //filter the menu according to user access rights 
      $access_rights = WebApp::getSVar('access_rights');
      $menu = $menu->filter($access_rights);
      $menu = $menu->compact();

      //get the first interface
      $item = &$menu;
      while (($item->link=='null') and ($item->nr_children() > 0))
        {
          $item = &$item->children[0];
        }
      if ($item->link<>'null')  $first_interface = $item->id;
      else
        {
          $first_interface = 'user_profile';
          $msg = "Ju nuk keni te drejta ne program,\n"
            . " pervecse te ndryshoni te dhenat personale.";
          WebApp::message($msg);
        }
    }

  //write menu to a cache file, according to the user id
  $menu->write_js(MENU_PATH."/cache/menu_items_$u_id.js");

  return $first_interface;
}
?>