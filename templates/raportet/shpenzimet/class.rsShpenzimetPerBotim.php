<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__)."/class.rsShpenzimet.php";

/**
 * @package raportet
 * @subpackage shpenzimet
 */
 
class rsShpenzimetPerBotim extends rsShpenzimet
{
  /**
   * Filtri qe zgjedh botimet qe do dalin ne raport.
   * Perdoret te getBotimet dhe te getBotimet_print.
   */
  function get_filter_condition()
    {
      $data_filter = WebApp::getSVar("data->filter");
      $data_filter = str_replace("date_field", "data", $data_filter);
      $botimet_filter = WebApp::getSVar("fBotimet->filter");
      $conditions[] = $data_filter;
      if ($botimet_filter<>"")   $conditions[] = $botimet_filter;

      $filter_condition = implode(" AND ", $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }

  /** merr punet per listen e zgjedhur te botimeve */
  function get_punet_rs($liste_botimesh, $reparti)
  {
    switch ($reparti)
      {
      case "offseti":
        $tabela_punet = "puna_e_kryer";
        break;
      case "rrotative":
        $tabela_punet = "puna_e_kryer";
        break;
      case "parapreg":
        $tabela_punet = "punet_parapreg";
        break;
      case "lidhja":
        $tabela_punet = "punet_lidhja";
        break;
      default:
        $msg = "rsShpenzimetPerBotim::get_punet_rs(): "
          . "reparti '$reparti' eshte i panjohur.";
        print WebApp::error_msg($msg);
        return;
        break;
      }
    $params = compact("tabela_punet", "liste_botimesh");
    $punet_rs = WebApp::openRS("getPunet", $params);

    return $punet_rs;
  }
}
?>