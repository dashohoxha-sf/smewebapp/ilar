<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rshp_zerat extends WebObject
{
  function onRender()
    {
      //create an empty array
      $arr_zerat = array();

      //get punet in the selected timeframe and add them up to the recordset
      $filter_condition = $this->get_filter_condition();
      $rs = WebApp::openRS('punet_offseti', compact('filter_condition'));
      $this->mblidh_shpenzimet($arr_zerat, $rs);
      $rs = WebApp::openRS('punet_parapreg', compact('filter_condition'));
      $this->mblidh_shpenzimet($arr_zerat, $rs);
      //$rs = WebApp::openRS('punet_lidhja', compact('filter_condition'));
      //$this->mblidh_shpenzimet($arr_zerat, $rs);

      //create the recordset that will fill the template
      $rs_leter = new EditableRS('shpenzimet_zerat');
      while (list($zeri,$arr_shp) = each($arr_zerat))
        {
          if ($zeri=='paga')    continue;
          if ($zeri=='pagap')   continue;
          if ($zeri=='leter')   continue;
          if ($zeri=='leterp')  continue;
          $rec['zeri'] = $zeri;
          $rec['sasi'] = round($arr_shp['sasi'], 2);
          $rec['vlere'] = round($arr_shp['vlere'], 2);
          $rs_leter->addRec($rec);
        }

      global $webPage;
      $webPage->addRecordset($rs_leter);
    }

  function get_filter_condition()
  {
    $data_filter = WebApp::getSVar("data->filter");
    $data_filter = str_replace("date_field", "data", $data_filter);

    return $data_filter;
  }

  /**
   * Mbledh shpenzimet qe ndodhen ne recordsetin $rs
   * te array $arr_zerat, sipas zerave perkates.
   */
  function mblidh_shpenzimet(&$arr_zerat, &$rs)
  {
    while (!$rs->EOF())
      {
        $str_shpenzimet = $rs->Field('shpenzimet');
        $arr_shpenzimet = unserialize($str_shpenzimet);
        if (!is_array($arr_shpenzimet))
          {
            $rs->MoveNext();
            continue;
          }

        //mblidh shpenzimet te $arr_zerat sipas zerave
        while (list($ze_shpenzimi,$vlera_numerike) = each($arr_shpenzimet))
          {
            list($zeri, $sasi_vlere) = explode('_', $ze_shpenzimi);
            if ($zeri=='letra') continue;
            if ($sasi_vlere!='sasi' and $sasi_vlere!='vlere')  continue;
            $arr_zerat[$zeri][$sasi_vlere] += $vlera_numerike;
          }

        $rs->MoveNext();
      }
  }
}
?>