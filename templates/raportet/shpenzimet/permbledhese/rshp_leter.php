<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rshp_leter extends WebObject
{
  function onRender()
    {
      //create an emty array
      $arr_leter = array();

      //get punet in the selected timeframe and add them up to the recordset
      $filter_condition = $this->get_filter_condition();
      $rs = WebApp::openRS('punet_leter', compact('filter_condition'));
      while (!$rs->EOF())
        {
          $formati = $rs->Field('formati');
          $str_shpenzimet = $rs->Field('shpenzimet');
          $arr_shpenzimet = unserialize($str_shpenzimet);

          //add shpenzimet
          $arr_leter[$formati]['flete_tipografike'] 
            += $arr_shpenzimet['flete_tipografike'];
          $arr_leter[$formati]['letra_sasi'] 
            += $arr_shpenzimet['letra_sasi'];
          $arr_leter[$formati]['letra_vlere'] 
            += $arr_shpenzimet['letra_vlere'];

          $rs->MoveNext();
        }

      //create the recordset that will fill the template
      $rs_leter = new EditableRS('shpenzimet_leter');
      while (list($formati,$arr_shp) = each($arr_leter))
        {
          if ($formati=='')  continue;
          $rec['formati'] = $formati;
          $rec['flete_tipografike'] = round($arr_shp['flete_tipografike'], 2);
          $rec['letra_sasi'] = round($arr_shp['letra_sasi'], 2);
          $rec['letra_vlere'] = round($arr_shp['letra_vlere'], 2);
          $rs_leter->addRec($rec);
        }

      global $webPage;
      $webPage->addRecordset($rs_leter);
    }

  function get_filter_condition()
  {
    $data_filter = WebApp::getSVar("data->filter");
    $data_filter = str_replace("date_field", "data", $data_filter);

    return $data_filter;
  }
}
?>