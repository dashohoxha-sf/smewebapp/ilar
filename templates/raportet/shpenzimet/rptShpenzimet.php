<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//
// ToDo: Ndrysho filtrat/zerat/zerat_parapreg.php,
//               filtrat/zerat/zerat_lidhja.php,
//               filtrat/zerat/zerat_all.php,
//       Ndrysho raportet/shpenzimet/zerat_parapreg.php,
//               raportet/shpenzimet/zerat_lidhja.php,
//               raportet/shpenzimet/zerat_all.php,
//
//       Zerat te shpenzimet duhet te perputhen me zerat
//       e llogaritur kur shtohet nje pune e re.  Keta
//       lidhen me zerat e filtrit duke i shtuar zerit te
//       filtrit nje '_sasi' ose '_vlere' nga pas (ose te
//       dyja).  
//
//       Ka rendesi qe te gjitha shpenzimet vlere te
//       mbarojne me prapashtesen vlere, p.sh. 'paga_vlere',
//       'shpenzime_shtese_vlere', etj.  Kjo eshte e
//       rendesishme sepse kur llogaritet shuma ose totali,
//       mblidhen te gjitha shpenzimet qe mbarojne me '_vlere'. 
//
//       Gjithashtu, te raportet/shpenzimet/shpenzimet.php,
//       ndrysho zerat e zgjedhur fillimisht (by default) per
//       cdo raport (te funksioni select_report()).
//

/**
 * @package raportet
 * @subpackage shpenzimet
 */
class rptShpenzimet extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("getBotimet->current_page", $page);
    }

  function onRender()
    {
      WebApp::setSVar("getBotimet->recount", "true");

      $this->add_template_recordsets();
      $rs = $this->get_shpenzimet_rs();
      $this->add_totalet_per_botim($rs);
      $this->add_totalet_per_ze($rs);
      $this->reformat_fields($rs);
      //WebApp::debug_msg($rs->toHtmlTable()); //debug

      global $webPage;
      $rs->ID = "rptShpenzimet_rs";
      $webPage->addRecordset($rs);
    }

  function get_shpenzimet_rs()
  {
    $lloj_raporti = WebApp::getSVar('shpenzimet->lloj_raporti');
    if ($lloj_raporti=='per_botim')
      {
        include_once dirname(__FILE__)."/class.rsShpenzimetPerBotim.php";
        $shpenzimet = new rsShpenzimetPerBotim;
      }
    else //per buletin
      {
        include_once dirname(__FILE__)."/class.rsShpenzimetPerBuletin.php";
        $shpenzimet = new rsShpenzimetPerBuletin;
      }

    return $shpenzimet->get_shpenzimet_rs();
  }

  /**
   * Nderto dhe shto ne faqe rekordsetet e templetit:
   *  - header1(header,colspan,rowspan)
   *  - header2(header)
   *  - fushat(fusha)
   *  - totalet(fusha,totali,colspan)
   * te cilat perdoren per te ndertuar faqen e raportit.
   */
  function add_template_recordsets()
    {
      //krijo rekordsetet e templetit
      $header1 = new EditableRS("header1");
      $header2 = new EditableRS("header2");
      $fushat  = new EditableRS("fushat");
      $totalet = new EditableRS("totalet");

      //merr zerat e zgjedhur
      $zerat = WebApp::getSVar("fZerat->selected");
      $ze_labels = WebApp::getSVar("fZerat->labels");
      if ($zerat=='')
        {
          $arr_zerat = array();
          $arr_labels = array();
        }
      else
        {
          $arr_zerat = explode(",", $zerat);
          $arr_labels = explode(",", $ze_labels);
        }

      //shto kollonat per cdo ze te zgjedhur
      for ($i=0; $i < sizeof($arr_zerat); $i++)
        {
          $zeri = $arr_zerat[$i];
          $label = $arr_labels[$i];
          if ($zeri=="letra")
            {
              //shto kollonat e vecanta te letres
              $this->add_columns_letra($header1,$header2,$fushat,$totalet);
            }
          
          if ($zeri=="paga" or $zeri=="shpenzimet_shtese" or $zeri=="parashikimi")
            {
              //shto kollonat e vecanta te letres
              $this->add_columns_paga_shpenzimet_shtese($header1,$header2,$fushat,$totalet,$zeri, $label);
            }
          else
          {
          //shto kollonat per ze
          $this->add_columns($header1,$header2,$fushat,$totalet,$zeri,$label);
           }
        }


      //shto ne strukturen e faqes rekordsetet e templetit
      global $webPage;
      $webPage->addRecordset($header1);
      $webPage->addRecordset($header2);
      $webPage->addRecordset($fushat);
      $webPage->addRecordset($totalet);
    }
    
  /** shto kollonat e letres */
  function add_columns_letra(&$header1, &$header2, &$fushat, &$totalet)
    {
      $header1->addRec(array("header" => "Lloj Letre",
                             "colspan" => "1",
                             "rowspan" => "2"));
      $header1->addRec(array("header" => "F.Tpgf",
                             "colspan" => "1",
                             "rowspan" => "2"));
      $fushat->addRec(array("fusha" => "formati_botimit"));
      $fushat->addRec(array("fusha" => "flete_tipografike"));
      $totalet->addRec(array("fusha" => "flete_tipografike",
                             "colspan" => "2"));
    }

function add_columns_paga_shpenzimet_shtese(&$header1, &$header2, &$fushat, &$totalet,$zeri, $ze_label)
    {
      $header1->addRec(array("header" => $ze_label,
                             "colspan" => "1",
                             "rowspan" => "2"));
      
      
      $fushat->addRec(array("fusha" => $zeri."_vlere"));
      $totalet->addRec(array("fusha"=> $zeri."_vlere",
                                 "colspan"=>"1"));
    
    }

 
  /** shto kollonat per ze */
  function add_columns(&$header1,&$header2,&$fushat,&$totalet,$zeri,$ze_label)
    {
      //merr parametrat e raportit shpenzimet
      $sasi = WebApp::getSVar("shpenzimet->sasi");
      $vlere = WebApp::getSVar("shpenzimet->vlere");

      if (($sasi=="true" and $vlere=="false")
          or ($sasi=="false" and $vlere=="true"))
        {
          $header1->addRec(array("header"  => $ze_label,
                             "colspan" => "1",
                             "rowspan" => "2"));
        }
      else if ($sasi=="true" and $vlere=="true")
        {
          
          $header1->addRec(array("header"  => $ze_label,
                             "colspan" => "2",
                             "rowspan" => "1"));
          $header2->addRec(array("header" => "sasi"));
          $header2->addRec(array("header" => "vlere"));
          
        }

      if ($sasi=="true")
        {
          //shto kollonen e sasise
          
          $fushat->addRec(array("fusha" => $zeri."_sasi"));
          $totalet->addRec(array(
                                 "fusha"=>$zeri."_sasi",
                                 "colspan"=>"1"));
        
        }

      if ($vlere=="true")
        {
          //shto kollonen e vleres
          $fushat->addRec(array("fusha" => $zeri."_vlere"));
          $totalet->addRec(array("fusha"=>$zeri."_vlere",
                                 "colspan"=>"1"));
        }
    }

  /** Reformats the fields of the given recordset. */
  function reformat_fields(&$rs)
    {
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $arr_fields = $rs->Fields();
          while ( list($field, $value) = each($arr_fields) )
            {
              //reformat the value for the $field
              if (is_numeric($value))
                {    
                  $new_value = round($value, 2);
                }
              else       //duhet rregulluar
                {
                  $new_value = $value;
                }

              //set the new value back
              $rs->setFld($field, $new_value);
            } 

          $rs->MoveNext();
        }
    }

  function add_totalet_per_ze(&$botimet_rs)
    {
      //merr rekordsetin e totaleve (qe perdoret ne templet)
      global $webPage;
      $totalet = $webPage->getRecordset("totalet");
      $totalet->addCol("totali", "0.0");  //shto kolonen "totali"

      //llogarit vleren e te gjitha totaleve
      $totalet->MoveFirst();
      while (!$totalet->EOF())
        {
          $fusha = $totalet->Field("fusha");

          //llogarit totalin e fushes $fusha per te gjitha botimet
          $totali = 0.0;
          $botimet_rs->MoveFirst();
          while (!$botimet_rs->EOF())
            {
              $vlera_per_botim = $botimet_rs->Field($fusha);
              $totali += $vlera_per_botim;
              $botimet_rs->MoveNext();
            }

          $totalet->setFld("totali", round($totali,2));
          $totalet->MoveNext();
        }

      //vendose ne faqe rekordsetin e ndryshuar 
      $webPage->addRecordset($totalet);
    }

  /**
   * Gjen shumen dhe totalin per te gjitha shpenzimet *vlere* per
   * cdo botim.  'Totali' eshte shuma e te gjitha shpenzimeve
   * vlere te botimit, pavaresisht nese shfaqen ose jo ne raport.
   * 'Shuma' eshte shuma e shpenzimeve vlere te shfaqura ne raport.
   */
  function add_totalet_per_botim(&$botimet_rs)
    {
      //nese ky raport nuk ka shpenzime vlere, atere
      //s'eshte nevoja per shumen dhe totalin 
      //(sepse s'kane kuptim per shpenzimet sasi)
      $vlere = WebApp::getSVar("shpenzimet->vlere");
      if ($vlere=="false")  return;

      //**** shto ne raport kollonat Shuma dhe Totali

      //merr nga faqja recordsetet e templetit
      global $webPage;
      $header1 = $webPage->getRecordset("header1");
      $header2 = $webPage->getRecordset("header2");
      $fushat = $webPage->getRecordset("fushat");
      $totalet = $webPage->getRecordset("totalet");

      //shto kollonen Shuma
      $header1->addRec(array("header" => "Shuma", 
                             "colspan" => "1",
                             "rowspan" => "2"));
      $fushat->addRec(array("fusha" => "shuma"));
      $botimet_rs->addCol("shuma", "0.0");
      $totalet->addRec(array("fusha" => "shuma", "colspan" => "1"));

      //shto kollonen Totali duke vendosur total/kopje
      $header1->addRec(array("header" => "�imi/cope", 
                             "colspan" => "1",
                             "rowspan" => "2"));
      $fushat->addRec(array("fusha" => "totali"));
      $botimet_rs->addCol("totali", "0.0");
      $totalet->addRec(array("fusha" => "totali", "colspan" => "1"));
      
            
      //vendos ne faqe rekordsetet e templetit
      $webPage->addRecordset($header1);
      $webPage->addRecordset($header2);
      $webPage->addRecordset($fushat);
      $webPage->addRecordset($totalet);


      //**** llogarit shumat

      //gjej fushat qe do mblidhen te shuma
      $arr_fushat = array();
      $fushat->MoveFirst();
      while (!$fushat->EOF())
        {
          $fusha = $fushat->Field("fusha");
          $arr = split('_', $fusha);
          if ($arr[sizeof($arr)-1] == 'vlere')
            {
              $arr_fushat[] = $fusha;
            }

          $fushat->MoveNext();
        }

      //llogarit shumen e shpenzimeve te shfaqura, per cdo botim
      $botimet_rs->MoveFirst();
      while (!$botimet_rs->EOF())
        {
          $shuma = 0.0;
          for ($i=0; $i < sizeof($arr_fushat); $i++)
            {
              $fusha = $arr_fushat[$i];
              $shuma += $botimet_rs->Field($fusha);
            }
          $botimet_rs->setFld("shuma", $shuma);

          $botimet_rs->MoveNext();
        }


      //**** llogarit totalet

      //gjej fushat qe do mblidhen te totali
      $arr_fushat = array();
      $botimet_rs->MoveFirst();
      $arr_rec = $botimet_rs->Fields();  //merr nje record te recordsetit
      if (!isset($arr_rec))  $arr_rec = array();
      while ( list($fusha, $vlera) = each($arr_rec) )
        {
          $arr = split('_', $fusha);
          if ($arr[sizeof($arr)-1] == 'vlere')
            {
              $arr_fushat[] = $fusha;
            }
        }

      //llogarit totalin e shpenzimeve per cdo botim
      $botimet_rs->MoveFirst();
      while (!$botimet_rs->EOF())
        {
          $totali = 0.0;
          for ($i=0; $i < sizeof($arr_fushat); $i++)
            {
              $fusha = $arr_fushat[$i];
              $totali += $botimet_rs->Field($fusha);
            }
          $kopjet = $botimet_rs->Field("kopjet");
       
       //kthim nga total ne cmim per cope te botimeve
       $totali = $totali/$kopjet;
       $totali = round($totali, 2);  
          $botimet_rs->setFld("totali", $totali);

          $botimet_rs->MoveNext();
        }
    }
}
?>
