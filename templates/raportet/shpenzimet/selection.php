<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage shpenzimet
 */
 
class selection extends WebObject
{
  function onRender()
    {
      //lloji i raportit
      $lloj_raporti = WebApp::getSVar('shpenzimet->lloj_raporti');
      if ($lloj_raporti=='per_botim')
        {
          $lloj_rpt = "Shpenzimet Per Botim";
        }
      else //per_buletin
        {
          $lloj_rpt = "Shpenzimet Sipas Buletineve";
        }
      WebApp::addVar('lloj_raporti', $lloj_rpt); 

      //filtrimi sipas periudhes kohore
      $day1 = WebApp::getSVar("data->day1");
      $month1 = WebApp::getSVar("data->month1");
      $year1 = WebApp::getSVar("data->year1");
      $day2 = WebApp::getSVar("data->day2");
      $month2 = WebApp::getSVar("data->month2");
      $year2 = WebApp::getSVar("data->year2");
      $periudha = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar("periudha", $periudha);

      //nderto selektimin e bere ne baze te botimit
      $botimet = "";
      $autorizimi = WebApp::getSVar("fBotimet->autorizimi");
      $titull_botimi = WebApp::getSVar("fBotimet->titull_botimi");
      $porositesi = WebApp::getSVar("fBotimet->porositesi");
      $formati_botimit = WebApp::getSVar("fBotimet->formati_botimit");
      $arr_botimet = array();
      if ($autorizimi<>"")  
        $arr_botimet[] = "Autorizimi='$autorizimi'";
      if ($titull_botimi<>"")  
        $arr_botimet[] = "Titulli permban '$titull_botimi'";
      if ($porositesi<>"")  
        $arr_botimet[] = "Porositesi permban '$porositesi'";
      if ($formati_botimit<>"")  
        $arr_botimet[] = "Formati permban '$formati_botimit'";  
      $botimet = implode(" dhe ", $arr_botimet);
      WebApp::addVar("botimet", $botimet);
    }
}
?>