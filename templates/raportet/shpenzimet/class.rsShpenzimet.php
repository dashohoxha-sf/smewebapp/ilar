<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Funksionet qe llogarisin shpenzimet per cdo botim
 * dhe ndertojne rekordsetin e shpenzimeve.
 *
 * @package raportet
 * @subpackage shpenzimet
 */
class rsShpenzimet
{
  function get_shpenzimet_rs()
    {
      //merr nje rekordset me botimet e zgjedhura (nga filtrimi)
      $filter_condition = $this->get_filter_condition();
      
          $rs_id = (PRINT_MODE=='true' ? 'getBotimet_print' : 'getBotimet');
          $botimet_rs = WebApp::openRS($rs_id, compact("filter_condition"));
        
      $reparti = WebApp::getSVar("shpenzimet->reparti");
      if ($reparti=="all")
        {
          $this->add_shpenzimet_e_repartit($botimet_rs, "offseti");
          //$this->add_shpenzimet_e_repartit($botimet_rs, "rrotative");
          $this->add_shpenzimet_e_repartit($botimet_rs, "parapreg");
          //$this->add_shpenzimet_e_repartit($botimet_rs, "lidhja");
        }
      else
        {
          $this->add_shpenzimet_e_repartit($botimet_rs, $reparti);
        }
          

      return $botimet_rs;
    }

  /**
   * Filtri qe zgjedh botimet qe do dalin ne raport.
   * Perdoret te getBotimet dhe te getBotimet_print.
   */
  function get_filter_condition()
    {
      return '(1=1)';
    }

  function add_shpenzimet_e_repartit(&$botimet_rs, $reparti)
    {
      //krijo nje liste me id-te e ketyre botimeve
      $arr_kart_id = $botimet_rs->getColumn("kart_id");
      $arr_kart_id[] = "-1";
      $liste_botimesh = implode(",", $arr_kart_id);
      //merr nje rekordset me punet e ketyre botimeve
      $punet_rs = $this->get_punet_rs($liste_botimesh, $reparti);

      //shto fushat e shpenzimeve te $botimet_rs sipas repartit
      $this->shto_fushat_e_shpenzimeve($botimet_rs, $reparti);

      //mblidhi te $botimet_rs shpenzimet e te gjitha puneve
      $botimet_rs->MoveFirst();
      $punet_rs->MoveFirst();
      while (!$botimet_rs->EOF())
        {
          $kart_id = $botimet_rs->Field("kart_id");
          while ($punet_rs->Field("kart_id") < $kart_id)
            {
              $punet_rs->MoveNext();
            }
          while ($punet_rs->Field("kart_id") == $kart_id)
            {
              $shpenzimet_e_nje_pune = $punet_rs->Field("shpenzimet");
               //print "<xmp>";
                //             print_r($shpenzimet_e_nje_pune );
              // print "</xmp>";
              $this->add_shpenzimet($botimet_rs, $shpenzimet_e_nje_pune);
              $punet_rs->MoveNext();
            }

          $botimet_rs->MoveNext();
        }
    }

  /** merr punet per listen e zgjedhur te botimeve */
  function get_punet_rs($liste_botimesh, $reparti)
  {
    return new EditableRS;
    }

  /** shto fushat e shpenzimeve te $botimet_rs sipas repartit */
  function shto_fushat_e_shpenzimeve(&$botimet_rs, $reparti)
  {
    $file_zerat = dirname(__FILE__)."/zerat_$reparti.php";
    include_once $file_zerat;
    
    for ($i=0; $i < sizeof($arr_zerat); $i++)
      {
        $zeri = $arr_zerat[$i];
      
        $botimet_rs->addCol($zeri, "0.0");
      }
  }

  function add_shpenzimet(&$botimet_rs, $str_shpenzimet_e_nje_pune)
    {
      $arr_shpenzimet = unserialize($str_shpenzimet_e_nje_pune);
      
      if (!is_array($arr_shpenzimet))  return;

      while (list($field, $value) = each($arr_shpenzimet))
        {
          $old_value = $botimet_rs->Field($field);
          
          if ($old_value===UNDEFINED) 
            {
              WebApp::debug_msg("Shpenzimi '$field' eshte i panjohur.",
                                "rptShpenzimet.php: add_shpenzimet()");
            }
          $new_value = (double)$old_value + (double)$value;
         
          $botimet_rs->setFld($field, $new_value);
        }
    }
}
?>
