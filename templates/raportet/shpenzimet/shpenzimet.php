<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage shpenzimet
 */

class shpenzimet extends WebObject
{
  function init()
    {
      $this->addSVar("sasi", "true");        // true|false
      $this->addSVar("vlere", "true");       // true|false
      $this->addSVar("reparti", "offseti");  // offseti|rrotative|parapreg|lidhja|all
      $this->addSVar("lloj_raporti", "per_buletin");  // per_botim|per_buletin
    }

  function on_set_lloj_raporti($event_args)
  {
    $lloj_rpt = $event_args['lloj_raporti'];
    $this->setSVar('lloj_raporti', $lloj_rpt);
    }

  function onParse()
    {
      global $event;
      if ($event->target=="main" and $event->name=="select")
        {
          $this->select_report($event->args);
        }
    }

  function select_report($event_args)
    {
      $interface = WebApp::getSVar("interface");
      $interface_title = WebApp::getSVar("interface_title");

      switch ($interface)
        {
          //shpenzimet sasi
        case "rpt_shpenzimet/sasi":
          $interface_title = "Shpenzimet Sasi";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "false");
          $this->setSVar("reparti", "all");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi/offseti":
          $interface_title = "Shpenzimet Sasi-->Offseti";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "false");
          $this->setSVar("reparti", "offseti");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi/rrotative":
          $interface_title = "Shpenzimet Sasi-->Rrotative";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "false");
          $this->setSVar("reparti", "rrotative");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi/parapreg":
          $interface_title = "Shpenzimet Sasi-->Parapregatitja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "false");
          $this->setSVar("reparti", "parapreg");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "laster");
          WebApp::setSVar("fZerat->labels", "Laster");
          break;

        case "rpt_shpenzimet/sasi/lidhja":
          $interface_title = "Shpenzimet Sasi-->Liberlidhja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "false");
          $this->setSVar("reparti", "lidhja");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

          //shpenzimet vlere
        case "rpt_shpenzimet/vlere":
          $interface_title = "Shpenzimet Vlere";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "false");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "all");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/vlere/offseti":
          $interface_title = "Shpenzimet Vlere-->Offseti";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "false");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "offseti");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/vlere/rrotative":
          $interface_title = "Shpenzimet Vlere-->Rrotative";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "false");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "rrotative");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/vlere/parapreg":
          $interface_title = "Shpenzimet Vlere-->Parapregatitja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "false");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "parapreg");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "laster");
          WebApp::setSVar("fZerat->labels", "Laster");
          break;

        case "rpt_shpenzimet/vlere/lidhja":
          $interface_title = "Shpenzimet Vlere-->Liberlidhja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "false");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "lidhja");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

          //shpenzimet sasi-vlere
        case "rpt_shpenzimet/sasi_vlere":
          $interface_title = "Shpenzimet Sasi-Vlere";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "all");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi_vlere/offseti":
          $interface_title = "Shpenzimet Sasi-Vlere-->Offseti";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "offseti");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi_vlere/rrotative":
          $interface_title = "Shpenzimet Sasi-Vlere-->Rrotative";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "rrotative");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;

        case "rpt_shpenzimet/sasi_vlere/parapreg":
          $interface_title = "Shpenzimet Sasi-Vlere-->Parapregatitja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "parapreg");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "laster");
          WebApp::setSVar("fZerat->labels", "Laster");
          break;

        case "rpt_shpenzimet/sasi_vlere/lidhja":
          $interface_title = "Shpenzimet Sasi-Vlere-->Liberlidhja";
          //cakto parametrat e raportit shpenzimet
          $this->setSVar("sasi", "true");
          $this->setSVar("vlere", "true");
          $this->setSVar("reparti", "lidhja");
          //cakto zerat e zgjedhur fillimisht
          WebApp::setSVar("fZerat->selected", "letra,boja");
          WebApp::setSVar("fZerat->labels", "Letra,Boja");
          break;
        }

      if ($interface=="rpt_shpenzimet")
        {
          $rpt_file = "rptShpenzimet1.html";
        }
      else
        {
          $rpt_file = "rptShpenzimet.html";
        }

      WebApp::setSVar("interface_title", $interface_title);
      WebApp::setSVar("rptFile", $rpt_file);
    }
}
?>