<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once dirname(__FILE__)."/class.rsShpenzimet.php";

/**
 *  @package raportet
 *  @subpackage shpenzimet
 */ 
class rsShpenzimetPerBuletin extends rsShpenzimet
{
  /**
   * Filtri qe zgjedh botimet qe do dalin ne raport.
   * Perdoret te getBotimet dhe te getBotimet_print.
   */
  function get_filter_condition()
  {
    $reparti = WebApp::getSVar("shpenzimet->reparti");
    switch ($reparti)
      {
        case 'offseti':
          $arr_kart_id = $this->get_kart_id_list('puna_e_kryer',
                                                 'buletinet');
          break;

        case 'parapreg':
          $arr_kart_id = $this->get_kart_id_list('punet_parapreg',
                                                 'buletinet_parapreg');
          break;

        case 'lidhja':
          $arr_kart_id = $this->get_kart_id_list('punet_lidhja',
                                                 'buletinet_lidhja');
          break;

        case 'all':
          $arr1_kart_id = $this->get_kart_id_list('puna_e_kryer',
                                                  'buletinet');
          $arr2_kart_id = $this->get_kart_id_list('punet_parapreg',
                                                  'buletinet_parapreg');
          $arr3_kart_id = $this->get_kart_id_list('punet_lidhja',
                                                  'buletinet_lidhja');
          $arr_kart_id = array_merge($arr1_kart_id,
                                     $arr2_kart_id,
                                     $arr3_kart_id);
          $arr_kart_id = array_unique($arr_kart_id);
          break;
      }
    $arr_kart_id[] = '-1'; //sentinel value, in case that it is empty
    $liste_botimesh = implode(',', $arr_kart_id);

    //liste_botimesh jane botimet qe do te shfaqeshin ne raport sikur
    //te kishte vetem filtrim sipas dates dhe te mos kishte filtrim 
    //sipas botimit. Duke perdorur kete liste dhe filtrimin sipas 
    //botimit, marrim nje filter per botimet qe do te shfaqen ne raport.

    $filter_botimesh = WebApp::getSVar("fBotimet->filter");
    if ($filter_botimesh=='')  $filter_botimesh='1=1';

    $filter_condition = "(($filter_botimesh)
            AND kart_id IN ($liste_botimesh))";

    return $filter_condition;
  }

  /**
   * Kthen nje liste me kartat per te cilat egzistojne pune te raportuara
   * brenda periudhes kohore te zgjedhur. Botimet me keto kart_id
   * _mund_ te shfaqen ne raport (sepse varet edhe nga filtri i botimeve).
   */
  function get_kart_id_list($tabela_punet, $tabela_buletinet)
  {
    $data_filter = WebApp::getSVar('data->filter');
    $data_filter = str_replace('date_field', 'data', $data_filter);
    $params = compact('tabela_punet', 'tabela_buletinet', 'data_filter');
    $kart_rs = WebApp::openRS('getKartList', $params); 
    $arr_kart_id = $kart_rs->getColumn('kart_id');

    return $arr_kart_id;
  }

  /** merr punet per listen e zgjedhur te botimeve */
  function get_punet_rs($liste_botimesh, $reparti)
  {
    switch ($reparti)
      {
      case 'offseti':
        $tabela_buletinet = 'buletinet';
        $tabela_punet = 'puna_e_kryer';
        break;
      case 'rrotative':
        $tabela_buletinet = 'buletinet';
        $tabela_punet = 'puna_e_kryer';
        break;
      case 'parapreg':
        $tabela_buletinet = 'buletinet_parapreg';
        $tabela_punet = 'punet_parapreg';
        break;
      case 'lidhja':
        $tabela_buletinet = 'buletinet_lidhja';
        $tabela_punet = 'punet_lidhja';
        break;
      default:
        $msg = 'rsShpenzimetPerBuletin::get_punet_rs(): '
          . "reparti '$reparti' eshte i panjohur.";
        print WebApp::error_msg($msg);
        return;
        break;
      }

    $data_filter = WebApp::getSVar('data->filter');
    $data_filter = str_replace('date_field', 'data', $data_filter);
    $params = compact('tabela_punet', 'tabela_buletinet',
                      'data_filter', 'liste_botimesh');
    $punet_rs = WebApp::openRS('getPunet1', $params); 

    return $punet_rs;
  }
}
?>