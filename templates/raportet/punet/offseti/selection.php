<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class selection extends WebObject
{
  function onRender()
    {
      $day1 = WebApp::getSVar("data->day1");
      $month1 = WebApp::getSVar("data->month1");
      $year1 = WebApp::getSVar("data->year1");
      $day2 = WebApp::getSVar("data->day2");
      $month2 = WebApp::getSVar("data->month2");
      $year2 = WebApp::getSVar("data->year2");
      $periudha = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar("periudha", $periudha);

      //nderto selektimin e bere ne baze te te dhenat e tjera
      $te_tjera = "";
      $turni = WebApp::getSVar("fBuletinet->turni");
      $makina = WebApp::getSVar("fBuletinet->makina");
      $makinisti = WebApp::getSVar("fBuletinet->makinisti");

      $kart_id = WebApp::getSVar("fPunet->kart_id");
      $titull_botimi = WebApp::getSVar("fPunet->titull_botimi");
      $lloj_pune = WebApp::getSVar("fPunet->lloj_pune");
      $formati = WebApp::getSVar("fPunet->formati");

      $arr_te_tjera = array();

      if ($turni<>"")
        $arr_te_tjera[] = "Turni ='$turni'";

      $reparti = WebApp::getSVar('rpt_punet->reparti');
      if ($reparti!='rrotative' and $makina<>"")  
        $arr_te_tjera[] = "Makina = '$makina'";

      if ($makinisti<>"")
        $arr_te_tjera[] = "Makinisti permban '$makinisti'";

      if ($kart_id<>"")
        $arr_te_tjera[] = "Karta Tekn. '$kart_id'";

      if ($titull_botimi<>"")
        $arr_te_tjera[] = "Titulli permban '$titull_botimi'";

      if ($lloj_pune<>"")
        $arr_te_tjera[] = "Lloj Pune ='$lloj_pune'";

      if ($formati<>"")
        $arr_te_tjera[] = "Formati='$formati'";

      $te_tjera = implode(" dhe ", $arr_te_tjera);
      WebApp::addVar("te_tjera", $te_tjera);

      $te_tjera = implode(" dhe ", $arr_te_tjera);
      WebApp::addVar("te_tjera", $te_tjera);
    }
}
?>



