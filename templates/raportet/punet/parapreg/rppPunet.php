<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rppPunet extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("rs_rppPunet->current_page", $page);
    }

  function onRender()
    {
      WebApp::setSVar("rs_rppPunet->recount", "true");

      $rs_rppPunet = $this->get_punet_rs();
      global $webPage;
      $webPage->addRecordset($rs_rppPunet);

      //llogarit shumen e oreve te punes dhe shtoje ne templet
      $arr_oret = $rs_rppPunet->getColumn("ore_shtypur");
      for ($i=0; $i < sizeof($arr_oret); $i++)  $ore_pune += $arr_oret[$i];
      WebApp::addVar("ore_pune", $ore_pune);
    }
    
  function get_punet_rs()
    { 
      //merr nje rekordset me botimet e zgjedhura
      $buletinet_condition = $this->get_filter_condition();
      $params = compact("buletinet_condition");
      $buletin_rs = WebApp::openRS("buletinet", $params);
        
      //merr nje liste te buletineve te zgjedhura
      $arr_bulet_id = $buletin_rs->getColumn("buletin_id");
      $arr_bulet_id[] = "-1";
      $liste_buletinesh = implode(",", $arr_bulet_id);

      //formo kushtin per te filtruar punet per keto buletine
      $arr_conditions[] = "(buletin_id IN ($liste_buletinesh))";
      $punet_filter = WebApp::getSVar("fPunet->filter");
      if ($punet_filter<>'')  $arr_conditions[] = $punet_filter;
      $punet_condition = implode(" AND ", $arr_conditions);

      //merr rekordsetin e puneve
      $rs_id = (PRINT_MODE=='true' ? 'rs_rppPunet_print' : 'rs_rppPunet'); 
      $punet_rs = WebApp::openRS($rs_id, compact("punet_condition")); 

      //shto fushat qe do merren nga buletinet
      $punet_rs->addCol("data", "");
      $punet_rs->addCol("turni", "");
      $punet_rs->addCol("reparti", "");
      $punet_rs->addCol("makina", "");
      $punet_rs->addCol("makinisti", "");
            
      //shto fushat e buletineve te punet
      $punet_rs->MoveFirst();
      $buletin_rs->MoveFirst();
      while (!$punet_rs->EOF())
        {
          $buletin_id = $punet_rs->Field("buletin_id");
          while ($buletin_rs->Field("buletin_id") < $buletin_id)
            {
              $buletin_rs->MoveNext();
            }
          if ($buletin_rs->Field("buletin_id") == $buletin_id)
            {     
              $punet_rs->setFld("data", $buletin_rs->Field("data"));
              $punet_rs->setFld("turni", $buletin_rs->Field("turni"));
              $punet_rs->setFld("reparti", $buletin_rs->Field("reparti"));
              $punet_rs->setFld("makina", $buletin_rs->Field("makina"));
              $punet_rs->setFld("makinisti", $buletin_rs->Field("makinisti"));
      
              $buletin_rs->MoveNext();
            }

          $punet_rs->MoveNext();  
        }

      return $punet_rs;
    }
  
  function get_filter_condition()
    {
      $data_filter = WebApp::getSVar("data->filter");
      $data_filter = str_replace("date_field", "data", $data_filter);
      $buletinet_filter = WebApp::getSVar("fBuletinet->filter");

      $conditions[] = $data_filter;
      if ($buletinet_filter<>"")  $conditions[] = $buletinet_filter;

      $filter_condition = implode(" AND ", $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
}
?>