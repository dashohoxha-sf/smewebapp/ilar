<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rppLidhja extends WebObject
{
  function onRender()
    {
      global $webPage;
      $punet = WebApp::execQuery("SELECT * FROM llojet_e_puneve");
      $punet->ID = "llojet_e_puneve";
      $webPage->addRecordset($punet);

      $makinat = WebApp::execQuery("SELECT * FROM makinat");
      
      $punet->MoveFirst();
      while (!$punet->EOF())
        {
          $lloj_pune = $punet->Field("lloj_pune");
          $makinat->addCol($lloj_pune, "11");
          WebApp::addVar("totali $lloj_pune", "66");
          $punet->MoveNext();
        }

      $makinat->MoveFirst();
      while (!$makinat->EOF())
        {
          $makina = $makinat->Field("makina");
          WebApp::addVar("Totali $makina", "77");
          $makinat->MoveNext();
        }

      WebApp::addVar("totali Totali", "237");

      $makinat->ID = "makinat";
      $webPage->addRecordset($makinat);
    }
}
?>