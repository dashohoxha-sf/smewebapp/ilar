<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rppParapreg extends WebObject
{
  function onRender()
    {
      $llojet_e_puneve = WebApp::openRS('llojet_e_puneve_parapreg');

      //create an associative array with 'lloj_pune' as key
      $arr_oret = array();
      $llojet_e_puneve->MoveFirst();
      while (!$llojet_e_puneve->EOF())
        {
          $lloj_pune = $llojet_e_puneve->Field("lloj_pune");
          $arr_oret[$lloj_pune] = ' ';
          $llojet_e_puneve->MoveNext();
        }

      //get punet in the selected timeframe and add them up to the recordset
      $filter_condition = $this->get_filter_condition();
      $punet = WebApp::openRS('punet_parapreg', compact('filter_condition'));
      $punet->MoveFirst();
      $punet->MoveFirst();
      while (!$punet->EOF())
        {
          $lloj_pune = $punet->Field('lloj_pune');
          $oret = $punet->Field('ore_shtypur');
          $arr_oret[$lloj_pune] += $oret;
          $punet->MoveNext();
        }

      //set array values to the recordset oret
      $oret = new EditableRS('oret');
      $llojet_e_puneve->MoveFirst();
      while (!$llojet_e_puneve->EOF())
        {
          $lloj_pune = $llojet_e_puneve->Field("lloj_pune");
          $rec = array('lloj_pune'=>$lloj_pune, 'oret'=>$arr_oret[$lloj_pune]);
          $oret->addRec($rec);
          $llojet_e_puneve->MoveNext();
        }

      $totali = array_sum($oret->getColumn('oret'));
      WebApp::addVar("totali", $totali);
      
      global $webPage;
      $webPage->addRecordset($oret);
    }

  function get_filter_condition()
  {
    $data_filter = WebApp::getSVar("data->filter");
    $data_filter = str_replace("date_field", "data", $data_filter);

    return $data_filter;
  }
}
?>