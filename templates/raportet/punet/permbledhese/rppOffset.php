<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */
 
class rppOffset extends WebObject
{
  function onRender()
    {
      //create a recordset with empty values
      //this is the recordset that will fill the template
      $rs = WebApp::openRS('makinat');
      $llojet_e_puneve = WebApp::openRS('llojet_e_puneve');
      $llojet_e_puneve->MoveFirst();
      while (!$llojet_e_puneve->EOF())
        {
          $lloj_pune = $llojet_e_puneve->Field("lloj_pune");
          $rs->addCol($lloj_pune, ' ');
          $llojet_e_puneve->MoveNext();
        }
      $rs->addCol('totali', '0.0');

      //get punet in the selected timeframe and add them up to the recordset
      $filter_condition = $this->get_filter_condition();
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $makina = $rs->Field('makina');
          $params = compact('filter_condition', 'makina');
          $punet = WebApp::openRS('punet', $params);
          $punet->MoveFirst();
          while (!$punet->EOF())
            {
              $lloj_pune = $punet->Field('lloj_pune');
              $oret = $punet->Field('ore_shtypur');
              $shuma = $rs->Field($lloj_pune);
              $shuma += $oret;
              $rs->setFld($lloj_pune, $shuma);
              $punet->MoveNext();
            }
          $rs->MoveNext();
        }

      //add totalet per makine
      $rs->MoveFirst();
      while (!$rs->EOF())
        {
          $totali = 0.0;
          $llojet_e_puneve->MoveFirst();
          while (!$llojet_e_puneve->EOF())
            {
              $lloj_pune = $llojet_e_puneve->Field("lloj_pune");
              $totali += $rs->Field($lloj_pune);
              $llojet_e_puneve->MoveNext();
            }
          $rs->setFld('totali', $totali);
          $rs->MoveNext();
        }

      //add totalet per pune
      $llojet_e_puneve->MoveFirst();
      while (!$llojet_e_puneve->EOF())
        {
          $lloj_pune = $llojet_e_puneve->Field("lloj_pune");
          $arr_column = $rs->getColumn($lloj_pune);
          $totali = array_sum($arr_column);
          WebApp::addVar("totali $lloj_pune", $totali);
          $total_sum += $totali;
          $llojet_e_puneve->MoveNext();
        }

      //add the big total
      WebApp::addVar("totali Totali", $total_sum);

      global $webPage;
      $webPage->addRecordset($rs);
    }

  function get_filter_condition()
  {
    $data_filter = WebApp::getSVar("data->filter");
    $data_filter = str_replace("date_field", "data", $data_filter);

    return $data_filter;
  }
}
?>