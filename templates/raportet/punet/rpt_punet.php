<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package raportet
 * @subpackage punet
 */

class rpt_punet extends WebObject
{
  function init()
    {
      $this->addSVar("module", "offseti/rpoPunet.html");
      $this->addSVar("reparti", "offseti");
    }

  function onParse()
    {
      global $event;
      if ($event->target=="main" and $event->name=="select")
        {
          $this->select_punet($event->args);
        }
    }

  function select_punet($event_args)
    {
      $interface = WebApp::getSVar("interface");
      switch ($interface)
        {
        case "rpt_punet/parapreg":
          $interface_title = "Punet e Raportuara-->Parapregatitja";
          $module = "parapreg/raport.html";
          $this->setSVar("reparti", "parapreg");
          $this->init_filters($event_args);
          break;
        case "rpt_punet/offseti":
          $interface_title = "Punet e Raportuara-->Offseti";
          $module = "offseti/raport.html";
          $this->setSVar("reparti", "offseti");
          $this->init_filters($event_args);
          break;
        case "rpt_punet/rrotative":
          $interface_title = "Punet e Raportuara-->Rrotative";
          $module = "offseti/raport.html";
          $this->setSVar("reparti", "rrotative");
          $this->init_filters($event_args);
          break;
        case "rpt_punet/lidhja":
          $interface_title = "Punet e Raportuara-->Liberlidhja";
          $module = "lidhja/raport.html";
          $this->setSVar("reparti", "lidhja");
          $this->init_filters($event_args);
          break;
        case "rpt_punet/permbledhese":
          $interface_title = "Raportet-->Punet e Raportuara-->Raport Permbledhes";
          $module = "permbledhese/raport.html";
          $this->init_filters($event_args);
          break;
        }

      WebApp::setSVar("interface_title", $interface_title);
      $this->setSVar("module", $module);
    }

  /**
   * Initiate the filters, in case that $event_args contains some
   * filter variables. If $event_args does not contain a filter
   * variable, then it will get an empty value.
   */
  function init_filters($event_args)
    {
      
      WebApp::setSVar("fPunet->kart_id",       ''.$event_args['kart_id']);
      WebApp::setSVar("fPunet->titull_botimi", ''.$event_args['titull_botimi']);
      WebApp::setSVar("fPunet->lloj_pune",     ''.$event_args['lloj_pune']);
      WebApp::setSVar("fPunet->formati",       ''.$event_args['formati']);
      WebApp::setSVar("fBuletinet->makina",    ''.$event_args['makina']);
      WebApp::setSVar("fBuletinet->makinisti", ''.$event_args['makinisti']);
      WebApp::setSVar("fBuletinet->turni",     ''.$event_args['turni']);
      WebApp::setSVar("fBuletinet->proces",    ''.$event_args['proces']);
    }
}
?>