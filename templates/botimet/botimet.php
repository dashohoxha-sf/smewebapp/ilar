<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package botimet
 */
class botimet extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'kartat/listo/listo.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_botim($event->args);
        }
    }

  function select_botim($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //Kontratat
        case 'botimet/kontratat/listo':
          $interface_title = 'Kontratat-->Lista';
          $module = 'kontratat/listo/listo.html';
          break;
        case 'botimet/kontratat/edit':
          $interface_title = 'Kontratat-->Ndrysho';
          $module = 'kontratat/edit/konEdit.html';
          //set the state of the konEdit module
          WebApp::setSVar('konEdit->mode', 'edit');
          WebApp::setSVar('konEdit->botim_id', $event_args['botim_id']);
          break;
        case 'botimet/kontratat/krijo':
          $interface_title = 'Kontratat-->Krijo';
          $module = 'kontratat/edit/konEdit.html';
          //set the state of the konEdit module
          WebApp::setSVar('konEdit->mode', 'add');
          WebApp::setSVar('konEdit->botim_id', UNDEFINED);
          break;

          //Kartat Teknologjike
        case 'botimet/kartat/listo':
          $interface_title = 'Kartat Teknologjike-->Lista';
          $module = 'kartat/listo/listo.html';
          break;
        case 'botimet/kartat/edit':
          $interface_title = 'Kartat Teknologjike-->Ndrysho';
          $module = 'kartat/edit/tcEdit.html';
          //set the state of the tcEdit module
          WebApp::setSVar('tcEdit->mode', 'edit');
          WebApp::setSVar('tcEdit->kart_id', $event_args['kart_id']);
          WebApp::setSVar('tabs::tcEdit->selected_item', 'te_pergjithshme');
          break;
        case 'botimet/kartat/krijo':
          $interface_title = 'Kartat Teknologjike-->Krijo';
          $module = 'kartat/edit/tcEdit.html';
          //set the state of the tcEdit module
          WebApp::setSVar('tcEdit->mode', 'add');
          WebApp::setSVar('tcEdit->kart_id', UNDEFINED);
          WebApp::setSVar('tabs::tcEdit->selected_item', 'te_pergjithshme');
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>