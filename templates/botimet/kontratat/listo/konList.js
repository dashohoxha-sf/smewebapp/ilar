// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function view_kontrat(botim_id)
{
  select('botimet/kontratat/edit', 'botim_id='+botim_id);
}

function print_kontrat(botim_id)
{
  print_template('botimet/kontratat/edit', 'botim_id='+botim_id);
}

/** create a new teknologjical card from this contract */
function new_tcard(nr_kontrate, titull_botimi)
{
  var msg = 'Po krijohet nje karte teknologjike e re \n'
    + 'duke u bazuar ne kontraten per botimin: \n'
    + '(' + nr_kontrate + ') ' + titull_botimi;
  if (!confirm(msg))  return;

  select('botimet/kartat/krijo', 'nr_kontrate='+nr_kontrate);
}

function rm_kontrat(botim_id, nr_kontrate, titull_botimi)
{
  var msg = 'Po fshihet kontrata: '
    + '(' + nr_kontrate + ') ' + titull_botimi;
  if (!confirm(msg))  return;

  SendEvent('konList', 'rm', 'botim_id='+botim_id);
}
