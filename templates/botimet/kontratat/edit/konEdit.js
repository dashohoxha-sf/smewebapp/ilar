// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function shtyp()
{
  var interface_id = session.getVar('interface');
  var botim_id = session.getVar('konEdit->botim_id');
  var args = 'botim_id=' + botim_id;
  print_template(interface_id, args);
}

function save()
{
  var form = document.konEdit;
  if (!editable())      return;
  if (!validate(form))  return;
  
  var event_args = getEventArgs(form);
   
  var mode = session.getVar("konEdit->mode");
  if (mode=="add")  saveFormData(form);
  SendEvent("konEdit", "save", event_args);
}

/** create a new kontrate with almost the same data */
function clone()
{
  var confirm_msg;
  confirm_msg = "Po krijohet nje kontrate e re \n"
    + "me te njejtat te dhena si kjo kontrate.";
  if (confirm(confirm_msg))
    {
      SendEvent("konEdit", "clone");
    }
}

/** create a new teknologjical card from this contract */
function new_tcard()
{
  var confirm_msg;
  confirm_msg = "Po krijohet nje karte teknologjike \n"
    + "duke u bazuar ne kete kontrate.";
  if (confirm(confirm_msg))
    {
      var nr_kontrate = document.konEdit.nr_kontrate.value;
      select('botimet/kartat/krijo', 'nr_kontrate='+nr_kontrate);
    }
}

function close_doc()
{
  var confirm_msg;
  confirm_msg = "Jeni duke mbyllur Botimin, \n"
    + "k�shtu q� nuk mund t� b�ni m� ndryshime n� t�.\n\n\n"
    + "Jeni t� sigurt se doni ta mbyllni k�t� Botim?\n\n";
  if (confirm(confirm_msg))
    {
      GoTo("thisPage?event=konEdit.close");
    }
}

/** returns false if the document is closed */
function editable()
{
  var closed = session.getVar("konEdit->closed");

  if (closed=="true")
    {
      alert("Ky Botim �sht� i mbyllur dhe nuk mund t� modifikohet.");
      return false;
    }

  return true;
}

function date_not_in_future(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = arr_date[0];
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date > curr_date)
    {
      alert("Warning: The selected date is later than the current date.");
      //set_current_date(txtbox);
    }
}

function date_not_in_past(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = (arr_date[0]*1) + 1;
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date < curr_date)
    {
      alert("Kujdes: Data e zgjedhur nga ju �sht� von� sesa data aktuale.");
      //set_current_date(txtbox);
    }
}

function set_current_date(txtbox)
{
  var curr_date = get_curr_date();
  var day = curr_date.getDate();
  var month = curr_date.getMonth() + 1;
  var year = curr_date.getFullYear();

  if (day < 10)    day   = "0" + day;
  if (month < 10)  month = "0" + month;
  str_date = day + '/' + month + '/' + year;
  txtbox.value = str_date;
}

function validate(form)
{
  if (form.nr_kontrate.value=="")
    {
      alert ("Ju lutem mos e lini bosh numrin e kontrates.");
      form.nr_kontrate.focus();
      return false;
    }
    
  if (form.gjinia.value=="")
    {
      alert ("Ju lutem mos e lini bosh 'Gjinia'.");
      form.gjinia.focus();
      return false;
    }
      
  if (form.titull_botimi.value == "")
    {
      alert ("Ju lutem mos e lini bosh titullin.");
      form.titull_botimi.focus();
      return false;
    }    
  
  return true;
}









