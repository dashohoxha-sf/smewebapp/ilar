<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

/**
 * @package botimet
 * @subpackage kontratat
 */
class konEdit extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      $this->addSVar("botim_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getKontrat");
          $close_date = $rs->Field("date_mbyllje_botimi");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table
          $rs = WebApp::execQuery("SHOW FIELDS FROM botimet");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));

          //title
          $title = "Krijo Kontrate";
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getKontrat");
          $fields = $rs->Fields();
          $this->explode_lloj_letre($fields, 'letra');
          $this->explode_lloj_letre($fields, 'kopertina');
          WebApp::addVars($fields);
          //title
          $nr_kontrate = $fields['nr_kontrate'];
          $pala_porositese = $fields['pala_porositese'];
          $titull_botimi = $fields['titull_botimi'];
          $title = "Kontrata: $nr_kontrate::$pala_porositese::$titull_botimi";
        }

      WebApp::addVar("title", $title);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      $params = $event_args;
      $this->implode_lloj_letre($params, 'letra');
      $this->implode_lloj_letre($params, 'kopertina');
      if ($mode=="add")
        {
          $this->add_new_kontrate($params);
        }
      else //mode=="edit"
        {
          WebApp::execDBCmd("updateKontrat", $params);
        }
    }

  /** create a new contract with almost the same data as this one */
  function on_clone($event_args)
    {
    $rs = WebApp::openRS("getKontrat");
    $fields = $rs->Fields();
    $fields['nr_kontrate'] = '';
    $this->add_new_kontrate($fields);
  }
       
  function on_close($event_args)
    {
      $params["date_mbyllje_botimi"] = get_curr_date("Y-M-D");
      WebApp::execDBCmd("closeBotim", $params);
    }

  function add_new_kontrate($params)
    {
      //find a botim_id for the new project
      $rs = WebApp::openRS("getLastKontratId");
      $last_botim_id = $rs->Field("last_botim_id");
      $botim_id = (($last_botim_id==UNDEFINED) ? 1 : $last_botim_id+1);

      //add the new kontrat
      $params["botim_id"] = $botim_id;
      $params["date_krijimi"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newKontrat", $params);

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'kontratat_rs->recount' to "true"
      //so that the records are counted again
      //and the new kontrat is displayed at the end of the list
      WebApp::setSVar("kontratat_rs->recount", "true");

      //set the new botim_id
      $this->setSVar("botim_id", $botim_id);
    }

  /**
   * Get the value of $var_name (which is like: 61x36/250/bindakot)
   * extract the vars p1=61, p2=36, pesha=250, lloji=bindakot
   * and add them to the array $fields.
   */
  function explode_lloj_letre(&$fields, $var_name)
  {
    $var_value = $fields[$var_name];
    list($permasat,$pesha,$lloji) = explode('/', $var_value, 3);
    list($p1,$p2) = explode('x', $permasat, 2);

    $fields[$var_name.'_p1'] = $p1;
    $fields[$var_name.'_p2'] = $p2;
    $fields[$var_name.'_pesha'] = $pesha;
    $fields[$var_name.'_lloji'] = $lloji;
  }

  /**
   * Get the vars p1, p2, pesha, lloji, construct 'p1xp2/pesha/lloji'
   * and add it as the value of $var_name in the array $params
   */
  function implode_lloj_letre(&$params, $var_name)
  {
    $p1 = $params[$var_name.'_p1'];
    $p2 = $params[$var_name.'_p2'];
    $pesha = $params[$var_name.'_pesha'];
    $lloji = $params[$var_name.'_lloji'];
    $params[$var_name] = $p1.'x'.$p2.'/'.$pesha.'/'.$lloji;
  }
}
?>