<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package botimet
 * @subpackage kontratat
 */
class konPrint extends WebObject
{
  function init()
    {
      $this->addSVar("botim_id", $botim_id);
    }

  function onParse()
  {
    //fill the latex template with the data of the selected document
    $path = WebApp::getVar('./');
    $latex_tpl = $path.'kontrate/kontrate.tex';
    global $event;
    $latex_content = WebApp::fill_template($latex_tpl, $event->args);

    //generate the pdf file and open it in a new window
    $pdf_file = latex2pdf("kontrate", $latex_content);
    WebApp::popup_window('pdf', $pdf_file, 'width=1,height=1');
  }

  function onRender()
    {
      $rs = WebApp::openRS("getKontrat");
      $fields = $rs->Fields();
      WebApp::addVars($fields);
    }
}
?>