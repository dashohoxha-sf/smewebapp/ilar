<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package botimet
 * @subpackage kartat
 */
class tcPrint extends WebObject
{
  function init()
    {
      $this->addSVar("kart_id", UNDEFINED);
    }

  function onParse()
  {
    //fill the latex template with the data of the selected document
    $path = WebApp::getVar('./');
    $latex_tpl = $path.'karte/karte.tex';
    global $event;
    $latex_content = WebApp::fill_template($latex_tpl, $event->args);

    //generate the pdf file and open it in a new window
    $pdf_file = latex2pdf('karte', $latex_content);
    WebApp::popup_window('pdf', $pdf_file, 'width=1,height=1');
  }

  function onRender()
    {
      $rs = WebApp::openRS("getKarte");
      $fields = $rs->Fields();
      $this->explode_skemat($fields);
      WebApp::addVars($fields);
    }

  function explode_skemat(&$arr_vars)
  {
    $str_skemat = $arr_vars['skemat'];
    if (trim($str_skemat)=='')
      {
        for ($i=1; $i <= 6; $i++)
          {
            $arr_vars["page$i-text"] = '';
            $arr_vars["page$i-nr-left"] = '';
            $arr_vars["page$i-nr-right"] = '';
          }
        return;
      }

    $arr = unserialize($str_skemat);
    while (list($var, $value) = each($arr))
      {
        $arr_vars[$var] = $value;
      }
  }
}
?>