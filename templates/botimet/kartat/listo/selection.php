<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package botimet
* @subpackage kartat
*/

class selection extends WebObject
{
  function onRender()
    {
      $day1 = WebApp::getSVar("data->day1");
      $month1 = WebApp::getSVar("data->month1");
      $year1 = WebApp::getSVar("data->year1");
      $day2 = WebApp::getSVar("data->day2");
      $month2 = WebApp::getSVar("data->month2");
      $year2 = WebApp::getSVar("data->year2");
      $periudha = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar("periudha", $periudha);


      //nderto selektimin e bere ne baze te te dhenat e tjera
      $te_tjera = "";
      $titull_botimi = WebApp::getSVar("fKartat->titull_botimi");
      $porositesi = WebApp::getSVar("fKartat->porositesi");
      $gjinia = WebApp::getSVar("fKartat->gjinia");
      $formati_botimit = WebApp::getSVar("fKartat->formati_botimit");
      $nr_faqesh = WebApp::getSVar("fKartat->nr_faqesh");
      $kopjet = WebApp::getSVar("fKartat->kopjet");

      $arr_fushat = array();
      if ($titull_botimi<>"")
        $arr_fushat[] = "Titulli permban '$titull_botimi'";
      if ($porositesi<>"")
        $arr_fushat[] = "Porosit�i permban '$porositesi'";

      if ($gjinia<>"")
        $arr_fushat[] = "Gjinia='$gjinia'";

      if ($formati_botimit<>"")
        $arr_fushat[] = "Formati permban '$formati_botimit'";

      if ($nr_faqesh<>"")
        $arr_fushat[] = "Faqet ='$nr_faqesh'";

      if ($kopjet<>"")
        $arr_fushat[] = "Kopjet='$kopjet'";

      $fushat = implode(" dhe ", $arr_fushat);
      WebApp::addVar("fushat", $fushat);
    }
}
?>