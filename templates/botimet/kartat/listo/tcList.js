// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function view_kart(kart_id)
{
  select('botimet/kartat/edit', 'kart_id='+kart_id);
}

function print_kart(kart_id)
{
  print_template('botimet/kartat/edit', 'kart_id='+kart_id);
}

function rm_kart(kart_id, nr_karte, titull_botimi)
{
  var msg = 'Po fshihet karta: '
    + '(' + nr_karte + ') ' + titull_botimi;
  if (!confirm(msg))  return;

  SendEvent('tcList', 'rm', 'kart_id='+kart_id);
}
