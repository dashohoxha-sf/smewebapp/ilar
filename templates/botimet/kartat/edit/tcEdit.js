// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Override this function of the webclass tabs, 
 * so that it sends the variables of the form as well
 * (for saving them before changing the tab).
 */
/*
function tabs_select(obj_id, item)	
{
  var mode = session.getVar('tcEdit->mode');
  if (mode=='add')
    {
      alert("Ne fillim krijoe karten duke shtypur Ruaj,\n"
            + "pastaj kalo ne pjeset e tjera.");
      return;
    }

  var form = document.tcEdit;
  //if (!validate())  return;

  var event_args = getEventArgs(form);
  SendEvent(obj_id, 'select', 'item=' + item + ';' + event_args);
}
*/

function shtyp()
{
  var interface_id = session.getVar('interface');
  var kart_id = session.getVar('tcEdit->kart_id');
  print_template(interface_id, 'kart_id='+kart_id);
}

function close_doc()
{
  var confirm_msg;
  confirm_msg = "Jeni duke e mbyllur karten, \n"
    + "k�shtu q� nuk mund t� b�ni m� ndryshime n� t�.\n\n\n"
    + "Jeni t� sigurt se doni ta mbyllni k�t� karte?\n\n";
  if (confirm(confirm_msg))
    {
      GoTo("thisPage?event=tcEdit.close");
    }
}

/** returns false if the document is closed */
function editable()
{
  var closed = session.getVar("tcEdit->closed");

  if (closed=="true")
    {
      alert("Kjo karte �sht� e mbyllur dhe nuk mund t� modifikohet.");
      return false;
    }

  return true;
}

/** create an almost identical card */
function clone()
{
  var confirm_msg;
  confirm_msg = "Po krijohet nje karte e re \n"
    + "me te njejtat te dhena si kjo karte.";
  if (!confirm(confirm_msg)) return;

  SendEvent('tcEdit', 'clone');
}

/** create a new card based on a contract */
function copy_contract()
{
  var confirm_msg;
  confirm_msg = "Po krijohet nje karte e re sipas\n"
    + "te dhenave te kontrates se dhene.";
  if (!confirm(confirm_msg)) return;

  var nr_kontrate = document.tcEdit.nr_kontrate.value;
  if (nr_kontrate=='')
    {
      alert('Jepni nje numer kontrate.');
      document.tcEdit.nr_kontrate.focus();
      return;
    }
    
  SendEvent('tcEdit', 'create_from_contract', 'nr_kontrate='+nr_kontrate);
}

function save()
{
  var form = document.tcEdit;
  if (!validate())  return;
  
  var event_args = getEventArgs(form);
   
  var mode = session.getVar('tcEdit->mode');
  if (mode=='add')  saveFormData(form);
  SendEvent('tcEdit', 'save', event_args);
}

function validate()
{
  var tab = session.getVar('tabs::tcEdit->selected_item');
  eval('var valid = validate_'+tab+'();');
  return valid;
}

function validate_te_pergjithshme()
{
  var form = document.tcEdit;
  if (form.nr_karte.value=="")
    {
      alert ("Ju lutem mos e lini bosh numrin e kartes.");
      form.nr_karte.focus();
      return false;
    }    
      
  if (form.titull_botimi.value == "")
    {
      alert ("Ju lutem mos e lini bosh 'Titulli i Botimit'.");
      form.titull_botimi.focus();
      return false;
    }    
  
  return true;
}
   
function validate_kompjuteri()
{
  return true;
}

function validate_montimi()
{
  return true;
}

function validate_shtypi()
{
  return true;
}

function validate_liberlidhja()
{
  return true;
}

function validate_udhezime()
{
  return true;
}

function validate_skemat()
{
  return true;
}
