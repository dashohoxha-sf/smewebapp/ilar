<!--# -*-SQL-*- tell emacs to use SQL mode #-->

<!--# This file contains the recordsets that are used 
      in the templates (e.g. for listboxes) #-->

<Recordset ID="makina">
  <Query>
    SELECT makina AS id, makina AS label
    FROM makinat
  </Query>
</Recordset>

<Recordset ID="gjinia">
  <Query>
    SELECT gjinia AS id,  gjinia AS label
    FROM gjinia
  </Query>
</Recordset>

<Recordset ID="permasa1">
  <Query>
    SELECT permasa1 AS id, permasa1 AS label
    FROM permasat_leter
  </Query>
</Recordset>

<Recordset ID="permasa2">
  <Query>
    SELECT permasa2 AS id, permasa2 AS label
    FROM permasat_leter
  </Query>
</Recordset>

<Recordset ID="pesha">
  <Query>
    SELECT pesha AS id, pesha AS label
    FROM pesha_leter
    </Query>
</Recordset>

<Recordset ID="lloji">
  <Query>
    SELECT lloji AS id, lloji AS label
    FROM lloji_leter
    </Query>
</Recordset>
