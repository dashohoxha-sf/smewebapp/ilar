<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

/**
 * @package botimet
 * @subpackage kartat
 */
class tcEdit extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      $this->addSVar("kart_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function on_close($event_args)
    {
      $params["date_mbyllje"] = get_curr_date("Y-M-D");
      WebApp::execDBCmd("closeKart", $params);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      $this->implode_vars($event_args);
      if ($mode=="add")
        {
          $this->add_new_tcard($event_args);
        }
      else //mode=="edit"
        {
          $tab = WebApp::getSVar('tabs::tcEdit->selected_item');
          WebApp::execDBCmd("updateKart_$tab", $event_args);
        }
    }

  function on_clone($event_args)
  {
    $rs = WebApp::openRS("getKart");
    $fields = $rs->Fields();
    $fields['nr_karte'] = '';
    $fields['nr_kontrate'] = '';
    $this->add_new_tcard($fields);
    WebApp::execDBCmd('updateKart_kompjuteri', $fields);
    WebApp::execDBCmd('updateKart_montimi', $fields);
    WebApp::execDBCmd('updateKart_shtypi', $fields);
    WebApp::execDBCmd('updateKart_liberlidhja', $fields);
    WebApp::execDBCmd('updateKart_udhezime', $fields);
    WebApp::execDBCmd('updateKart_skemat', $fields);
  }

  function on_create_from_contract($event_args)
  {
    $rs = WebApp::openRS("getKontrat", $event_args);
    $kontrate = $rs->Fields();
    $karte = 
      array(
            //these fields are needed by the dbCommand 'newKart'
            'nr_karte' => '',
            'nr_kontrate' => $kontrate['nr_kontrate'],
            'nr_plani' => '',
            'date_krijimi' => get_curr_date('Y-m-d'),
            'titull_botimi' => $kontrate['titull_botimi'],
            'porositesi' => $kontrate['pala_porositese'],
            'sasia_cope' => $kontrate['tirazhi'],
            'formati_botimit' => $kontrate['formati'],
            'formati_i_prere' => '',
            'lloj_letre' => $kontrate['letra'],
            'kapaku' => $kontrate['kopertina'],
            'leter_per_fashikull' => '',
            'leter_per_skarcitet_makine' => '',
            'leter_per_skarcitet_libri' => '',
            'leter_per_skarcitet_numeratori' => '',
            'shuma_flete' => '',
            'leter_per_gjithe_botimin' => '',
            //these fields get some values from the contract
            'faqe_doreshkrim' => $kontrate['faqe_te_planifikuara'],
            'foto_1c' => $kontrate['foto_kalk'] + $kontrate['foto_film'],
            'kapaku_color' => $kontrate['nr_ngjyre_kopertine'],
            'lloji_lidhjes' => $kontrate['qepja'],
            'lloji_letres' => $kontrate['letra'],
            'shtyp_color' => $kontrate['nr_ngjyra_teksti'],
            'tirazhi' => $kontrate['tirazhi'],
            'lloji_lidhjes' => $kontrate['qepja'],
            'formati_perfundimtar' => $kontrate['formati_perfundimtar']
            );

    $this->add_new_tcard($karte);
    WebApp::execDBCmd('saveKontratFields', $karte);

    $interface_title = 'Kartat Teknologjike-->Ndrysho';
    WebApp::setSVar('interface_title', 'Kartat Teknologjike-->Ndrysho');
    WebApp::setSVar('interface', 'botimet/kartat/edit');
  }

  function onParse()
    {
      global $event;
      /*
      //save the content of the tab when the tab is changed
      if ($event->target=='tabs::tcEdit' and $event->name=='select')
        {
          //$this->on_save($event->args);
          WebApp::message('saved');
        }
      */

      //if nr_kontrate is set, then create a new kart 
      //based on the kontrate with this number
      if ($event->target=='main' and $event->name=='select'
          and isset($event->args['nr_kontrate']))
        {
          $this->on_create_from_contract($event->args);
        }

      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getKart");
          $close_date = $rs->Field("date_mbyllje");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table
          $rs = WebApp::execQuery("SHOW FIELDS FROM kartat");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));

          //title
          $title = "Krijo Karte Teknologjike";
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getKart");
          $fields = $rs->Fields();
          $this->explode_vars($fields);
          WebApp::addVars($fields);
          //title
          $nr_karte = $fields['nr_karte'];
          $titull_botimi = $fields['titull_botimi'];
          $title = "Karta Teknologjike: $nr_karte: $titull_botimi";
        }

      WebApp::addVar("title", $title);
    }

  function add_new_tcard($params)
    {
      //find a kart_id for the new project
      $rs = WebApp::openRS("getLastKartId");
      $last_kart_id = $rs->Field("last_kart_id");
      $kart_id = (($last_kart_id==UNDEFINED) ? 1 : $last_kart_id+1);

      //add the new kart
      $params["kart_id"] = $kart_id;
      $params["date_krijimi"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newKart", $params);

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'kartat_rs->recount' to "true"
      //so that the records are counted again
      //and the new kart is displayed at the end of the list
      WebApp::setSVar("kartat_rs->recount", "true");

      //set the new kart_id
      $this->setSVar("kart_id", $kart_id);
    }

  /**
   * According to the selected tab, explode the composed variables
   * that come from DB, so that they can fill the template properly.
   */
  function explode_vars(&$arr_vars)
  {
    $tab = WebApp::getSVar('tabs::tcEdit->selected_item');
    switch ($tab)
      {
      case 'te_pergjithshme':
        $this->explode_lloj_letre($arr_vars, 'lloj_letre');
        $this->explode_lloj_letre($arr_vars, 'kapaku');
        break;
      case 'kompjuteri':
        break;
      case 'montimi':
        break;
      case 'shtypi':
        break;
      case 'liberlidhja':
        break;
      case 'udhezime':
        break;
      case 'skemat':
        $this->explode_skemat($arr_vars);
        break;
      }
  }

  /**
   * According to the selected tab, implode some variables that come
   * from the template, so that they can be saved in DB properly.
   */
  function implode_vars(&$arr_vars)
  {
    $tab = WebApp::getSVar('tabs::tcEdit->selected_item');
    switch ($tab)
      {
      case 'te_pergjithshme':
        $this->implode_lloj_letre($arr_vars, 'lloj_letre');
        $this->implode_lloj_letre($arr_vars, 'kapaku');
        break;
      case 'kompjuteri':
        break;
      case 'montimi':
        break;
      case 'shtypi':
        break;
      case 'liberlidhja':
        break;
      case 'udhezime':
        break;
      case 'skemat':
        $str_skemat = serialize($arr_vars);
        $arr_vars['skemat'] = $str_skemat;
        break;
      }
  }

  /**
   * Get the value of $var_name (which is like: 61x36/250/bindakot)
   * extract the vars p1=61, p2=36, pesha=250, lloji=bindakot
   * and add them to the array $fields.
   */
  function explode_lloj_letre(&$arr_vars, $var_name)
  {
    $var_value = $arr_vars[$var_name];
    list($permasat,$pesha,$lloji) = explode('/', $var_value, 3);
    list($p1,$p2) = explode('x', $permasat, 2);

    $arr_vars[$var_name.'_p1'] = $p1;
    $arr_vars[$var_name.'_p2'] = $p2;
    $arr_vars[$var_name.'_pesha'] = $pesha;
    $arr_vars[$var_name.'_lloji'] = $lloji;
  }

  /**
   * Get the vars p1, p2, pesha, lloji, construct 'p1xp2/pesha/lloji'
   * and add it as the value of $var_name in the array $params
   */
  function implode_lloj_letre(&$arr_vars, $var_name)
  {
    $p1 = $arr_vars[$var_name.'_p1'];
    $p2 = $arr_vars[$var_name.'_p2'];
    $pesha = $arr_vars[$var_name.'_pesha'];
    $lloji = $arr_vars[$var_name.'_lloji'];
    $arr_vars[$var_name] = $p1.'x'.$p2.'/'.$pesha.'/'.$lloji;
  }

  function explode_skemat(&$arr_vars)
  {
    $str_skemat = $arr_vars['skemat'];
    if (trim($str_skemat)=='')
      {
        for ($i=1; $i <= 6; $i++)
          {
            $arr_vars["page$i-text"] = '';
            $arr_vars["page$i-nr-left"] = '';
            $arr_vars["page$i-nr-right"] = '';
          }
        return;
      }

    $arr = unserialize($str_skemat);
    while (list($var, $value) = each($arr))
      {
        $arr_vars[$var] = $value;
      }
  }
}
?>