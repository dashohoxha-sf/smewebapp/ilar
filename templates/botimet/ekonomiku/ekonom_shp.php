<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

/**
 * @package botimet
 * @subpackage kontratat
 */
class ekonom_shp extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      $this->addSVar("botim_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getKontrat");
          $close_date = $rs->Field("close_date");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table projects
          $rs = WebApp::execQuery("SHOW FIELDS FROM botimet");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));

          //title
          $title = "Krijo Kontrate";
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getKontrat");
          $fields = $rs->Fields();
          WebApp::addVars($fields);
          //title
          $autorizim = $fields['nr_kontrate'];
          $titull_botimi = $fields['titull_botimi'];
          $title = "Kontrata: $autorizim: $titull_botimi";
        }

      WebApp::addVar("title", $title);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      $params = $event_args;
      $params["formati_botimit"] = $this->get_formati_botimit($event_args);
      if ($mode=="add")
        {
          $this->add_new_kontrate($params);
        }
      else //mode=="edit"
        {
          WebApp::execDBCmd("updateKontrat", $params);
        }
    }

  /** create a new contract with almost the same data as this one */
  function on_clone($event_args)
  {
    $rs = WebApp::openRS("getKontrat");
    $fields = $rs->Fields();
    $fields['nr_kontrate'] = '';
    $fields['creation_date'] = get_curr_date('Y-m-d');
    $this->add_new_kontrate($fields);
  }

  function on_close($event_args)
    {
      $params["close_date"] = get_curr_date("Y-M-D");
      WebApp::execDBCmd("closeKontrat", $params);
    }

  /** create a new technological card from this contract */
  function on_new_tcard($event_args)
    {
    }

  function get_formati_botimit($event_args)
    {
      $permasa1 = $event_args["permasa1"];
      $permasa2 = $event_args["permasa2"];
      $pesha = $event_args["pesha"];
      $lloji = $event_args["lloji"];
      $lloj_pune = $event_args["lloj_pune"];
      //formati vetem per lloj pune shtyp
       
      $formati_botimit= $permasa1."/".$permasa2."/".$pesha."gr/".$lloji;
      //WebApp::debug_msg($formati_botimit, "formati_botimit: ");
      return $formati_botimit;
    }

  function add_new_kontrate($params)
    {
      //find a botim_id for the new project
      $rs = WebApp::openRS("getLastKontratId");
      $last_botim_id = $rs->Field("last_botim_id");
      $botim_id = (($last_botim_id==UNDEFINED) ? 1 : $last_botim_id+1);

      //add the new kontrat
      $params["botim_id"] = $botim_id;
      $params["data"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newKontrat", $params);

      //after a project is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'kontratat->recount' to "true"
      //so that the records are counted again
      //and the new kontrat is displayed at the end of the list
      WebApp::setSVar("kontratat_rs->recount", "true");

      //set the new botim_id
      $this->setSVar("botim_id", $botim_id);
    }
}
?>