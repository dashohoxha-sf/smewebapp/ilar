<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
* @subpackage user_profile
*/

class user_profile extends WebObject
{
  function on_save($event_args)
    {
      WebApp::execDBCmd("saveUserProfile", $event_args);

      $new_password = $event_args["new_password"];
      if ($new_password != "")
        {
          //check that the old password supplied is correct
          $rs = WebApp::openRS("getPassword");
          $password = $rs->Field("password");
          $old_password = $event_args["old_password"];
          if ($old_password==$password)
            {
              //save the new password
              $query_vars["password"] = $new_password;
              WebApp::execDBCmd("savePassword", $query_vars);
              WebApp::message("Fjalekalimi u ndryshua me sukses.");
            }
          else
            {
              $msg = "Fjalekalimi i vjeter nuk eshte i rregullt!\n"
                ."Fjalekalimi nuk u ndryshua. Ju lutem provoni perseri .";
              WebApp::message($msg);
            }         
        }
    }

  function onRender()
    {
      $rs = WebApp::openRS("getUserProfile");
      WebApp::addVars($rs->Fields());
    }
}
?>