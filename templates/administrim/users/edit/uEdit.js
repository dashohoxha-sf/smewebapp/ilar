// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function add()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.uEdit);
  event_args += ";access_rights=" + getCheckedMenuItems();
  SendEvent('uEdit', 'add', event_args);
}

function save()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.uEdit);
  event_args += ";access_rights=" + getCheckedMenuItems();
  SendEvent('uEdit', 'save', event_args);
}

function del()
{
  var msg = "Ju jeni duke fshire  perdoruesin.";
  if (!confirm(msg))  return;
  SendEvent('uEdit', 'delete');
}

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.uEdit;
  var username = form.username.value;

  //username should not be empty, otherwise 
  //the user cannot be accessed anymore
  username = username.replace(/ +/, '');
  if (username=='')
    {
      alert("Ju lutem shkruani nje emer perdoruesit.");
      form.username.focus();
      return true;
    }
    
  return false;
}
