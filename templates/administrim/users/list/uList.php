<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
* @subpackage users
*/

class uList extends WebObject
{
  function init()
    {
      $this->addSVar("currentUser", UNDEFINED);      
      //set current the first user in the list
      $this->selectFirst();  
    }

  /** set currentUser as the first user in the list */
  function selectFirst()
    {  
      $rs = WebApp::openRS("selected_users");
      $first_user = $rs->Field("user_id");
      $this->setSVar("currentUser", $first_user);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("selected_users->current_page", $page);
      $this->selectFirst();  
    }

  function on_refresh($event_args)
    {
      //recount the selected users
      WebApp::setSVar("selected_users->recount", "true");

      //select the first one in the current page
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      //set currentUser to the selected one
      $rs = WebApp::openRS("getUser", $event_args);
      if ($rs->EOF())
        {
          $this->selectFirst();   
        }
      else
        {
          $user = $event_args["user_id"];
          $this->setSVar("currentUser", $user);
        }
    }

  function on_addNewUser($event_args)
    {
      //when the button AddNewUser is clicked
      //make currentUser UNDEFINED
      $this->setSVar("currentUser", UNDEFINED);
      WebApp::setSVar("selected_users->recount", "true");
    }

  function onParse()
    {
      //recount the selected users
      WebApp::setSVar("selected_users->recount", "true");
    }
}
?>