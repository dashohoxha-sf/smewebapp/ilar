<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
* @subpackage users
*/

include_once FORM_PATH."formWebObj.php";

class pEdit extends formWebObj
{
  var $newUser_data =  array( 
                             "username"     => "",
                             "firstname"    => "",
                             "lastname"     => "",
                             "titulli"      => "",
                             "access_rights"    => "",
                             "tel1"     => "",
                             "paga"     => "",
                             "e_mail"       => "",
                             "address"      => "",
                             "notes"        => ""
                             );

  function init()
    {
      $this->addSVar("mode", "add");
      $this->addSVar("user", UNDEFINED);
    }

  /** add the new user in database */
  function on_add($event_args)
    {
      //check that such a username does not exist in DB
      $username = $event_args["username"];
      $rs = WebApp::openRS("getUserID", compact("username"));
      if (!$rs->EOF())
        {
          $msg = "Ky emer perdoruesi  '$username' tashme ekziston.\n"
            . "Ju lutem zgjidhni nje emer tjeter.";
          WebApp::message($msg);
          $this->newUser_data = $event_args;
          $this->newUser_data["username"] = "";
          return;
        }

      //add the user
      WebApp::execDBCmd("addUser", $event_args);

      //acknowledgment message
      WebApp::message("Perdoruesi u shtua, shtoni nje tjeter.");
    }

  /** delete the user and his access rights */
  function on_delete($event_args)
    {
      WebApp::execDBCmd("deleteUser");

      //the currentUser is deleted,
      //set current the first user in the list
      $uList = WebApp::getObject("uList");
      $uList->selectFirst();

      //acknowledgment message
      WebApp::message("Perdoruesi u fshi.");
    }

  /** save the changes */
  function on_save($event_args)
    {
      WebApp::execDBCmd("updateUser", $event_args);
    }

  function onParse()
    {
      //get the current user from the list of users
      $user = WebApp::getSVar("uList->currentUser");

      $this->setSVar("user", $user);

      if ($user==UNDEFINED)
        {
          $this->setSVar("mode","add");
        }
      else
        {
          $this->setSVar("mode", "edit");
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $user_data = $this->newUser_data; 
        }
      else
        {
          $rs = WebApp::openRS("currentUser");
          $user_data = $rs->Fields();
        }
      WebApp::addVars($user_data);      
    }
}
?>