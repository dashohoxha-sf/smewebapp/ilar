<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
* @subpackage users
*/

class fUsers extends WebObject
{
  function init()
    {
      $this->addSVars( array(
                             'username' => '',
                             'emri' => '',
                             'filtro_sipas_reparteve' => 'true',
                             'offseti' => '',
                             'parapreg' => '',
                             'lidhja' => '',
                             'administrata' => '',
                             'tjeter' => '',
                             'filter' => '1=1'
                             ) );
    }

  function onParse()
    {
      $this->buildFilter();
    }

  function buildFilter()
    {
      //get state vars
      extract($this->getSVars());

      if ($username=='')
        {  $cond1 = '';  }
      else
        {  $cond1 = '(username LIKE "%'.$username.'%")'; }

      if ($emri=='')
        {  $cond2 = '';  }
      else
        {
          $cond2 = '(firstname LIKE "%'.$emri.'%" '
            . 'OR lastname LIKE "%'.$emri.'%")'; 
        }

      if ($offseti=='')
        {  $cond3 = '';  }
      else
        {
          $cond3 = '(titulli="normist_shtypje" '
            . 'OR titulli="punetor_shtypje")';
        }

      if ($parapreg=='')
        {  $cond4 = '';  }
      else
        {
          $cond4 = '(titulli="normist_parapreg" '
            . 'OR titulli="punetor_parapreg")'; 
        }

      if ($lidhja=='')
        {  $cond5 = '';  }
      else
        {
          $cond5 = '(titulli="normist_lidhja" '
            . 'OR titulli="punetor_lidhja")'; 
        }

      if ($administrata=='')
        {  $cond6 = '';  }
      else
        {
          $cond6 = '(titulli="administrata" '
            . 'OR titulli="administrator" '
            . 'OR titulli="teknolog" '
            . 'OR titulli="financier" '
            . 'OR titulli="normist_offseti" '
            . 'OR titulli="normist_parapreg" '
            . 'OR titulli="normist_lidhja")';
        }

      if ($tjeter=='')
        {  $cond7 = '';  }
      else
        {
          $cond7 = '( titulli="tjeter" '
            . 'OR titulli="" OR (titulli IS NULL) )';
        }

      $arr_cond = array();
      if ($cond3<>'')  $arr_cond[] = $cond3;            
      if ($cond4<>'')  $arr_cond[] = $cond4;            
      if ($cond5<>'')  $arr_cond[] = $cond5;
      if ($cond6<>'')  $arr_cond[] = $cond6;
      if ($cond7<>'')  $arr_cond[] = $cond7;

      //combine all role conditions into $cond8 with OR
      $cond8 = '';
      if (sizeof($arr_cond) > 0)
        {
          $cond8 = implode(' OR ', $arr_cond);
        }

      $arr_cond = array();
      if ($cond1<>'')  $arr_cond[] = $cond1;            
      if ($cond2<>'')  $arr_cond[] = $cond2;            
      if ($cond8<>'')  $arr_cond[] = $cond8;            
  
      $condition = '';
      if (sizeof($arr_cond) > 0)
        {
          $condition = implode(' AND ', $arr_cond);
        }
      if ($condition=='')   { $condition = '1=1'; }

      $this->setSVar('filter', $condition);
    }  
}
?>

