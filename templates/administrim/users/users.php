<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
* @subpackage users
*/
class users extends WebObject
{
  function init()
    {
      $this->addSVar('edit_file', 'edit/uEdit.html');  
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_interface($event->args);
        }
    }

  function select_interface()
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
        case 'perdoruesit':
          $interface_title = 'Perdoruesit';
          $edit_file = 'edit/uEdit.html';
          $this->init_fUsers_vars();
          break;

        case 'punetoret':
          $interface_title = 'Punetoret';
          $edit_file = 'punetor/pEdit.html';
          $this->init_fUsers_vars();
          break;

        case 'punetoret/offseti':
          $interface_title = 'Punetoret e Offsetit';
          $edit_file = 'punetor/pEdit_normist.html';
          $this->init_fUsers_vars();
          WebApp::setSVar('fUsers->filtro_sipas_reparteve', 'false');
          WebApp::setSVar('fUsers->offseti', 'checked');
          break;

        case 'punetoret/parapreg':
          $interface_title = 'Punetoret e Parapregatitjes';
          $edit_file = 'punetor/pEdit_normist.html';
          $this->init_fUsers_vars();
          WebApp::setSVar('fUsers->filtro_sipas_reparteve', 'false');
          WebApp::setSVar('fUsers->parapreg', 'checked');
          break;

        case 'punetoret/lidhja':
          $interface_title = 'Punetoret e Liberlidhjes';
          $edit_file = 'punetor/pEdit_normist.html';
          $this->init_fUsers_vars();
          WebApp::setSVar('fUsers->filtro_sipas_reparteve', 'false');
          WebApp::setSVar('fUsers->lidhja', 'checked');
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('edit_file', $edit_file);
    }

  function init_fUsers_vars()
    {
      WebApp::setSVar('fUsers->username', '');
      WebApp::setSVar('fUsers->emri', '');
      WebApp::setSVar('fUsers->filtro_sipas_reparteve', 'true');
      WebApp::setSVar('fUsers->offseti', '');
      WebApp::setSVar('fUsers->parapreg', '');
      WebApp::setSVar('fUsers->lidhja', '');
      WebApp::setSVar('fUsers->administrata', '');
      WebApp::setSVar('fUsers->tjeter', '');
      WebApp::setSVar('fUsers->filter', '1=1');
    }
}
?>