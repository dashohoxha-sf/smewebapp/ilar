<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package administrim
*/

class administrim extends WebObject
{
  function init()
    {
      $this->addSVar('module', 'user_profile/user_profile.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_interface($event->args);
        }
    }

  function select_interface($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
        default:
        case 'user_profile':
          $interface_title = 'Te Dhenat e Perdoruesit';
          $module  = 'user_profile/user_profile.html';
          break;

        case 'superuser':
          $interface_title = 'Superuser';
          $module  = 'superuser/superuser.html';
          break;

        case 'ndrysho_daten':
          if (TEST)
            {
              $interface_title = 'Ndrysho Daten';
              $module = 'ndrysho_daten/ndrysho_daten.html';
            }
          break;

          //Punetoret
        case 'perdoruesit':
        case 'punetoret':
        case 'punetoret/parapreg':
        case 'punetoret/offseti':
        case 'punetoret/lidhja':
          $interface_title = 'Punetoret';
          $module = 'users/users.html';
          break;

        case 'backup_restore':
          $interface_title = 'Backup/Restore';
          $module = 'backup/backup.html';
          break;
        case 'tabelat':
          $interface_title = 'Modifiko Tabelat';
          $module = 'kartat/listo/listo.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }
}
?>