<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package print
 */
class print_template extends WebObject
{
  function init()
    {
      $this->addSVar("print_file", UNDEFINED);
    }

  function on_select($event_args)
    {
      $interface = $event_args["interface"];
      switch ($interface)
        {
        default:
          $this->title = WebApp::getSVar('interface_title');
          $print_file = WebApp::getSVar("module");
          break;

          //botimet
        case 'botimet/kontratat/edit':
          $this->title = 'Kontrate';
          $print_file = "botimet/kontratat/print/konPrint.html";
          WebApp::setSVar("konPrint->botim_id", $event_args["botim_id"]);
          break;
        case 'botimet/kartat/edit':
          $this->title = 'Karte Teknologjike';
          $print_file = "botimet/kartat/print/tcPrint.html";
          WebApp::setSVar("tcPrint->kart_id", $event_args["kart_id"]);
          break;

          //buletinet (offseti)
        case 'buletinet/offseti/listo':
          $print_file = "buletinet/offseti/listo/boList.html";
          break;
        case 'buletinet/offseti/printo':
          $this->title = "Reparti Offset-->Buletinet Ditore-->Printo";
          $print_file = "buletinet/offseti/edit/boEdit.html";
          WebApp::setSVar("boEdit->mode", "edit");
          WebApp::setSVar("boEdit->buletin_id", $event_args["buletin_id"]);
          break;

          //buletinet (parapreg)
        case 'buletinet/parapreg/listo':
          $print_file = "buletinet/parapreg/listo/bpList.html";
          break;
        case 'buletinet/parapreg/printo':
          $this->title = "Reparti Parapregatitje-->Buletinet Ditore-->Printo";
          $print_file = "buletinet/parapreg/edit/bpEdit.html";
          WebApp::setSVar("bpEdit->mode", "edit");
          WebApp::setSVar("bpEdit->buletin_id", $event_args["buletin_id"]);
          break;

          //buletinet (lidhja)
        case 'buletinet/lidhja/listo':
          $print_file = "buletinet/lidhja/listo/blList.html";
          break;
        case 'buletinet/lidhja/printo':
          $this->title = "Reparti Liberlidhje-->Buletinet Ditore-->Printo";
          $print_file = "buletinet/lidhja/edit/blEdit.html";
          WebApp::setSVar("blEdit->mode", "edit");
          WebApp::setSVar("blEdit->buletin_id", $event_args["buletin_id"]);
          break;

          //Magazina
        case 'magazina/leter/faturat/listo':
        case 'magazina/boje/faturat/listo':
          $print_file = "magazina/faturat/listo/faturList.html";
          break;
        case 'magazina/leter/levizjet':
        case 'magazina/boje/levizjet':
          $print_file = "magazina/levizjet/levizjeList.html";
          break;
        case 'magazina/faturat/printo':
          $magazin_id = $event_args['magazin_id'];
          $fature_id = $event_args['fature_id'];
          $hyrje_dalje = $event_args['lloj_fature'];

          $this->title = "Magazina ".ucfirst($magazin_id)
            . "-->Fature ".ucfirst($hyrje_dalje);
          $print_file = "magazina/faturat/edit/faturEdit.html";

          WebApp::setSVar('magazina->id', $magazin_id);
          WebApp::setSVar('faturEdit->fature_id', $fature_id);
          WebApp::setSVar('faturEdit->hyrje_dalje', $hyrje_dalje);
          WebApp::setSVar('faturEdit->mode', 'edit');
          break;

          //Raportet-->Shpenzimet
        case "rpt_shpenzimet":
          $print_file = "raportet/shpenzimet/permbledhese/raport.html";
          break;
        case "rpt_shpenzimet/sasi":
        case "rpt_shpenzimet/sasi/offseti":
        case "rpt_shpenzimet/sasi/parapreg":
        case "rpt_shpenzimet/sasi/lidhja":
        case "rpt_shpenzimet/vlere":
        case "rpt_shpenzimet/vlere/offseti":
        case "rpt_shpenzimet/vlere/parapreg":
        case "rpt_shpenzimet/vlere/lidhja":
          $print_file = "raportet/shpenzimet/rptShpenzimet.html";
          break;

          //Raportet-->Punet
        case "rpt_punet/parapreg":
          $print_file = "raportet/punet/parapreg/rppPunet.html";
          break;
        case "rpt_punet/offseti":
          $print_file = "raportet/punet/offseti/rpoPunet.html";
          break;
        case "rpt_punet/lidhja":
          $print_file = "raportet/punet/lidhja/rplPunet.html";
          break;
        }
      WebApp::setSVar("print_file", $print_file);
      WebApp::addGlobalVar("title", $this->title);
  }
}
?>