// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function date_not_in_future(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = arr_date[0];
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date > curr_date)
    {
      alert("Kujdes: Data e zgjedhur eshte pas dates se sotme.");
      //set_current_date(txtbox);
    }
}

function date_not_in_past(txtbox)
{
  var curr_date = get_curr_date();
  var str_date = txtbox.value;
  var arr_date = str_date.split('/');
  var day = (arr_date[0]*1) + 1;
  var month = arr_date[1] - 1;
  var year = arr_date[2];
  var date = new Date(year, month, day);

  if (date < curr_date)
    {
      alert("Kujdes: Data e zgjedhur eshte perpara dates se sotme.");
      //set_current_date(txtbox);
    }
}

function set_current_date(txtbox)
{
  var curr_date = get_curr_date();
  var day = curr_date.getDate();
  var month = curr_date.getMonth() + 1;
  var year = curr_date.getFullYear();

  if (day < 10)    day   = "0" + day;
  if (month < 10)  month = "0" + month;
  str_date = day + '/' + month + '/' + year;
  txtbox.value = str_date;
}

function save()
{
  var form = document.blEdit;
  var event_args = getEventArgs(form);

  var mode = session.getVar("blEdit->mode");
  if (mode=="add")  saveFormData(form);
  SendEvent("blEdit", "save", event_args);
}

function close_doc()
{
  GoTo("thisPage?event=blEdit.close");
}

/** returns false if the document is closed */
function editable()
{
  var closed = session.getVar("blEdit->closed");
  if (closed=="true")
    {
      alert("Ky buletin �sht� i mbyllur dhe nuk mund t� modifikohet.");
      return false;
    }
    
  var mode = session.getVar("blEdit->mode");
  if (mode=="add")
    {
      alert("Ky buletin eshte i pa ruajtur.");
      return false;
    }

  return true;
}
