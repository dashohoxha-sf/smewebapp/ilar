<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage lidhja
*/

class Shpenzimet
{
  function get_shpenzimet($event_args)
    {
      //get the data from the html form
      extract($event_args);

      //get the data of the buletin
      $rs = WebApp::openRS("getBuletin");
      $buletin = $rs->Fields();

      //gjej ne cilen makine
      $makina = $buletin["makina"];
      
      //get the coeficients
      $rs = WebApp::execQuery("SELECT emri, sasia, vlera FROM koeficientet");
      $koef_sasi = array();
      $koef_vlere = array();
      while (!$rs->EOF())
        {
          $emri = $rs->Field("emri");
          $sasia = $rs->Field("sasia");
          $vlera = $rs->Field("vlera");
          $koef_sasi[$emri] = $sasia;
          $koef_vlere[$emri] = $vlera;
          $rs->MoveNext();
        }
      
      //WebApp::debug_msg($lloj_pune, "lloj pune: ");
      if ($lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak")
        {
          //llogarit fletet tipografike vetem kur lloj i punes eshte shtyp
          $ftpg = 
            $this->llogarit_fletet_tipografike($sasia_shtypur,
                                               $permasa1, 
                                               $permasa2,
                                               $ngjyrat,
                                               $ndarjet_letres,
                                               $nr_rulash,
                                               $makina);
        }
      else
        {
          $ftpg = 0.0;
        }
      
      if ($lloj_pune == "Shtyp" and $ana == "1")
        {
          $letra_sasi = 
            $this->llogarit_sasia_e_letres($sasia_shtypur,
                                           $ndarjet_letres,
                                           $nr_rulash,
                                           $makina);
          //llogarit vleren e letres
          $letra_vlere = 
            $this->llogarit_vleren_e_letres($letra_sasi, 
                                            $permasa1, 
                                            $permasa2,
                                            $pesha, 
                                            $lloji);
                  
               
        }
      else
        {
          $letra_vlere = 0;
          $letra_sasi = 0;
        }
      //WebApp::debug_msg($ana, "ana: ");
    
      
      if ($lloj_pune == "Shtyp")
        {
          $boja_sasi = 
            $this->llogarit_boja_sasi($ftpg,$koef_sasi,$kart_id,$makina);
          $boja_vlere = 
            $this->llogarit_boja_vlere($boja_sasi,$koef_vlere,$makina );
        }
      if ($lloj_pune == "Shtyp varak")
        {
          $boja_varak_sasi = 
            $this->llogarit_boja_varak_sasi($ftpg, $koef_sasi);
        }
          
      //if shtyp llak  //nqs eshte punuar me llak llog sasi dhe vlere
            
      if ($lloj_pune == "Shtyp llak")
        {      
          $boja_llak_sasi = 
            $this->llogarit_boja_llak_sasi($ftpg, $koef_sasi);

      
          //llogarit vleren e pages
          $paga_vlere = 
            $this->llogarit_vleren_e_pages($lloj_pune,
                                           $ore_shtypur, 
                                           $buletin["makinisti"],
                                           $buletin["punetor1"],
                                           $buletin["punetor2"],
                                           $buletin["punetor3"],
                                           $buletin["punetor4"],
                                           $buletin["punetor5"]);
      
          //variablat me te dhenat per sasine e koef, jane me /1000 ftpg
          $alkohol_sasi = ($ftpg * $koef_sasi["alkool"])/1000;
          //WebApp::debug_msg($koef_sasi["alkool"], "alkol: ");
          $wash_sasi = ($ftpg * $koef_sasi["wash"])/1000;
          $rc_sasi = ($ftpg * $koef_sasi["rc"])/1000;
          $sol_ujor_sasi = ($ftpg * $koef_sasi["sol_ujor"])/1000;
          $g_arabike_sasi = ($ftpg * $koef_sasi["gome_arabike"])/1000;
          $benzine_sasi = ($ftpg * $koef_sasi["benzine"])/1000;
      
          $arr_shpenzimet = 
            array(
                  "letra_sasi" => $letra_sasi,
                  "letra_vlere" => $letra_vlere,
                  "flete_tipografike" => $ftpg,
                  "boja_sasi" => $boja_sasi,
                  "boja_vlere" => $boja_vlere,
                  "alkohol_sasi" => $alkohol_sasi,
                  "alkohol_vlere" => ($alkohol_sasi * $koef_vlere["alkool"]),
                  "wash_sasi" => $wash_sasi,
                  "wash_vlere" => $wash_sasi * $koef_vlere["wash"],
                  "rc_sasi" => $rc_sasi,
                  "rc_vlere" => ($rc_sasi * $koef_vlere["rc"]),
                  "sol_ujor_sasi" => $sol_ujor_sasi,
                  "sol_ujor_vlere" => $sol_ujor_sasi * $koef_vlere["sol_ujor"],
                  "g_arabike_sasi" => $g_arabike_sasi ,
                  "g_arabike_vlere" => $g_arabike_sasi * $koef_vlere["gome_arabike"],
                  "benzine_sasi" => $benzine_sasi,
                  "benzine_vlere" => ($benzine_sasi * $koef_vlere["benzine"]),
                  "boja_llak_sasi" => $boja_llak_sasi,
                  "boja_llak_vlere" => ($boja_llak_sasi * $koef_vlere["llak"]),
                  "boja_varak_sasi" => $boja_varak_sasi,
                  "boja_varak_vlere" => ($boja_varak_sasi * $koef_vlere["boje_varak"]),
                  "shpenzimet_shtese_vlere"=>$koef_sasi["shpenzime_shtese"] * $paga_vlere,
                  "paga_vlere" => $paga_vlere,
                  );

          $str_shpenzimet = serialize($arr_shpenzimet);
          WebApp::debug_msg("str_shpenzimet: '$str_shpenzimet'"); 
          return $str_shpenzimet;
        }
    }

  function llogarit_sasia_e_letres($sasia_shtypur, 
                                   $ndarjet_letres, 
                                   $nr_rulash, 
                                   $makina)
    {
      $letra_sasi = 0.0;
      if ($makina=='Rrotative')
        {
          $letra_sasi = ($sasia_shtypur * $nr_rulash)/$ndarjet_letres;
        }
      else
        {
          $letra_sasi = $sasia_shtypur/$ndarjet_letres;
        }
      WebApp::debug_msg("letra_sasi: '$letra_sasi'");  //debug
      return $letra_sasi;
    }

  function llogarit_vleren_e_letres($letra_sasi, 
                                    $permasa1, 
                                    $permasa2,
                                    $pesha, 
                                    $lloji)
    {
      $letra_vlere = 0.0;
      
      //marr cmimin e leters per kg  
      $rs = WebApp::execQuery("SELECT cmimi_kg FROM lloji_leter WHERE lloji= '$lloji'");
            
      $koef_cmim = array();
      while (!$rs->EOF())
        {
          $cmimi = $rs->Field("cmimi_kg");
          $koef_cmim[$cmimi] = $cmimi;
          $rs->MoveNext();
        }
      WebApp::debug_msg("cmimi: '$cmimi'");  //debug
      
      
      $letra_vlere = ( $permasa1 * $permasa2 * $pesha * $cmimi)/10000000;
      $letra_vlere = $letra_vlere * $letra_sasi ;
      WebApp::debug_msg("letra_vlere: '$letra_vlere'");  //debug

      return $letra_vlere;
    }

  function llogarit_fletet_tipografike($sasia_shtypur, 
                                       $permasa1, 
                                       $permasa2,
                                       $ngjyrat, 
                                       $ndarjet_letres, 
                                       $nr_rulash,
                                       $makina)
    {
      if ($makina=='Rrotative')
        {
          $fletet_tipografike = ($permasa1*$permasa2)/5246; 
          $fletet_tipografike = $fletet_tipografike * (($sasia_shtypur * $nr_rulash)/$ndarjet_letres);
          $fletet_tipografike = $fletet_tipografike * $ngjyrat * 2;
        }
      else
        {
          $fletet_tipografike =( ($permasa1*$permasa2)/(5246 * $ndarjet_letres)) * $sasia_shtypur * $ngjyrat;
            
        }
      WebApp::debug_msg("fletet_tipografike: '$fletet_tipografike'");  //debug
      return $fletet_tipografike;
    }

  function llogarit_boja_sasi($ftpg, $koef_sasi, $kart_id, $makina)
    {
      $boja_sasi = 0.0; 
      //gjej gjinine e botimit
      $rs = WebApp::execQuery("SELECT gjinia FROM kartat_teknologjike WHERE kart_id='$kart_id'");
      //print $rs->toHtmlTable();     
      $lloji = array();
      while (!$rs->EOF())
        {
          
          $gjinia = $rs->Field("gjinia");
          $lloji[$gjinia] = $gjinia;
          $rs->MoveNext();
        }
      //WebApp::debug_msg("gjinia: '$gjinia'");
        
        
      if ($gjinia == 'Liber')
        {
          if ($makina == 'Rrotative')
            { 
              $koef_sasi = $koef_sasi["boje_rrotative"];
              WebApp::debug_msg("koef_sasi: '$koef_sasi'");
              $boja_sasi = ($ftpg * $koef_sasi)/1000;
            }
          else
            {
              $koef_sasi = $koef_sasi["boja_libra"];
              WebApp::debug_msg("koef_sasi: '$koef_sasi'");
              $boja_sasi = ($ftpg * $koef_sasi)/1000;
            }
        }
             
      else
        {
          $koef_sasi = $koef_sasi["boja_revista"];
          $boja_sasi = ($ftpg * $koef_sasi)/1000;
        }
      
       
       
      WebApp::debug_msg("boja_sasi: '$boja_sasi'");
      return $boja_sasi;
    }


  function llogarit_boja_vlere($boja_sasi,$koef_vlere,$makina )
    {
      $boja_vlere = 0.0;
          
      if ($makina=='Rrotative')
        {
          $koef_vlere = $koef_vlere["boje_rrotative"];
          WebApp::debug_msg("koef_vlere: '$koef_vlere'");
          $boja_vlere = $boja_sasi * $koef_vlere ;
        }
      else     
        {
          $koef_vlere = $koef_vlere["boja_libra"];
          WebApp::debug_msg("koef_vlere: '$koef_vlere'");
          $boja_vlere = $boja_sasi * $koef_vlere ;
        }
      WebApp::debug_msg("boja_vlere: '$boja_vlere'");
      return $boja_vlere;
    }
    
  function llogarit_boja_varak_sasi($ftpg, $koef_sasi)
    {
      $boja_varak_sasi = 0.0;
      $koef_varak_sasi = $koef_sasi["boje_varak"];
      $boja_varak_sasi = ($ftpg * $koef_varak_sasi)/1000;
        
      //WebApp::debug_msg("koef_varak_sasi: '$koef_varak_sasi'");
      //WebApp::debug_msg("boja_varak_sasi: '$boja_varak_sasi'");
      return $boja_varak_sasi;
    }
 
  function llogarit_boja_llak_sasi( $ftpg, $koef_sasi)
    {
      $boja_llak_sasi = 0.0;
      $koef_sasi = $koef_sasi["llak"];
      $boja_llak_sasi = ($ftpg * $koef_sasi)/1000;
           
        
      //WebApp::debug_msg("koef_sasi: '$koef_sasi'");
      //WebApp::debug_msg("boja_llak_sasi: '$boja_llak_sasi'");
      return $boja_llak_sasi;
    }
      
  function llogarit_vleren_e_pages($lloj_pune, $ore_shtypur, $makinisti, 
                                   $punetor1, $punetor2,$punetor3,$punetor4,$punetor5)
    {
      //get the salaries of the workers
      $rs = WebApp::execQuery("SELECT username, paga FROM users");
      //print $rs->toHtmlTable();
      $pagat = array();
      while (!$rs->EOF())
        {
          $username = $rs->Field("username");
          $paga = $rs->Field("paga");
          $pagat[$username] = $paga;
          $rs->MoveNext();
        }

      $paga_vlere = 0.0;
      if ($lloj_pune == "Rreg seksioni" 
          OR $lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak" 
          OR $lloj_pune == "Prerje" )
        {
          if ($ore_shtypur<>"")
            {
              if ($makinisti<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$makinisti];
                  //WebApp::debug_msg("paga_makinist: '$pagat[$makinisti]'");
                }
              if ($punetor1<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$punetor1];
                  //WebApp::debug_msg("paga_pun1: '$pagat[$punetor1]'");
                }
              if ($punetor2<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$punetor2];
                  //WebApp::debug_msg("paga_pun2: '$pagat[$punetor2]'");
                }
              if ($punetor3<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$punetor3];
                  //WebApp::debug_msg("paga_pun3: '$pagat[$punetor3]'");
                }
              if ($punetor4<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$punetor4];
                  // WebApp::debug_msg("paga_pun4: '$pagat[$punetor4]'");
                } 
              if ($punetor5<>NULL_VALUE)
                {
                  $paga_vlere += $ore_shtypur * $pagat[$punetor5];
                  // WebApp::debug_msg("paga_pun5: '$pagat[$punetor5]'");
                }   
            }
        }
      // WebApp::debug_msg("makinist: '$makinisti'");
      //WebApp::debug_msg("ore pune: '$ore_shtypur'");
      // WebApp::debug_msg("paga_vlere: '$paga_vlere'");
      return $paga_vlere;
    }
}
?>