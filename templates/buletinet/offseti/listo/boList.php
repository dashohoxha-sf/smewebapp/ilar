<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage offseti
*/

class boList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("bOffseti_rs->current_page", $page);
    }

  function on_rm($event_args)
    {
      WebApp::execDBCmd("rmbuletin", $event_args);
      WebApp::execDBCmd("rmAllPunet",$event_args);
    } 

  function onRender()
    {
      WebApp::setSVar("bOffseti_rs->recount", "true"); 
    
      $filter_condition = $this->get_filter_condition();
      WebApp::addVar("filter_condition", $filter_condition);
    }

  function get_filter_condition()
    {
      $conditions = array();

      //add data filter
      $data_filter = WebApp::getSVar("data->filter");
      $data_filter = str_replace("date_field", "data", $data_filter);
      $conditions[] = $data_filter;

      //add botimet filter
      $botimet_filter = WebApp::getSVar("fBuletinet->filter");
      if ($botimet_filter<>"")   $conditions[] = $botimet_filter;

      //build the filter condition
      $filter_condition = implode(" AND ", $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
}  
?>