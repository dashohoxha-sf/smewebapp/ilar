<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage offseti
*/

include_once FORM_PATH."formWebObj.php";
include_once TPL_PATH."buletinet/offseti/edit/add_pune/shpenzimet.php";

class boAddPune extends formWebObj
{
  function onRender()
    {
      global $webPage;
      
      //shton vend bosh tek autorizimi
      $rs = WebApp::openRS("listbox::autorizim");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      //shton vend bosh tek kart_id
      $rs = WebApp::openRS("listbox::kart_id");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      
      //shton vend bosh tek formatet e letres
      $rs = WebApp::openRS("listbox::permasa1");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::permasa2");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::pesha");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::lloji");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);      
    }
    
  function on_add($event_args)
    { 
      $event_args["ore_shtypur"] = $this->get_ore_shtypur($event_args);
      $event_args["autorizim"] = $this->get_autorizim_nr($event_args);
      
      $params = $event_args;
      $params["formati"] = $this->get_format($event_args);
      $params["ngjyrat"] = $this->get_ngjyrat($event_args);
      $params["perqind_realizuar"] = $this->get_perqind_realizuar($event_args);
      $shpenzimet = new Shpenzimet;
      $params["shpenzimet"] = $shpenzimet->get_shpenzimet($event_args); 
      WebApp::execDBCmd("addPune", $params);
      boEdit::update_oret();
    }

  function get_ore_shtypur($event_args)
    {
      $ore_shtypur = $event_args["ore_shtypur"];  
      if ($ore_shtypur <> "")    return $ore_shtypur;

      //perndryshe llogarite ne varesi te llojit te punes        

      $lloj_pune = $event_args["lloj_pune"];
      $sasia_shtypur = $event_args["sasia_shtypur"];
      $normativa = $this->get_normativ($lloj_pune);

      if ($lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak")
        {
          $ore_shtypur = ($sasia_shtypur / $normativa);
        }
      else
        {
          $ore_shtypur = ($sasia_shtypur * $normativa);
        }      
            
      return $ore_shtypur; 
    }

  function get_normativ($lloj_pune)
    {
      //gjej normativen
      $rs = WebApp::openRS("get_makina");
      $makina = $rs->Field("makina");
      $rs = WebApp::openRS("get_normativ", compact("makina", "lloj_pune"));
      $normativa = $rs->Field("normativat");

      return $normativa;
    }
    
  function get_autorizim_nr($event_args)
    {
      $kart_id = $event_args["kart_id"];
      $rs = WebApp::openRS("get_autorizim_nr", compact("kart_id"));
      $auto = $rs->Field("autorizim_nr");
      if ($auto==UNDEFINED)  $auto="";
      return $auto;
    }  

  function get_format($event_args)
    {
      //formati vetem per lloj pune shtyp
      $lloj_pune = $event_args["lloj_pune"];
      if ($lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak" 
          OR $lloj_pune == "Prerje")
        {
          $permasa1 = $event_args["permasa1"];
          $permasa2 = $event_args["permasa2"];
          $pesha = $event_args["pesha"];
          $lloji = $event_args["lloji"];
          $format = $permasa1."/".$permasa2."/".$pesha."gr/".$lloji;
        }
      else
        {
          $format = " ";
        }

      return $format;
    }
    
  function get_ngjyrat($event_args)
    {
      $lloj_pune = $event_args["lloj_pune"];
      //formati vetem per lloj pune shtyp
      if ($lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak" )
        {
          $ngjyrat = $event_args["ngjyrat"];
        }
      else
        {
          $ngjyrat = " ";
        }
      return $ngjyrat;
    }
  
  function get_perqind_realizuar($event_args)
    {
      $lloj_pune = $event_args["lloj_pune"];
      if ($lloj_pune == "Shtyp" 
          OR $lloj_pune == "Shtyp varak" 
          OR $lloj_pune == "Shtyp llak")
        {
          $sasia_shtypur = $event_args["sasia_shtypur"];
          $ore_shtypur = $event_args["ore_shtypur"];
          $normativa = $this->get_normativ($lloj_pune);

          //($sasia_shtypur* $normativa)/(1000*$ore_shtypur)
          $perqind_realizuar = $sasia_shtypur/($ore_shtypur * $normativa) ;
          $perqind_realizuar = $perqind_realizuar * 100;
        }
      else
        {
          $perqind_realizuar = 0;
        }
             
      return $perqind_realizuar;
    }
}
?>