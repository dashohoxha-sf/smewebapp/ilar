// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function addPune()
{
  if (!editable())  return;

  var form = document.boAddPune;
  
  if (!validate(form))  return;
  
  var event_args = getEventArgs(form);  
  GoTo("thisPage?event=boAddPune.add(" + event_args + ")");
}

/**
 * Return true if all input fields are OK,
 * otherwise return false.
 */
function validate(form)
{
  if (form.sasia_shtypur.value=="")
    {
      alert ("Ju lutem mos e lini bosh sasine.");
      form.sasia_shtypur.focus();
      return false;
    }
    
  if (form.lloj_pune.value=="Rreg seksioni" )
    {
      if (form.kart_id.value=="")
        {
          alert ("Ju lutem mos e lini bosh emrin e botimit.");
          form.kart_id.focus();
          return false;
        }
    }
  if (form.lloj_pune.value=="Prerje" )
    {
      if (form.kart_id.value=="")
        {
          alert ("Ju lutem mos e lini bosh emrin e botimit.");
          form.kart_id.focus();
          return false;
        }
    }
  
  if ( (form.lloj_pune.value =="Shtyp") 
       || (form.lloj_pune.value =="Shtyp varak") 
       || (form.lloj_pune.value =="Shtyp llak") )
    { 
      if (form.kart_id.value=="")
        {
          alert ("Ju lutem mos e lini bosh emrin e botimit.");
          form.kart_id.focus();
          return false;
        }
         
      if (form.nr_rulash.value == "")
        {
          alert ("Ju lutem mos e lini bosh fushen 'Nr Rulash'.");
          form.nr_rulash.focus();
          return false;
        }
         
      if (form.ana.value == "")
        {
          alert ("Ju lutem mos e lini bosh fushen 'Ana'.");
          form.ana.focus();
          return false;
        }
         
      /*if (form.ore_shtypur.value=="")
        {
        alert ("Ju lutem mos e lini bosh fushen 'Ore'.");
        form.ore_shtypur.focus();
        return false;
        }*/
             
      if (form.ngjyrat.value == "")
        {
          alert ("Duhet te plotesoni fushen 'Ngjyrat'.");
          form.ngjyrat.focus();
          return false;
        }   
             
      if (form.ndarjet_letres.value == "")
        {
          alert ("Duhet te plotesoni fushen 'Ndarjen e letres'.");
          form.ndarjet_letres.focus();
          return false;
        }   
    }

  return true;
}
