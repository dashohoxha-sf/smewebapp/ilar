<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage offseti
*/

include_once FORM_PATH."formWebObj.php";

class boEdit extends formWebObj
{
  function init()
    {
      $this->addSVar("mode", "add");  //add or edit
      $this->addSVar("buletin_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getBuletin");
          $close_date = $rs->Field("close_date");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      global $webPage;
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table
          $rs = WebApp::execQuery("SHOW FIELDS FROM buletinet");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
       
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getBuletin");
          WebApp::addVars($rs->Fields());
        }
      
      $title = (($mode=="add") ? "Shto buletin" : "Modifikim");
      WebApp::addVar("title", $title);
         
      //shton vend bosh tek makinisti dhe punetoret
      $rs = WebApp::openRS("listbox::makinisti");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
                 
      $rs = WebApp::openRS("lista_punetoret");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs); 
      
      //shton vend bosh tek makina list
      $rs = WebApp::openRS("listbox::makina");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      
    }

  function on_close($event_args)
    {
      $params["close_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("closeBuletin", $params);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->add_new_buletin($event_args);
        }
      else //mode=="edit"
        {
          WebApp::execDBCmd("updateBuletin", $event_args);
        }
    }

  function add_new_buletin($event_args)
    {
      //find a buletin_id for the new project
      $rs = WebApp::openRS("getLastBuletinId");
      $last_buletin_id = $rs->Field("last_buletin_id");
      $buletin_id = (($last_buletin_id==UNDEFINED) ? 1 : $last_buletin_id+1);

      //add the new buletin
      $event_args["creation_date"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newBuletin", $event_args);

      //after the document is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set 'bOffseti_rs->recount' to "true"
      //so that the records are counted again
      //and the new buletin is displayed at the end of the list
      WebApp::setSVar("bOffseti_rs->recount", "true");

      //set the new buletin_id
      $this->setSVar("buletin_id", $buletin_id);
    }

  /**
   * This is called when a new record is added or when records are deleted.
   */
  function update_oret()
    {
      //oret e punes
      $ore_pune = 0;
      $rs = WebApp::openRS("get_ore_shtypur");
      while (!$rs->EOF())
        {
          $ore_pune += $rs->Field("ore_shtypur");
          $rs->MoveNext();
        }
      WebApp::execDBCmd("set_ore_pune", compact("ore_pune"));

      //perqindja e realizimit te makines dhe punetoreve
      $rs = WebApp::openRS("get_ore_deklaruar");
      $ore_deklaruar = $rs->Field("ore_deklaruar");
      if ($ore_deklaruar==UNDEFINED)
        {
          $realizimi = 0.0;
        }
      else
        {
          $realizimi = ($ore_pune/$ore_deklaruar)*100;
          $realizimi = round($realizimi,1);
        }
      WebApp::execDBCmd("set_realizimi", compact("realizimi"));
    }
}
?>