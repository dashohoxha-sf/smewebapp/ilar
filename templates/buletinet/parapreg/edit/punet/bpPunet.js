// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al
*/

function rmpPune(pune_id)
{
  if (!editable())  return;
  GoTo("thisPage?event=bpPunet.rm(pune_id=" + pune_id + ")");
}

function rmpAllPunet()
{
  if (!editable())  return;
  GoTo("thisPage?event=bpPunet.rmAll");
}

