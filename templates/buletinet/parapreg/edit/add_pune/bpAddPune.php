<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage parapreg
*/

include_once FORM_PATH."formWebObj.php";
include_once TPL_PATH."buletinet/parapreg/edit/add_pune/shpenzimet.php";

class bpAddPune extends formWebObj
{
  function onRender()
    {
      global $webPage;
      
      //shton vend bosh tek autorizimi
      $rs = WebApp::openRS("listbox::kart_id");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      //shton vend bosh tek lloji i punes
            $rs = WebApp::openRS("listbox::lloj_pune");
            $rs->insRec(array("id"=>"", "label"=>" "));
            $webPage->addRecordset($rs);
      
      //shton vend bosh tek makinat
            $rs = WebApp::openRS("listbox::makina_p");
            $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      
      //shton vend bosh tek lloji i materialit
            $rs = WebApp::openRS("listbox::lloj_materialit");
            $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      
      //shton vend bosh tek formatet e letres
      $rs = WebApp::openRS("listbox::permasa1");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::permasa2");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::pesha");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      $rs = WebApp::openRS("listbox::lloji");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);      
    }
    
  function on_add($event_args)
    { 
      $event_args["sasia_shtypur"] = $this->get_sasia_shtypur($event_args);  
      $event_args["ore_shtypur"] = $this->get_ore_shtypur($event_args);
      $event_args["autorizim"] = $this->get_autorizim_nr($event_args);
      
      $params = $event_args;
      $params["formati"] = $this->get_format($event_args);
      //$params["ngjyrat"] = $this->get_ngjyrat($event_args);
      //$params["perqind_realizuar"] = $this->get_perqind_realizuar($event_args);
      $shpenzimet = new Shpenzimet;
      $params["shpenzimet"] = $shpenzimet->get_shpenzimet($event_args); 
      WebApp::execDBCmd("addPune", $params);
      bpEdit::update_oret();
    }


    function get_sasia_shtypur($event_args)
        {
          
          $lloj_pune = $event_args["lloj_pune"];
            
          if ($lloj_pune == "Printim film") 
          {
              
              $permasa1 = $event_args["permasa1"];
              $permasa2 = $event_args["permasa2"];
              $kart_id = $event_args["kart_id"];
              $rs = WebApp::execQuery("SELECT  nr_ngjyrave, formati_gatshem FROM kartat_teknologjike WHERE (kart_id='$kart_id' )");
              $ngjyrat = $rs->Field("nr_ngjyrave");
              $ndarjet_letres = $rs->Field("formati_gatshem");
                         
              WebApp::debug_msg("nr ngjyrave: '$ngjyrat'"); 
              WebApp::debug_msg("ndarjet: '$ndarjet_letres'"); 
              $sasia_shtypur = ( ($permasa1*$permasa2)/(5246 * $ndarjet_letres)) * 8 * $ngjyrat;

          }
          else
          {
               $sasia_shtypur = $event_args["sasia_shtypur"];
          }
          WebApp::debug_msg("sasia shtypur: '$sasia_shtypur'");        
          return $sasia_shtypur;
         
    }
    
     
  function get_ore_shtypur($event_args)
    {
      $ore_shtypur = $event_args["ore_shtypur"];  
      if ($ore_shtypur <> "") 
      {
          return $ore_shtypur;
      }
      else
      {

      //perndryshe llogarite ne varesi te llojit te punes        

      $lloj_pune = $event_args["lloj_pune"];
      $sasia_shtypur = $event_args["sasia_shtypur"];
      $lloj_materialit = $event_args["lloj_materialit"];
      $makina_p = $event_args["makina_p"];    
      $normativa = $this->get_normativ($lloj_pune, $lloj_materialit);
      if ($lloj_pune == "Rradhim" 
          OR $lloj_pune == "Punim foto" 
          OR $lloj_pune == "Kompozim faqe (faqosje)"
          OR $lloj_pune == "Skanim foto")
        {
          
          $ore_shtypur = (($sasia_shtypur *8)/ $normativa);
        }
      else
        {
            if ($lloj_pune == "Printim film")
            {
                $ore_shtypur = ($sasia_shtypur * $normativa)/8;
            }
            else
            {
          $ore_shtypur = ($sasia_shtypur * $normativa);
         } 
         } 
        WebApp::debug_msg("ore shtypur: '$ore_shtypur'");        
      return $ore_shtypur;
      }
    }

 function get_normativ($lloj_pune, $lloj_materialit)
    {
      //gjej normativen
     
      //$rs = WebApp::openRS("get_normativ", compact("makina_p", "lloj_pune"));
      if( $lloj_materialit<>"")
       {
        $rs = WebApp::execQuery("SELECT  normativat FROM normativat_parapreg WHERE (lloj_pune='$lloj_pune'  AND lloj_materialit='$lloj_materialit')");
       }
       else
       {
         $rs = WebApp::execQuery("SELECT  normativat FROM normativat_parapreg WHERE (lloj_pune='$lloj_pune')"); 
       }
      
     // print $rs->toHtmlTable();
      $normativa = $rs->Field("normativat");
      WebApp::debug_msg("normativ: '$normativa'");
      return $normativa;
    }
  
  function get_normativ1($lloj_pune)
      {
        //gjej normativen
        //$rs = WebApp::openRS("get_normativ", compact("makina_p", "lloj_pune"));
        $rs = WebApp::execQuery("SELECT  normativat FROM normativat_parapreg WHERE (lloj_pune='$lloj_pune')");
        //print $rs->toHtmlTable();
        $normativa = $rs->Field("normativat");
        WebApp::debug_msg("normativ: '$normativa'");
        return $normativa;
    }
  
  function get_autorizim_nr($event_args)
    {
      $kart_id = $event_args["kart_id"];
      $rs = WebApp::openRS("get_autorizim_nr", compact("kart_id"));
      $auto = $rs->Field("autorizim_nr");
      if ($auto==UNDEFINED)  $auto="";
      return $auto;
    }  

  function get_format($event_args)
    {
       $kart_id = $event_args["kart_id"];
       $rs = WebApp::openRS("get_kart_format", compact("kart_id"));
       $format = $rs->Field("formati_botimit");
       if ($format==UNDEFINED)  $format="";
   
       return $format;
    }
    
  

             
}
?>
