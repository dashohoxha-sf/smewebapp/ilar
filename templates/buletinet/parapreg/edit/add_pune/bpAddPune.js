// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function addPune()
{
  if (!editable())  return;

  var form = document.bpAddPune;
  
  if (!validate(form))  return;
  
  var event_args = getEventArgs(form);  
  GoTo("thisPage?event=bpAddPune.add(" + event_args + ")");
}

/**
 * Return true if all input fields are OK,
 * otherwise return false.
 */
function validate(form)
{
  if (form.sasia_shtypur.value=="")
    {
      alert ("Ju lutem mos e lini bosh sasine.");
      form.sasia_shtypur.focus();
      return false;
    }
 if (form.kart_id.value=="")
     {
       alert ("Ju lutem mos e lini bosh Titullin e Botimit.");
       form.kart_id.focus();
       return false;
    }
  
  if  ((form.lloj_pune.value =="Montim folje") 
       || (form.lloj_pune.value =="Kopje lastre") )
      // || (form.lloj_pune.value =="Shtyp llak") )
    { 
      
                    
      if (form.makina_p.value == "")
        {
          alert ("Duhet te plotesoni fushen 'Makina'.");
          form.makina_p.focus();
          return false;
        } 
        
      }
      
    if  ((form.lloj_pune.value =="Rradhim") 
       || (form.lloj_pune.value =="Kompozim faqe (faqosje)") )
      // || (form.lloj_pune.value =="Shtyp llak") )
    { 
      
                    
      if (form.lloj_materialit.value == "")
        {
          alert ("Duhet te plotesoni fushen 'Lloji i Materialit'.");
          form.makina_p.focus();
          return false;
        }     
      
    }

  return true;
}
