<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
* @subpackage parapreg
*/

class Shpenzimet
{
  function get_shpenzimet($event_args)
    {
      //get the data from the html form
      extract($event_args);

      //get the data of the buletin
      $rs = WebApp::openRS("getBuletin");
      $buletin = $rs->Fields();

      //gjej ne cilen makine
      //$makina = $buletin["makina"];
      $makinisti = $buletin["makinisti"];
      WebApp::debug_msg($makinisti, "punetor: ");
      //get the coeficients
      $rs = WebApp::execQuery("SELECT emri, sasia, vlera FROM koeficientet_parapreg");
      $koef_sasi = array();
      $koef_vlere = array();
      while (!$rs->EOF())
        {
          $emri = $rs->Field("emri");
          $sasia = $rs->Field("sasia");
          $vlera = $rs->Field("vlera");
          $koef_sasi[$emri] = $sasia;
          $koef_vlere[$emri] = $vlera;
          $rs->MoveNext();
        }
      
      WebApp::debug_msg($lloj_pune, "lloj pune: ");
      if ($lloj_pune == "Kopje lastre" )
        {
           $laster_sasi = $sasia_shtypur;
        }
      else
        {
           $laster_sasi = 0;
        }
      
      $laster_vlere = 
            $this->llogarit_laster_vlere($laster_sasi,$koef_vlere,$makina_p);
      
      if ($lloj_pune == "Printim kalk")
        {
          $kalk_sasi = $sasia_shtypur;
        }
      else
        {
          $kalk_sasi = 0;
        }
      WebApp::debug_msg($kalk_sasi, "kalk sasi: ");
      
    
    if ($lloj_pune == "Printim poliester" )
       {
         $poliester_sasi = $sasia_shtypur;
       }
    else
       {
         $poliester_sasi = 0;
       }
    
    if ($lloj_pune == "Printim leter" )
     {
        $leter_sasi = $sasia_shtypur;
     }
    else
      {
       $leter_sasi = 0;
      }
     WebApp::debug_msg($leter_sasi, "leter sasi: ");
    
     
    
    if ($lloj_pune == "Printim film" )
     {
         $film_sasi = $this->llogarit_film_sasi($permasa1,
                                                $permasa2,
                                                $kart_id); 
         $sasia_shtypur = $film_sasi;                                  
     }
    else
     {
         $film_sasi = 0;
     }
     WebApp::debug_msg($kart_id, "kartid: ");
     
         
          //llogarit vleren e pages
      $paga_vlere = 
            $this->llogarit_vleren_e_pages($lloj_pune,
                                           $ore_shtypur, 
                                           $makinisti);
           
          $arr_shpenzimet = 
            array(
                  "kalk_sasi" => $kalk_sasi,
                  "kalk_vlere" => ($kalk_sasi * $koef_vlere["kalk"]),
                  "leter_sasi" => $leter_sasi,
                  "leter_vlere" => ($leter_sasi * $koef_vlere["leter_parapreg"]),
                  "film_sasi" => $film_sasi,
                  "film_vlere" => ($film_sasi * $koef_vlere["film"]),
                  "poliester_sasi" => $poliester_sasi,
                  "poliester_vlere" => ($poliester_sasi * $koef_vlere["poliester"]),
                  "laster_sasi" => $laster_sasi,
                  "laster_vlere" => $laster_vlere,
                  "shpenzimet_shtese_vlere"=>($koef_sasi["shpenzime_shtese"] * $paga_vlere),
                  "paga_vlere" => $paga_vlere,
                  );

          $str_shpenzimet = serialize($arr_shpenzimet);
          WebApp::debug_msg("str_shpenzimet: '$str_shpenzimet'"); 
          return $str_shpenzimet;
        
    }

    
  function llogarit_film_sasi($permasa1,$permasa2,$kart_id)   
  {
           $film_sasi = 0.0;
           
           $rs = WebApp::execQuery("SELECT  nr_ngjyrave, formati_gatshem FROM kartat_teknologjike WHERE (kart_id='$kart_id' )");
           $ngjyrat = $rs->Field("nr_ngjyrave");
           $ndarjet_letres = $rs->Field("formati_gatshem");
           
           WebApp::debug_msg("nr ngjyrave: '$ngjyrat'"); 
           WebApp::debug_msg("ndarjet: '$ndarjet_letres'"); 
           
           $film_sasi = ( ($permasa1*$permasa2)/(5246 * $ndarjet_letres)) * 8 * $ngjyrat;
           WebApp::debug_msg("film_sasi: '$film_sasi'");
           return $film_sasi;
   }
    

  function llogarit_laster_vlere($laster_sasi,$koef_vlere,$makina_p )
    {
         $laster_vlere = 0.0;
         $emri='laster';    
         $rs = WebApp::execQuery("SELECT  vlera FROM koeficientet_parapreg WHERE (emri='$emri' AND lloj_makina='$makina_p')");
         $koef_vlere = $rs->Field("vlera");
         WebApp::debug_msg("vlera laster sipas mak: '$koef_vlere'"); 
         
         $laster_vlere = $laster_sasi * $koef_vlere;
         WebApp::debug_msg("laster_vlere: '$laster_vlere'");
         return $laster_vlere;
    }
   
 
     
   
   function llogarit_vleren_e_pages($lloj_pune, $ore_shtypur, $makinisti)                                 
      {
        //get the salaries of the workers
        $rs = WebApp::execQuery("SELECT username, paga FROM users");
        //print $rs->toHtmlTable();
        $pagat = array();
        while (!$rs->EOF())
          {
            $username = $rs->Field("username");
            $paga = $rs->Field("paga");
            $pagat[$username] = $paga;
            $rs->MoveNext();
          }
  
        $paga_vlere = 0.0;
                  
            if ($ore_shtypur<>"")
              {
                if ($makinisti<>NULL_VALUE)
                  {
                    $paga_vlere = $ore_shtypur * $pagat[$makinisti];
                    WebApp::debug_msg("paga_makinist: '$pagat[$makinisti]'");
                  }
                
              }
          
         WebApp::debug_msg("makinist: '$makinisti'");
         WebApp::debug_msg("ore pune: '$ore_shtypur'");
         WebApp::debug_msg("paga_vlere: '$paga_vlere'");
        return $paga_vlere;
    }
}
?>