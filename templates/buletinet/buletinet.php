<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package buletinet
*/

class buletinet extends WebObject
{
  function init()
    {
      $this->addSVar("reparti", "offseti");
      $this->addSVar("module", "offseti/listo/listo.html");
    }

  function onParse()
    {
      global $event;
      if ($event->target=="main" and $event->name=="select")
        {
          $this->select_buletin($event->args);
        }
    }

  function select_buletin($event_args)
    {
      $interface = WebApp::getSVar("interface");
      switch ($interface)
        {
          //buletinet ditore (offset)
        case "buletinet/offseti/listo":
          $interface_title = "Reparti Offset-->Buletinet Ditore-->Lista";
          $module = "offseti/listo/listo.html";
          $this->setSVar("reparti", "offseti");
          break;
        case "buletinet/offseti/edit":
          $interface_title = "Reparti Offset-->Buletinet Ditore-->Ndrysho";
          $module = "offseti/edit/boEdit.html";
          //set the state of the boEdit module
          WebApp::setSVar("boEdit->mode", "edit");
          WebApp::setSVar("boEdit->buletin_id", $event_args["buletin_id"]);
          break;
        case "buletinet/offseti/krijo":
          $interface_title = "Reparti Offset-->Buletinet Ditore-->Krijo";
          $module = "offseti/edit/boEdit.html";
          //set the state of the boEdit module
          WebApp::setSVar("boEdit->mode", "add");
          WebApp::setSVar("boEdit->buletin_id", UNDEFINED);
          break;

          //buletinet ditore (rrotative)
        case "buletinet/rrotative/listo":
          $interface_title = "Reparti Rrotativ-->Buletinet Ditore-->Lista";
          $module = "offseti/listo/listo.html";
          $this->setSVar("reparti", "rrotative");
          break;
        case "buletinet/rrotative/edit":
          $interface_title = "Reparti Rrotativ-->Buletinet Ditore-->Ndrysho";
          $module = "offseti/edit/boEdit.html";
          //set the state of the boEdit module
          WebApp::setSVar("boEdit->mode", "edit");
          WebApp::setSVar("boEdit->buletin_id", $event_args["buletin_id"]);
          break;
        case "buletinet/rrotative/krijo":
          $interface_title = "Reparti Rrotativ-->Buletinet Ditore-->Krijo";
          $module = "offseti/edit/boEdit.html";
          //set the state of the boEdit module
          WebApp::setSVar("boEdit->mode", "add");
          WebApp::setSVar("boEdit->buletin_id", UNDEFINED);
          break;

          //buletinet ditore (parapreg)
        case "buletinet/parapreg/listo":
          $interface_title = "Parapregatitja-->Buletinet Ditore-->Lista";
          $module = "parapreg/listo/listo.html";
          $this->setSVar("reparti", "parapreg");
          break;
        case "buletinet/parapreg/edit":
          $interface_title = "Parapregatitja-->Buletinet Ditore-->Ndrysho";
          $module = "parapreg/edit/bpEdit.html";
          //set the state of the bpEdit module
          WebApp::setSVar("bpEdit->mode", "edit");
          WebApp::setSVar("bpEdit->buletin_id", $event_args["buletin_id"]);
          break;
        case "buletinet/parapreg/krijo":
          $interface_title = "Parapregatitja-->Buletinet Ditore-->Krijo";
          $module = "parapreg/edit/bpEdit.html";
          //set the state of the bpEdit module
          WebApp::setSVar("bpEdit->mode", "add");
          WebApp::setSVar("bpEdit->buletin_id", UNDEFINED);
          break;

          //buletinet ditore (lidhja)
        case "buletinet/lidhja/listo":
          $interface_title = "Liberlidhja-->Buletinet Ditore-->Lista";
          $module = "lidhja/listo/listo.html";
          $this->setSVar("reparti", "lidhja");
          break;
        case "buletinet/lidhja/edit":
          $interface_title = "Liberlidhja-->Buletinet Ditore-->Ndrysho";
          $module = "lidhja/edit/blEdit.html";
          //set the state of the blEdit module
          WebApp::setSVar("blEdit->mode", "edit");
          WebApp::setSVar("blEdit->buletin_id", $event_args["buletin_id"]);
          break;
        case "buletinet/lidhja/krijo":
          $interface_title = "Liberlidhja-->Buletinet Ditore-->Krijo";
          $module = "lidhja/edit/blEdit.html";
          //set the state of the blEdit module
          WebApp::setSVar("blEdit->mode", "add");
          WebApp::setSVar("blEdit->buletin_id", UNDEFINED);
          break;
        }

      WebApp::setSVar("interface_title", $interface_title);
      $this->setSVar("module", $module);
    }
}
?>