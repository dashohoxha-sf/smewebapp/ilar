<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/

/**
* @package menu
* @subpackage edit
*/

include_once dirname(__FILE__)."/class.MenuItem.php";

/**
 * Class MenuRoot represents all the menus of a page.
 *
 * @package     boxes
 * @subpackage  editMenu
 */
class MenuRoot extends MenuItem
{
  function MenuRoot($id ="MenuRoot")
    {
      MenuItem::MenuItem($id, 'root item', 'null');
    }
        
  /** writes the menu as an XML file */
  function write_xml($fname)
    {
      $fp = fopen($fname, "w");
      fwrite($fp, $this->to_xml());
      fclose($fp);
    }

  /** writes the menu as a JS file */
  function write_js($fname)
    {
      $fp = fopen($fname, "w");
      fwrite($fp, $this->to_js());
      fclose($fp);
    }

  /**
   * Returns a new menu that contains only the items
   * whose id is in the given list.  Of course, any
   * parents of them are also included, even if their
   * id is not in the list.
   */
  function filter($id_list)
    {
      $this->arr_filter = explode(',', $id_list);
      $new_menu = new MenuRoot($this->id);

      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $item = &$this->children[$i];
          $new_item = $this->filter_items($item);
          if ($new_item<>UNDEFINED)  $new_menu->add_child($new_item);
        }

      return $new_menu;
    }

  /**
   * Filters the given menu item (and its children) according to the list.
   * Returns the accepted items or UNDEFINED if nothing is accepted.
   */
  function filter_items(&$item)
    {
      $new_item = new MenuItem($item->id,$item->caption,$item->link);

      //filter the children
      for ($i=0; $i < $item->nr_children(); $i++)
        {
          $subitem = &$item->children[$i];
          $new_subitem = $this->filter_items($subitem);
          if ($new_subitem<>UNDEFINED)  $new_item->add_child($new_subitem);
        }
      
      //filter this item;
      //it is accepted if it is in the list,
      //or some of its children are accepted
      if ( in_array($item->id, $this->arr_filter) 
           or ($new_item->nr_children() > 0) )
        return $new_item;
      else
          return UNDEFINED;
    }

  /**
   * Returns a new menu by removing the items that have 
   * only a single child.  However the first level is
   * kept even if it has only one item.
   */
  function compact()
    {
      $new_menu = new MenuRoot($this->id);
      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $item = &$this->children[$i];
          $new_item = $this->compact_item($item);
          $new_menu->add_child($new_item);
        }

      return $new_menu;
    }

  function compact_item(&$item)
    {
      $new_item = new MenuItem($item->id,$item->caption,$item->link);

      //compact the children
      for ($i=0; $i < $item->nr_children(); $i++)
        {
          $subitem = &$item->children[$i];
          $new_subitem = $this->compact_item($subitem);
          $new_item->add_child($new_subitem);
        }
 
      if ($new_item->nr_children()==1)
        {
          $child = $new_item->children[0];
          if ($child->nr_children() > 0)
            {
              $new_item->children = $child->children;
            }
          else
            {
              $child->caption = $new_item->caption; 
              $new_item = $child;
            }
        }
      
      return $new_item;
    }
        
  /** returns the menus as an XML string */
  function to_xml()
    {
      $xml_menu = "<?xml version='1.0'?>
<!DOCTYPE menu SYSTEM 'menu.dtd'>

<menu id='$this->id'>
";
      $xml_menu .= $this->children_to_xml("  ");
      $xml_menu .= "</menu>";
      return $xml_menu;
    }

  /**
   * Returns the JavaScript array with the menu items.
   */
  function to_js()
    {
      $js_arr = "// -*-C-*- \n\n"; 
      $js_arr .= "var MENU_ITEMS =\n[\n"; 

      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $child = $this->children[$i];
          $indent = '  ';
          $js_arr .= $child->to_js_arr($indent);
        }
      $js_arr .= " ];\n";
      return $js_arr;
    }
}
?>
