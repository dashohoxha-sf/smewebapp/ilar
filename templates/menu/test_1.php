<?php
include_once "class.MenuRoot.php";
include_once "class.XMLMenu.php";

$xmlMenu = new XMLMenu("menu.xml");
$menu = $xmlMenu->getMenu();
//print "<xmp>";
//print $menu->to_text("@@");
//print $menu->to_xml();
//print $menu->to_js();
print menu_to_html($menu);
//print "</xmp>";

//$menu->write_xml("menu_1.xml");
//$menu->write_js("menu_items_1.js");

//--------------------------------------

function menu_to_html($menu)
{
  $html = "
<style>
.menutitle
{
  color: #000044;
  border: 1px outset #ffffff;
  background-color: #dddddd;
  padding: 0;
  margin-top: 7px;
  margin-bottom: 3px;
}
.menuitem
{
  color: #000044;
  border: 1px outset #ffffff;
  background-color: #eeeeee;
  padding: 0;
}
</style>
<table cellspacing='5' cellpadding='0' border='0'>
  <tr>
";

  for ($i=0; $i < $menu->nr_children(); $i++)
    {
      if ($i % 3 == 0)
        {
          $html .= "  </tr>\n";
          $html .= "  <tr>\n";
        }

      $html .= "    <td valign='top' bgcolor='#f8f8f8'>\n";
      $html .= "      <dl>\n";
      $item = $menu->children[$i];
      $html .= menuitem_to_html($item, '        ');
      $html .= "      </dl>\n";
      $html .= "    </td>\n";
    }

  $html .= "
  </tr>
</table>
";

  return $html;
}

function menuitem_to_html($item, $indent)
{
  if ($item->link=='null')
    {
      $html .= $indent."<dt class='menutitle'>\n";
      $html .= $indent."  ".$item->caption."\n";
      $html .= $indent."</dt>\n";
    }
  else
    {
      $id = $item->id;
      $html .= $indent."<dt class='menuitem'>\n";
      $html .= $indent."  <input type='checkbox' name='$id' value='$id'>\n";
      $html .= $indent."  ".$item->caption."\n";
      $html .= $indent."</dt>\n";
    }

  if ($item->has_children())
    {
      $html .= $indent."<dd>\n";
      $html .= $indent."  <dl>\n";
      for ($i=0; $i < $item->nr_children(); $i++)
        {
          $subitem = $item->children[$i];
          $html .= menuitem_to_html($subitem, '    '.$indent);
        }
      $html .= $indent."  </dl>\n";
      $html .= $indent."</dd>\n";
    }

  return $html;
}
?>