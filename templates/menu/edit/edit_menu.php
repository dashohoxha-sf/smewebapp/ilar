<?php
class edit_menu extends WebObject
{
  function init()
    {
      //the menu item that is being edited, initially the root
      $this->addSVar('item_id', 'menu');
    }

  /** select an item for editing */
  function on_select($event_args)
    {
      $item_id = $event_args['item_id'];
      $this->setSVar('item_id', $item_id);
    }

  function transform_menu($transformer, $arr_params =array())
    {
      //apply the transformer and get the new menu
      $new_menu = $this->transform($transformer, $arr_params);

      //save the transformed menu to a tmp file
      $tmpfname = tempnam("/tmp", "menu");
      $fp = fopen($tmpfname, 'w');
      fputs($fp, $new_menu);
      fclose($fp);

      //move to menu.xml and set permissions
      $menu_xml = MENU_PATH.'edit/menu.xml';
      shell_exec("./wrapper mv $tmpfname $menu_xml");
      shell_exec("./wrapper chown dasho.dasho $menu_xml");
      shell_exec("./wrapper chmod 664 $menu_xml");
    }

  /**
   * Applies the given xsl transformer to menu.xml and returns 
   * the result. $arr_params is an associative array of parameters. 
   */
  function transform($transformer, $arr_params =array())
    {
      //construct the string $params
      $params = '';
      while (list($p_name, $p_value) = each($arr_params))
        {
          $params .= "--stringparam $p_name \"$p_value\" ";
        }

      //apply the $transformer with $params to menu.xml
      $menu_xml = MENU_PATH.'edit/menu.xml';
      $xsl_file = MENU_PATH."edit/xsl/$transformer";
      $result = shell_exec("xsltproc $params $xsl_file $menu_xml 2>&1");
      //print "<xmp>$result</xmp>\n";

      return $result;
    }

  /** save modifications in id and caption of the current item */
  function on_update($event_args)
    {
      $params = $event_args;
      $params['id'] = $this->getSVar('item_id');
      $this->transform_menu('update.xsl', $params);
    }

  /** delete the current item and set the parent item as the current one */
  function on_delete($event_args)
    {
      $params['id'] = $this->getSVar('item_id');
      $parent_id = $this->transform('get_parent_id.xsl', $params);
      $this->transform_menu('delete.xsl', $params);
      $this->setSVar('item_id', $parent_id);
    }

  /** move the current item up */
  function on_move_up($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_up.xsl', $params);
    }

  /** move the current item down */
  function on_move_down($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_down.xsl', $params);
    }

  /** add a new subitem to the current item */
  function on_add_subitem($event_args)
    {
      $params = $event_args;
      $params['id'] = $this->getSVar('item_id');
      $this->transform_menu('add_subitem.xsl', $params);
    }

  /** apply the modifications to the main menu */
  function on_apply($event_args)
    {
      $menu_xml = MENU_PATH.'menu.xml';
      $edit_menu_xml = MENU_PATH.'edit/menu.xml';
      shell_exec("./wrapper cp $edit_menu_xml $menu_xml");
    }

  /** discard any modifications and get a copy of the main menu */
  function on_cancel($event_args)
    {
      $menu_xml = MENU_PATH.'menu.xml';
      $edit_menu_xml = MENU_PATH.'edit/menu.xml';
      shell_exec("./wrapper cp $menu_xml $edit_menu_xml");
    }

  function onRender()
    {
      //this variable makes visible the edit menu in menu.html
      WebApp::addGlobalVar('edit_menu_visible', 'true');

      $item_id = $this->getSVar('item_id');

      //get the selected item and subitems
      $xsl_file = MENU_PATH.'edit/xsl/subitems.xsl';
      $xml_file = MENU_PATH.'edit/menu.xml';
      $cmd = "xsltproc --stringparam id $item_id $xsl_file $xml_file 2>&1";
      $items = shell_exec($cmd);
      $arr_lines = explode("\n", $items);

      //get the caption of the selected item (from the first line)
      list($id, $item_caption) = split(' ', $arr_lines[0], 2);
      WebApp::addVar('item_caption', $item_caption);

      //create a recordset with id-s and captions of the subitems
      $rs = new EditableRS('subitems');
      for ($i=1; $i < sizeof($arr_lines); $i++)
        {
          list($id, $caption) = split(' ', $arr_lines[$i], 2);
          if ($id=='')  continue;
          $rs->addRec(compact('id','caption'));
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>