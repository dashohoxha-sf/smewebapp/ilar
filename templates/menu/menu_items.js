// -*-C-*- //tell emacs to use the C mode

var MENU_ITEMS = 
[ 
 ['Botimet', null, null,
  ['Listo Kartat', 'javascript:select(\'botimet/kartat/listo\')', null],
  ['Krijo Karte', 'javascript:select(\'botimet/kartat/krijo\')', null],
  ['Listo Kontratat', 'javascript:select(\'botimet/kontratat/listo\')', null],
  ['Krijo Kontrate', 'javascript:select(\'botimet/kontratat/krijo\')', null],
  ],
 ['Buletinet', null, null,
  ['Ofseti', null, null,
   ['Listo', 'javascript:select(\'buletinet/offseti/listo\')', null],
   ['Krijo', 'javascript:select(\'buletinet/offseti/krijo\')', null],
   ],
  ['Parapregatitja', null, null,
   ['Listo', 'javascript:select(\'buletinet/parapreg/listo\')', null],
   ['Krijo', 'javascript:select(\'buletinet/parapreg/krijo\')', null],
   ],
  ['Liberlidhja', null, null,
   ['Listo', 'javascript:select(\'buletinet/lidhja/listo\')', null],
   ['Krijo', 'javascript:select(\'buletinet/lidhja/krijo\')', null],
   ],
  ],
 ['Magazina', null, null,
  ['Leter', null, null,
   ['Listo Faturat', 'javascript:select(\'magazina/leter/faturat\')', null],
   ['Bej Hyrje', 'javascript:select(\'magazina/leter/hyrje\')', null],
   ['Bej Dalje', 'javascript:select(\'magazina/leter/dalje\')', null],
   ['Gjendja', 'javascript:select(\'magazina/leter/gjendja\')', null],
   ],
  ['E pergjithshme', null, null,
   ['Listo Faturat', 'javascript:select(\'magazina/tjeter/faturat\')', null],
   ['Bej Hyrje', 'javascript:select(\'magazina/tjeter/hyrje\')', null],
   ['Bej Dalje', 'javascript:select(\'magazina/tjeter/dalje\')', null],
   ['Gjendja', 'javascript:select(\'magazina/tjeter/gjendja\')', null],
   ],
  ],
 ['Raportet', null, null,
  ['Shpenzimet', null, null,
   ['Permbledhese', 'javascript:select(\'rpt_shpenzimet\')', null],
   ['Sasi', null, null,
    ['Parapregatitja', 'javascript:select(\'rpt_shpenzimet/sasi/parapreg\')', null],
    ['Offseti', 'javascript:select(\'rpt_shpenzimet/sasi/offseti\')', null],
    ['Liberlidhja', 'javascript:select(\'rpt_shpenzimet/sasi/lidhja\')', null],
    ['Te Gjitha', 'javascript:select(\'rpt_shpenzimet/sasi\')', null],
    ],
   ['Vlere', null, null,
    ['Parapregatitja', 'javascript:select(\'rpt_shpenzimet/vlere/parapreg\')', null],
    ['Offseti', 'javascript:select(\'rpt_shpenzimet/vlere/offseti\')', null],
    ['Liberlidhja', 'javascript:select(\'rpt_shpenzimet/vlere/lidhja\')', null],
    ['Te Gjitha', 'javascript:select(\'rpt_shpenzimet/vlere\')', null],
    ],
   ['Sasi-Vlere', null, null,
    ['Parapregatitja', 'javascript:select(\'rpt_shpenzimet/sasi_vlere/parapreg\')', null],
    ['Offseti', 'javascript:select(\'rpt_shpenzimet/sasi_vlere/offseti\')', null],
    ['Liberlidhja', 'javascript:select(\'rpt_shpenzimet/sasi_vlere/lidhja\')', null],
    ['Te Gjitha', 'javascript:select(\'rpt_shpenzimet/sasi_vlere\')', null],
    ],
   ],
  ['Puna e Kryer', null, null,
   ['Parapregatitja', 'javascript:select(\'rpt_punet/parapreg\')', null],
   ['Offseti', 'javascript:select(\'rpt_punet/offseti\')', null],
   ['Liberlidhja', 'javascript:select(\'rpt_punet/lidhja\')', null],
   ['Permbledhese', 'javascript:select(\'rpt_punet/permbledhese\')', null],
   ],
  ],
 ['Koeficientet', null, null,
  ['Teknologjike', null, null,
   ['Parapregatitja', 'javascript:select(\'koeficientet/parapreg\')', null],
   ['Offseti', 'javascript:select(\'koeficientet/offseti\')', null],
   ['Liberlidhja', 'javascript:select(\'koeficientet/lidhja\')', null],
   ],
  ['Financiare', 'javascript:select(\'koeficientet/financiare\')', null],
  ],
 ['Administrim', null, null,
  ['Perdoruesit', 'javascript:select(\'perdoruesit\')', null],
  ['Punetoret', 'javascript:select(\'punetoret\')', null],
  ['Punetor Paraperg.', 'javascript:select(\'punetoret/parapreg\')', null],
  ['Punetor Offseti', 'javascript:select(\'punetoret/offseti\')', null],
  ['Punetor Liberlidhje', 'javascript:select(\'punetoret/lidhja\')', null],
  ['Backup/Restore', 'javascript:select(\'backup_restore\')', null],
  ['Tabelat', 'javascript:select(\'tabelat\')', null],
  ],
 ];
