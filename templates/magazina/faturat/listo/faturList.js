// -*-C-*- //tell emacs to use C mode
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function edit_fature(fature_id, lloj_fature)
{
  var magazin_id = session.getVar('magazina->id');
  var intrf = 'magazina/'+magazin_id+'/faturat/edit';
  select(intrf, 'fature_id='+fature_id+';lloj_fature='+lloj_fature);
}

function print_fature(fature_id, lloj_fature)
{
  var magazin_id = session.getVar('magazina->id');
  var args = 'magazin_id=' + magazin_id + ';fature_id=' + fature_id
    + ';lloj_fature=' + lloj_fature;
  print_interface('magazina/faturat/printo', args);
}

function rm_fature(fature_id)
{
  SendEvent('faturList', 'rm', 'fature_id='+fature_id);
}

function bej_hyrje()
{
  var magazin_id = session.getVar('magazina->id');
  var intrf = 'magazina/'+magazin_id+'/faturat/hyrje';
  select(intrf);
}

function bej_dalje()
{
  var magazin_id = session.getVar('magazina->id');
  var intrf = 'magazina/'+magazin_id+'/faturat/dalje';
  select(intrf);
}
