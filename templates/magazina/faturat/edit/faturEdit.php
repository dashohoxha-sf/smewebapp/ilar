<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWebApp is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package magazina
 * @subpackage faturat
 */

include_once FORM_PATH."formWebObj.php";

class faturEdit extends formWebObj
{
  function init()
    {
      $this->addSVar("hyrje_dalje", "hyrje");  // hyrje | dalje
      $this->addSVar("mode", "add");           // add | edit
      $this->addSVar("fature_id", UNDEFINED);
      $this->addSVar("closed", "false");
    }

  function on_close()
    {
      $params["date_mbyllje"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("closeFature", $params);
    }

  function on_save($event_args)
    {
	  $hyrje_dalje = $this->getSVar('hyrje_dalje');
	  if ($hyrje_dalje=='hyrje') $event_args['reparti'] = '';
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->add_new_fature($event_args);
        }
      else //mode=="edit"
        {
          WebApp::execDBCmd("updateFature", $event_args);
        }
    }

  function add_new_fature($event_args)
    {
      //find a fature_id for the new project
      $rs = WebApp::openRS("getLastFatureId");
      $last_fature_id = $rs->Field("last_fature_id");
      $fature_id = (($last_fature_id==UNDEFINED) ? 1 : $last_fature_id+1);

      //add the new fature
      $event_args["date_regjistrimi"] = get_curr_date("Y-m-d");
      WebApp::execDBCmd("newFature", $event_args);

      //after the document is added, switch the editing
      //mode from 'add' to 'edit'
      $this->setSVar("mode", "edit");

      //set the new fature_id
      $this->setSVar("fature_id", $fature_id);
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          $this->setSVar("closed", "false");
        }
      else
        {
          $rs = WebApp::openRS("getFature");
          $close_date = $rs->Field("date_mbyllje");
          $closed = ($close_date==NULL_VALUE ? "false" : "true");
          $closed = 'false';
          $this->setSVar("closed", $closed);
        }
    }

  function onRender()
    {
      $mode = $this->getSVar("mode");
      if ($mode=="add")
        {
          //add variables with empty values for each field in the table
          $rs = WebApp::execQuery("SHOW FIELDS FROM magazina_faturat");
          while (!$rs->EOF())
            {
              $fieldName = $rs->Field("Field");
              WebApp::addVar($fieldName, "");
              $rs->MoveNext();
       
            }
          //add some default values
          WebApp::addVars(array(/* . . . */));
        }
      else if ($mode=="edit")
        {
          $rs = WebApp::openRS("getFature");
          WebApp::addVars($rs->Fields());
        }
      
      WebApp::addVar("title", $this->get_title());
    }

  /** returns the title of the fature that is being edited */
  function get_title()
    {
      $hyrje_dalje = $this->getSVar('hyrje_dalje');
      $hyrje_dalje = ucfirst($hyrje_dalje);
      $magazina = WebApp::getSVar('magazina->id');
      $magazina = ucfirst($magazina);

      $mode = $this->getSVar('mode');
      if ($mode=="add")
        {
          $title = "Magazina $magazina / Fature $hyrje_dalje e Re"; 
        }
      else
        {
          $rs = WebApp::openRS("getFature");
          $date_fature = $rs->Field('date_fature');
          $nr_fature = $rs->Field('nr_fature');
          $title = "Magazina $magazina / Fature $hyrje_dalje"
            . " / Date $date_fature / Nr $nr_fature ";
        }

      return $title;
    }
}
?>
