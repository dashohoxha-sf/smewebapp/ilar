// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of  small and medium enterprises.  
Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al
*/

function rmLevizje(levizje_id)
{
  if (!editable())  return;
  SendEvent('faturLevizjet', 'rm', 'levizje_id='+levizje_id); 
}

function rmAllLevizjet()
{
  if (!editable())  return;
  SendEvent('faturLevizjet', 'rmAll'); 
}

