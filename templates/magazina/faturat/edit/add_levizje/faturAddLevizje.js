// -*-C-*- //tell emacs to use C mode
/* 
This file is  part of SMEWeb.  SMEWeb is a  web application that helps
the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWeb is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

SMEWeb is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A  PARTICULAR PURPOSE. See the GNU  General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWeb; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function addLevizje()
{
	var form = document.faturAddLevizje
  if (!validate(form))
  {
	alert ("erdhi");
  	return;
  }
  
  var event_args = getEventArgs(form);
  //alert(event_args);  //debug
  SendEvent('faturAddLevizje', 'add', event_args);
}

/**
 * Return true if all input fields are OK,
 * otherwise return false.
 */
function validate(form)
{
  if (form.sasia.value=="")
    {
      alert ("Ju lutem mos e lini bosh sasine.");
      form.sasia.focus();
      return false;
    }
  /*if (form.cmimi.value=="")
    {
      alert ("Ju lutem mos e lini bosh Cmimin.");
      form.cmimi.focus();
      return false;
    }*/
  return true;
}
    
function set_value(txtbox1, txtbox2, txtbox3)
{
  //session.setVar("faturAddLevizje->vlera", txtbox1.value * txtbox2.value);
  txtbox3.value=txtbox1.value*txtbox2.value
}
