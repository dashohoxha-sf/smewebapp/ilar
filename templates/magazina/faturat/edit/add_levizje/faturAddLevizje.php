<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package magazina
* @subpackage faturat
*/

include_once FORM_PATH."formWebObj.php";
include_once dirname(__FILE__).'/shpenzimet.php';

class faturAddLevizje extends formWebObj
{
  function onRender()
    {
      global $webPage;
      
      //shton vend bosh tek autorizimi
      $rs = WebApp::openRS("listbox::autorizim");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
      //shton vend bosh tek kart_id
      $rs = WebApp::openRS("listbox::kart_id");
      $rs->insRec(array("id"=>"", "label"=>" "));
      $webPage->addRecordset($rs);
    }
    
  function on_add($event_args)
    { 
      WebApp::execDBCmd("addLevizje", $event_args);
	  $this->update_gjendja($event_args);
    }
    
  function get_autorizim_nr($event_args)
    {
      $kart_id = $event_args["kart_id"];
      $rs = WebApp::openRS("get_autorizim_nr", compact("kart_id"));
      $auto = $rs->Field("autorizim_nr");
      if ($auto==UNDEFINED)  $auto="";
      return $auto;
    }  

  function update_gjendja($event_args)
    {
	$hyrje_dalje = WebApp::getSVar("faturEdit->hyrje_dalje");
	$magazina = WebApp::getSVar("magazina->id");
	if ($magazina=='leter')
	{
	  $rs=WebApp::openRS("gjendja_leter", $event_args);
	  $elem = $rs->Field("gjendja_id");
	  if ($hyrje_dalje=='hyrje')
	  {
      if ($elem==UNDEFINED)
		WebApp::execDBCmd("insGjendja_leter_h", $event_args);
	  else
      	WebApp::execDBCmd("updGjendja_leter_h", $event_args);
	  }
	  else
	  {
		if ($elem==UNDEFINED)
			WebApp::execDBCmd("insGjendja_leter_d", $event_args);
		else
			WebApp::execDBCmd("updGjendja_leter_d", $event_args);
	  }
	}
	else
	{
	  $rs=WebApp::openRS("gjendja_boje", $event_args);
	  $elem = $rs->Field("gjendja_id");
      if ($hyrje_dalje=='hyrje')
	  {
      if ($elem==UNDEFINED)
		WebApp::execDBCmd("insGjendja_boje_h", $event_args);
	  else
      	WebApp::execDBCmd("updGjendja_boje_h", $event_args);
	  }
	  else
    {
		if ($elem==UNDEFINED)
			WebApp::execDBCmd("insGjendja_boje_d", $event_args);
		else
			WebApp::execDBCmd("updGjendja_boje_d", $event_args);
	  }
	}
    }
}
?>
