<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package magazina
* @subpackage levizjet
*/

class levizjeList extends WebObject
{
  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("levizjet_rs->current_page", $page);
    }

  function onRender()
    {
      WebApp::setSVar("levizjet_rs->recount", "true");

      $filter_condition = $this->get_filter_condition();
      WebApp::addVar("filter_condition", $filter_condition);
    }

  function get_filter_condition()
    {
      //return '1=1';

      $data_filter = WebApp::getSVar("data->filter");
      $data_filter = str_replace("date_field", "date_fature", $data_filter);
	  //$data_filter = str_replace("date_field", "data", $data_filter);
      $levizjet_filter = WebApp::getSVar("fFaturat->filter");
      $malli_filter = WebApp::getSVar("fMallrat->filter");

      $conditions[] = $data_filter;
      if ($levizjet_filter<>"")   $conditions[] = $levizjet_filter;
	  if ($malli_filter<>"")   $conditions[] = $malli_filter;

      $filter_condition = implode(" AND ", $conditions);
      $filter_condition = "($filter_condition)";

      return $filter_condition;
    }
}
?>