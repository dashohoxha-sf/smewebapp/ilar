<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package magazina
* @subpackage levizjet
*/

class selection extends WebObject
{
  function onRender()
    {
      $day1 = WebApp::getSVar("data->day1");
      $month1 = WebApp::getSVar("data->month1");
      $year1 = WebApp::getSVar("data->year1");
      $day2 = WebApp::getSVar("data->day2");
      $month2 = WebApp::getSVar("data->month2");
      $year2 = WebApp::getSVar("data->year2");
      $periudha = "$day1/$month1/$year1 -- $day2/$month2/$year2";
      WebApp::addVar("periudha", $periudha);


      //nderto selektimin e bere ne baze te te dhenat e tjera

      $emertimi = WebApp::getSVar("fMallrat->malli");
      $lloj_fature = WebApp::getSVar("fFaturat->lloj_fature");
      $nr_fature = WebApp::getSVar("fFaturat->nr_fature");
      $magazinieri = WebApp::getSVar("fFaturat->magazinieri");
      $sasia = WebApp::getSVar("fMallrat->sasia");
      $njesia = WebApp::getSVar("fMallrat->njesia");

      $arr_fushat = array();

	  if ($emertimi<>"")
        $arr_fushat[] = "Artikulli '$emertimi'";
		
      if ($sasia<>"")
        $arr_fushat[] = "Sasia = '$sasia'";

      if ($njesia<>"")
        $arr_fushat[] = "Njesia = '$njesia'";

	  if ($lloj_fature<>"")
        $arr_fushat[] = "Hyrje/Dalje = '$lloj_fature'";

      if ($nr_fature<>"")
        $arr_fushat[] = "Numri Fatures = '$nr_fature'";

      if ($magazinieri<>"")
        $arr_fushat[] = "Magazinieri '$magazinieri'";


      $fushat = implode("; ", $arr_fushat);
      WebApp::addVar("fushat", $fushat);
    }
}
?>
