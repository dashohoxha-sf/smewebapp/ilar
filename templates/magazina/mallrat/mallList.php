<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package magazina
 * @subpackage mallrat
 */

 include_once FORM_PATH."formWebObj.php";

class mallList extends WebObject
{
  function init()
    {
      //lejon mallrat te mund te editohen ose jo
      WebApp::addSVar('edit', 'true');  // true | false
	  $magazina = WebApp::getSVar('magazina->id');
      $magazina = ucfirst($magazina);
	  /*$this->addSVars( array(
	  						 "malli"   => "",
							 "malli1"   => "",
                             "njesia"     => "",
                             ));
      //$this->addSVar("filter", "");
	  //WebApp::message('init');*/
    }

  function on_next($event_args)
    {
      $page = $event_args['page'];
      WebApp::setSVar('mallrat_rs->current_page', $page);
    }
    
  function on_rm($event_args)
    {
      WebApp::execDBCmd('rmMall', $event_args);
    } 
    
  function on_add($event_args)
    {
      WebApp::setSVar('mallEdit->mode', 'add');
      WebApp::setSVar('mallEdit->gjendja_id', UNDEFINED);
    } 

  function on_edit($event_args)
    {
      WebApp::setSVar('mallEdit->mode', 'edit');
      WebApp::setSVar('mallEdit->gjendja_id', $event_args['gjendja_id']);
    } 

  function onRender()
    {
      WebApp::setSVar('mallrat_rs->recount', 'true');

      $filter_condition = $this->get_filter_condition();
      WebApp::addVar('filter_condition', $filter_condition);
    }

  function get_filter_condition()
    {
      /*//return '1=1';

      //$data_filter = WebApp::getSVar('data->filter');
      //$data_filter = str_replace('date_field', 'data', $data_filter);
      $mallrat_filter = WebApp::getSVar('fMallrat->filter');
	  $boje_leter_filter=WebApp::getSVar("magazina->id");

      $conditions[] = $mallrat_filter;
      //if ($mallrat_filter<>'')   $conditions[] = $mallrat_filter;
	  if ($boje_leter_filter<>"")
	  	$conditions[] = "magazinaaaaa = '$boje_leter_filter'";
	  $filter_condition = implode(' AND ', $conditions);
      $filter_condition = "($filter_condition)";
	echo $filter_condition;
      return $filter_condition;

	}*/
		  $magazina = WebApp::getSVar("magazina->id");
		WebApp::debug_msg( WebApp::getSVar("fMallrat->malli"), 'malli');
	  if ($magazina=='leter')
	  {
	  	$malli = WebApp::getSVar("fMallrat->permasa1")."/".WebApp::getSVar("fMallrat->permasa2")."/".WebApp::getSVar("fMallrat->pesha")."/".WebApp::getSVar("fMallrat->malli1");
		}
	  else
	  	$malli = WebApp::getSVar("fMallrat->malli");

	  $sasia = WebApp::getSVar("fMallrat->sasia");
      $njesia = WebApp::getSVar("fMallrat->njesia");

      //$njesia = $this->getSVar("njesia");

      $conditions = array();

	  	WebApp::debug_msg($magazina, 'magazina');
		WebApp::debug_msg($njesia, 'njesia');

	  if ($magazina<>"")   $conditions[] = "magazina ='".$magazina."'";
	  if ($malli<>"")
        $conditions[] = "malli Like '%".$malli."%'";
      if ($sasia<>"")
        $conditions[] = "sasia='".$sasia."'";

      if ($njesia<>"")
        $conditions[] = "njesia Like '%".$njesia."%'";

      $filter = implode(" AND ", $conditions);
      if ($filter<>"")  $filter = "(".$filter.")";
	  WebApp::debug_msg( $filter);
      return $filter;
      //$this->setSVar("filter", $filter);
    }
}
?>


