<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package magazina
* @subpackage mallrat
*/

include_once FORM_PATH.'formWebObj.php';

class mallEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('$gjendja_id', UNDEFINED);
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //add variables with empty values
          WebApp::addVars(array('malli'=>'', 'njesia'=>'', 'sasia'=>'', 'cmimi'=>''));
        }
      else if ($mode=='edit')
        {
          $rs = WebApp::openRS('getMall');
          $fields = $rs->Fields();
          WebApp::addVars($fields);
        }

      WebApp::addVar('title', $title);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //find a mall_id for the new project
          $rs = WebApp::openRS('getLastMallId');
          $last_mall_id = $rs->Field('last_mall_id');
          $gjendja_id = (($last_mall_id==UNDEFINED) ? 1 : $last_mall_id+1);

          //save the new id
          $this->setSVar('gjendja_id', $gjendja_id);

          //add the new mall
          WebApp::execDBCmd('newMall', $event_args);
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('updateMall', $event_args);
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }
}
?>