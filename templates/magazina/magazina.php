<?php
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
* @package magazina
*/

class magazina extends WebObject
{
  function init()
    {
      $this->addSVar('id', 'leter');  // leter|boje|produkt
      $this->addSVar('module', 'faturat/listo/listo.html');
    }

  function onParse()
    {
      global $event;
      if ($event->target=='main' and $event->name=='select')
        {
          $this->select_submodule($event->args);
        }
    }

  function select_submodule($event_args)
    {
      $interface = WebApp::getSVar('interface');
      switch ($interface)
        {
          //Magazina e Letres
        case 'magazina/leter/faturat/listo':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Faturat';
          $module = 'faturat/listo/listo.html';
          break;
        case 'magazina/leter/faturat/hyrje':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Hyrje';
          $module = 'faturat/edit/faturEdit.html';
          $this->add_fature('hyrje');
          break;
        case 'magazina/leter/faturat/dalje':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Dalje';
          $module = 'faturat/edit/faturEdit.html';
          $this->add_fature('dalje');
          break;
        case 'magazina/leter/faturat/edit':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Modifiko Fature';
          $module = 'faturat/edit/faturEdit.html';
          $this->edit_fature();
          break;
        case 'magazina/leter/mallrat/listo':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Gjendja';
          $module = 'mallrat/mallList.html';
          break;
        case 'magazina/leter/levizjet':
          $this->setSVar('id', 'leter');
          $interface_title = 'Magazina e Letres-->Levizjet';
          $module = 'levizjet/listo.html';
          break;

          //Magazina e Bojes
        case 'magazina/boje/faturat/listo':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Faturat';
          $module = 'faturat/listo/listo.html';
          break;
        case 'magazina/boje/faturat/hyrje':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Hyrje';
          $module = 'faturat/edit/faturEdit.html';
          $this->add_fature('hyrje');
          break;
        case 'magazina/boje/faturat/dalje':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Dalje';
          $module = 'faturat/edit/faturEdit.html';
          $this->add_fature('dalje');
          break;
        case 'magazina/boje/faturat/edit':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Modifiko Fature';
          $module = 'faturat/edit/faturEdit.html';
          $this->edit_fature();
          break;
        case 'magazina/boje/mallrat/listo':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Gjendja';
          $module = 'mallrat/mallList.html';
          break;
        case 'magazina/boje/levizjet':
          $this->setSVar('id', 'boje');
          $interface_title = 'Magazina e Bojes-->Levizjet';
          $module = 'levizjet/listo.html';
          break;
        }

      WebApp::setSVar('interface_title', $interface_title);
      $this->setSVar('module', $module);
    }

  function add_fature($hyrje_dalje)
    {
      WebApp::setSVar('faturEdit->hyrje_dalje', $hyrje_dalje);
      WebApp::setSVar('faturEdit->mode', 'add');
      WebApp::setSVar('faturEdit->fature_id', UNDEFINED);
    }

  function edit_fature()
    {
      global $event;
      $fature_id = $event->args['fature_id'];
      $lloj_fature = $event->args['lloj_fature'];
      WebApp::setSVar('faturEdit->hyrje_dalje', $lloj_fature);
      WebApp::setSVar('faturEdit->mode', 'edit');
      WebApp::setSVar('faturEdit->fature_id', $fature_id);
    }
}
?>