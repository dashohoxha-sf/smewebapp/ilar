/**
 * This wrapper program is used so that apache can run commands
 * as the specified user and group (this is needed for writting
 * files, etc.)
 */

#include <stdio.h>
#include <stdlib.h>

/** the UID-s that are allowed to run programs using the wrapper */
#define APACHE_UID  48   /* the UID of apache */
#define MY_UID      500  /* our UID so we can test things ourselves */

/** target user id, group id, etc. (that will be used to run the command)  */
#define TGTUID 0  /* the desired run UID   */
#define TGTGID 0  /* the desired run GID */
#define TGTUSER "dasho"
#define TGTHOME "/home/" TGTUSER

int main(int argc, char**argv)
{
  size_t  i, n=0;
  char *command;

  /* check that it has been called by apache or by me */
  n = getuid();
  if(n!=APACHE_UID && n!=MY_UID) exit(-1);

  /* get the length of the command line */
  for(i=1; i<argc; i++)  n += strlen(argv[i]);
  if(!n) exit(0);

  /* get the command and its arguments */
  command = malloc(n+argc+1);
  *command = 0;
  for(i=1; i < argc; i++)
  {
    if (i > 1) strcat(command, " ");
    strcat(command, argv[i]);
  }

  /* run the command as the target user */
  setuid(TGTUID);
  setgid(TGTGID);
  setenv("USER", TGTUSER, 1);
  setenv("HOME", TGTHOME, 1);
  /*printf("%s\n", command);  /* debug */
  system(command);
}

