<?php
/* 
This file is  part of SMEWebApp.  SMEWebApp is a  web application that helps
the informatization of small and medium enterprises.
Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al
*/

//constants of the paths in the application
define('WEBAPP_PATH',   UP_PATH.'web_app-1_0/');
define('GRAPHICS',      'graphics/');
define('TPL_PATH',      'templates/');
define('TPL_URL',       'templates/');
define('FILTER_PATH',   TPL_PATH.'filtrat/');
define('MENU_PATH',     TPL_PATH.'menu/');
define('TABS',          'webobjects/tabs/');
?>
